<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Email
| -------------------------------------------------------------------------
| This file lets you define parameters for sending emails.
| Please see the user guide for info:
|
|	http://codeigniter.com/user_guide/libraries/email.html
|
*/

$config['protocol'] = "smtp";
$config['smtp_host'] = "ssl://smtp.googlemail.com";
$config['smtp_port'] = "465";
$config['smtp_user'] = "web.based.survey.platform@gmail.com";//also valid for Google Apps Accounts
$config['smtp_pass'] = "codeigniterUser";
$config['mailtype'] = 'html';
$config['charset'] = 'utf-8';
$config['newline'] = "\r\n";

//using cse credentials
/*
$config['protocol'] = "smtp";
$config['smtp_host'] = "smtp.cse.unsw.edu.au";
$config['smtp_user'] = "USERNAME@cse.unsw.edu.au";//also valid for Google Apps Accounts
$config['smtp_pass'] = "PASSWORD";
$config['mailtype'] = 'html';
$config['charset'] = 'utf-8';
$config['newline'] = "\r\n";
*/

/* End of file email.php */
/* Location: ./application/config/email.php */