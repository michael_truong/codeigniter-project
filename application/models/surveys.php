<?php
class surveys extends CI_Model {
	protected $surveys_table_name = 'surveys'; 
	protected $survey_owner_table_name = 'survey_owner'; 
	protected $parent_field_table_name = 'parent_field';
	protected $survey_parent_field_table_name = 'survey_parent_fields'; 
	protected $survey_questions_table_name = 'survey_questions';
	protected $survey_question_options_table_name = 'survey_question_options';
	protected $registered_surveys_table_name = 'registered_surveys';
	protected $groups_table_name = 'groups'; // groups
	protected $user_groups_table_name = 'user_groups'; // user groups
	protected $users_completed_surveys_table_name = 'users_completed_surveys';	
	protected $survey_responses_table_name = 'survey_responses';
	protected $users_table_name	= 'users';  // user accounts
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	function update_default_group($survey_id, $groups_id){
		$this->db->set('default_group', $groups_id);
		$this->db->where('surveyid', $survey_id);
		return $this->db->update($this->surveys_table_name);
	}
	
	/**
	 * Add group as participant for survey.
	 * @param	int
	 * @param	int
	 * @return	bool
	 */
	function add_group_participant($group_id, $survey_id)
	{
		// check if group is already a participant of the survey
		$this->db->where('surveyid', $survey_id);
		$this->db->where('groupid', $group_id);
		$query = $this->db->get($this->registered_surveys_table_name);
		if ($query->num_rows() > 0){
			// do nothing, user is already part of the group
			return true;
		} else {
			$this->db->set('surveyid', $survey_id);
			$this->db->set('groupid', $group_id);
			$insertResult = $this->db->insert($this->registered_surveys_table_name);
			if($insertResult){
				//insert success!
				return  $this->db->insert_id();
			}else{
				//handle failed insert here, rollback, whatever
				return false;
			}
			
		}
	}
	
	/**
	 * Swaps the parent field with $option_order up one position
	 * @param integer $option_order
	 */
	function swap_parent_field_up($option_order){
		$this->db->select($this->parent_field_table_name.'.*');
		$this->db->select($this->survey_parent_field_table_name.'.surveyid');
		$this->db->where('survey_order', $option_order);
		$this->db->where($this->survey_parent_field_table_name.'.parent_field_id = '.$this->parent_field_table_name.'.parent_field_id');
		$this->db->from($this->survey_parent_field_table_name);
		$this->db->from($this->parent_field_table_name);
		$option_query = $this->db->get();
		
		if ($option_query->num_rows() > 0){ // option exists
			$option = $option_query->result_array();
			$optionid = $option[0]['parent_field_id'];
			$surveyid = $option[0]['surveyid'];
				
			// select the next lowest value
			$this->db->select($this->parent_field_table_name.'.*');
			$this->db->where('survey_order < '.$option_order);
			$this->db->where($this->survey_parent_field_table_name.'.surveyid', $surveyid);
			$this->db->where($this->survey_parent_field_table_name.'.parent_field_id = '.$this->parent_field_table_name.'.parent_field_id');
			$this->db->order_by('survey_order', 'desc');
			$this->db->from($this->parent_field_table_name);
			$this->db->from($this->survey_parent_field_table_name);
			$second_option_query = $this->db->get();
			
			if ($second_option_query->num_rows() > 0){ // option exists
				$second_option = $second_option_query->result_array();
				$second_option_order = $second_option[0]['survey_order'];
				$second_optionid = $second_option[0]['parent_field_id'];
				// set original option to order of second option
				$this->db->set('survey_order', $second_option_order);
				$this->db->where('parent_field_id', $optionid);
				$this->db->update($this->parent_field_table_name);
				
				// set second option to order of original option
				$this->db->set('survey_order', $option_order);
				$this->db->where('parent_field_id', $second_optionid);
				$this->db->update($this->parent_field_table_name);

			}
			return $surveyid;
		} else {
			// no option with $option_order exists
			return null;
		}
	}
	
	/**
	 * Swaps the parent field with $option_order up one position
	 * @param integer $option_order
	 */
	function swap_parent_field_down($option_order){
		$this->db->select($this->parent_field_table_name.'.*');
		$this->db->select($this->survey_parent_field_table_name.'.surveyid');
		$this->db->where('survey_order', $option_order);
		$this->db->where($this->survey_parent_field_table_name.'.parent_field_id = '.$this->parent_field_table_name.'.parent_field_id');
		$this->db->from($this->survey_parent_field_table_name);
		$this->db->from($this->parent_field_table_name);
		$option_query = $this->db->get();
		
		if ($option_query->num_rows() > 0){ // option exists
			$option = $option_query->result_array();
			$optionid = $option[0]['parent_field_id'];
			$surveyid = $option[0]['surveyid'];
				
			// select the next lowest value
			$this->db->select($this->parent_field_table_name.'.*');
			$this->db->where('survey_order > '.$option_order);
			$this->db->where($this->survey_parent_field_table_name.'.surveyid', $surveyid);
			$this->db->where($this->survey_parent_field_table_name.'.parent_field_id = '.$this->parent_field_table_name.'.parent_field_id');
			$this->db->order_by('survey_order', 'asc');
			$this->db->from($this->parent_field_table_name);
			$this->db->from($this->survey_parent_field_table_name);
			$second_option_query = $this->db->get();
			
			if ($second_option_query->num_rows() > 0){ // option exists
				$second_option = $second_option_query->result_array();
				$second_option_order = $second_option[0]['survey_order'];
				$second_optionid = $second_option[0]['parent_field_id'];
				// set original option to order of second option
				$this->db->set('survey_order', $second_option_order);
				$this->db->where('parent_field_id', $optionid);
				$this->db->update($this->parent_field_table_name);
				
				// set second option to order of original option
				$this->db->set('survey_order', $option_order);
				$this->db->where('parent_field_id', $second_optionid);
				$this->db->update($this->parent_field_table_name);

			}
			return $surveyid;
		} else {
			// no option with $option_order exists
			return null;
		}
	}
	
	/**
	 * Swaps the option with $option_order up one position
	 * @param integer $option_order
	 */
	function swap_option_up($option_order){
		$this->db->select('*');
		$this->db->where('option_order', $option_order);
		$this->db->from($this->survey_question_options_table_name);
		$option_query = $this->db->get();
		if ($option_query->num_rows() > 0){ // option exists
			$option = $option_query->result_array();
			$optionid = $option[0]['itemid'];
			$questionid = $option[0]['questionid'];
			// select the next lowest value
			$this->db->select('*');
			$this->db->where('option_order < '.$option_order);
			$this->db->where('questionid', $questionid);
			$this->db->order_by('option_order', 'desc');
			$this->db->from($this->survey_question_options_table_name);
			$second_option_query = $this->db->get();
			
			if ($second_option_query->num_rows() > 0){ // option exists
				$second_option = $second_option_query->result_array();
				$second_option_order = $second_option[0]['option_order'];
				$second_optionid = $second_option[0]['itemid'];
				// set original option to order of second option
				$this->db->set('option_order', $second_option_order);
				$this->db->where('itemid', $optionid);
				$this->db->update($this->survey_question_options_table_name);

				// set second option to order of original option
				$this->db->set('option_order', $option_order);
				$this->db->where('itemid', $second_optionid);
				$this->db->update($this->survey_question_options_table_name);
			}
			return $questionid;
		} else {
			// no option with $option_order exists
			return null;
		}
	}
	
	/**
	 * Swaps the option with $option_order down one position
	 * @param integer $option_order
	 */
	function swap_option_down($option_order){
		$this->db->select('*');
		$this->db->where('option_order', $option_order);
		$this->db->from($this->survey_question_options_table_name);
		$option_query = $this->db->get();
		if ($option_query->num_rows() > 0){ // option exists
			$option = $option_query->result_array();
			$optionid = $option[0]['itemid'];
			$questionid = $option[0]['questionid'];
			// select the next lowest value
			$this->db->select('*');
			$this->db->where('option_order > '.$option_order);
			$this->db->where('questionid', $questionid);
			$this->db->order_by('option_order', 'asc');
			$this->db->from($this->survey_question_options_table_name);
			$second_option_query = $this->db->get();
			if ($second_option_query->num_rows() > 0){ // option exists
				$second_option = $second_option_query->result_array();
				$second_option_order = $second_option[0]['option_order'];
				$second_optionid = $second_option[0]['itemid'];
				// set original option to order of second option
				$this->db->set('option_order', $second_option_order);
				$this->db->where('itemid', $optionid);
				$this->db->update($this->survey_question_options_table_name);
				
				// set second option to order of original option
				$this->db->set('option_order', $option_order);
				$this->db->where('itemid', $second_optionid);
				$this->db->update($this->survey_question_options_table_name);
			}
			return $questionid;
		} else {
			// no option with $option_order exists
			return null;
		}
	}
	
	function remove_participating_group($group_id, $survey_id){
		$this->db->where('surveyid', $survey_id);
		$this->db->where('groupid', $group_id);
		$this->db->delete($this->registered_surveys_table_name);
	}
	
	function get_participating_groups($survey_id){
		$this->db->select('*');
		$this->db->from($this->groups_table_name);
		$this->db->join($this->registered_surveys_table_name, $this->groups_table_name.'.id = '.$this->registered_surveys_table_name.'.groupid');
		$this->db->where('surveyid', $survey_id);
		$this->db->order_by("groupname", "desc");
		
		$query = $this->db->get();
		if ($query->num_rows() > 0) return $query->result_array();
		return NULL;
	}

	
	/**
	 * Get all surveys the user owns
	 * @param string $user_id
	 */
	function get_all_surveys($user_id, $debug = false){
		$this->db->select($this->surveys_table_name.'.*');
		$this->db->from($this->survey_owner_table_name);
		$this->db->from($this->surveys_table_name);
		$this->db->where($this->survey_owner_table_name.'.userid', $user_id);
		$this->db->where($this->survey_owner_table_name.'.surveyid = '.$this->surveys_table_name.'.surveyid');
		$this->db->order_by($this->surveys_table_name.'.disabled', 'asc');
		$this->db->order_by($this->surveys_table_name.'.survey_state', 'asc');
		$this->db->order_by($this->surveys_table_name.'.surveyname', 'asc');
		$query = $this->db->get();
		if ($debug){
			return $this->db->last_query(); 
		} else {
			return $query->result_array();
		}

	}
	

	/**
	 * Get all surveys the user owns
	 * @param string $user_id
	 */
	function get_filtered_surveys($user_id, $filter, $debug = false){
		$this->db->select($this->surveys_table_name.'.*');
		$this->db->from($this->survey_owner_table_name);
		$this->db->from($this->surveys_table_name);
		$this->db->where($this->survey_owner_table_name.'.userid', $user_id);
		$this->db->where($this->survey_owner_table_name.'.surveyid = '.$this->surveys_table_name.'.surveyid');
		$this->db->like('LOWER('.$this->surveys_table_name.'.surveyname)', strtolower($filter));
		$this->db->order_by($this->surveys_table_name.'.disabled', 'asc');
		$this->db->order_by($this->surveys_table_name.'.survey_state', 'asc');
		$this->db->order_by($this->surveys_table_name.'.surveyname', 'asc');
		$query = $this->db->get();
		if ($debug){
			return $this->db->last_query();
		} else {
			return $query->result_array();
		}
	
	}

	/**
	 * Get an array of survey objects from an array of survey ids
	 * @param array() $survey_ids
	 */
	function get_surveys_by_id($survey_ids){
		$this->db->where_in('surveyid', $survey_ids);
		$this->db->order_by("surveyname", "desc");
		$query = $this->db->get($this->surveys_table_name);
		if ($query->num_rows() > 0) return $query->result_array();
		return NULL;
	}
	
	/**
	 * Return survey object if it exists
	 * @param string $survey_ids
	 */
	function get_survey_by_id($survey_id){
		$this->db->where('surveyid', $survey_id);
		$query = $this->db->get($this->surveys_table_name);
		if ($query->num_rows() == 1) return $query->row_array();
		return NULL;
	}

	/**
	 * Inserts a survey and returns the survey id
	 * @param string $surveyname
	 * @param string $survey_description
	 */
	public function insert_survey($surveyname, $survey_description, $anonymous)
	{
		$this->db->set('surveyname', $surveyname);
		$this->db->set('survey_description', $survey_description);
		$this->db->set('anonymous', $anonymous);
		$this->db->insert($this->surveys_table_name);
		return  $this->db->insert_id();
	}

	public function set_survey_owner($survey_id, $owner_id){
		$this->db->set('surveyid', $survey_id);
		$this->db->set('userid', $owner_id);
		return $this->db->insert($this->survey_owner_table_name);
	}
	
	public function disable_survey($survey_id){
		$this->db->set('disabled', 1);
		$this->db->where('surveyid', $survey_id);
		return $this->db->update($this->surveys_table_name);
	}
	
	public function enable_survey($survey_id){
		$this->db->set('disabled', 0);
		$this->db->where('surveyid', $survey_id);
		return $this->db->update($this->surveys_table_name);
	}
	
	public function start_survey($survey_id){
		$this->db->set('survey_state', 'STARTED');
		$this->db->where('surveyid', $survey_id);
		return $this->db->update($this->surveys_table_name);
	}
	
	public function stop_survey($survey_id){
		$this->db->set('survey_state', 'STOPPED');
		$this->db->where('surveyid', $survey_id);
		return $this->db->update($this->surveys_table_name);
	}
	
	public function update_survey_details($surveyid, $title, $survey_description, $anonymous){
		$this->db->set('surveyname', $title);
		$this->db->set('survey_description', $survey_description);
		$this->db->set('anonymous', $anonymous);
		$this->db->where('surveyid', $surveyid);
		
		$this->db->update($this->surveys_table_name);
		return $this->db->affected_rows() > 0;
	}
	
	public function update_single_line_question($questionid, $questiontitle, $help_text, $required){
		$this->db->set('questiontitle', $questiontitle);
		$this->db->set('help_text', $help_text);
		$this->db->set('required', $required);
		$this->db->where('questionid', $questionid);
		
		$this->db->update($this->survey_questions_table_name);
		return $this->db->affected_rows() > 0;
	}
	
	public function addParentField($surveyid, $format_type){
		$defaultQuestionTitle = 'Edit Question...';
		$defaultSectionTitle = 'Edit Section Label...';
		
		// start database transaction
		$this->db->trans_start();
		
		// insert parent field
		$description = "generic default description";
		$this->db->set('field_type', $format_type);
		$this->db->set('description', $description);
		$this->db->insert($this->parent_field_table_name);
		
		// get inserted parent field id
		$insertedParentField = 	$this->db->insert_id();
				
		// set parent field question order
		$this->set_parent_field_order($insertedParentField, $insertedParentField);
		
		// link parent field with survey
		$this->db->set('parent_field_id', $insertedParentField);
		$this->db->set('surveyid', $surveyid);
		$this->db->insert($this->survey_parent_field_table_name);
				
			// connect children survey question fields depending on the $format_type
			if ($format_type == 'SECTION'){
				$this->db->set('questiontitle', $defaultSectionTitle);
				$this->db->set('input_type', 'NONE');
				$this->db->set('help_text', 'A description of the section goes here.');
				$this->db->set('parent_field_id', $insertedParentField);
				$this->db->insert($this->survey_questions_table_name);
			} else if ($format_type == 'SINGLE_LINE_TEXT'){
				// create one survey question field of type single line text
				$this->db->set('questiontitle', $defaultQuestionTitle);
				$this->db->set('input_type', 'TEXT');
				$this->db->set('parent_field_id', $insertedParentField);
				$this->db->insert($this->survey_questions_table_name);
			} else if ($format_type == 'NUMBER'){
				// create one survey question field of type multiple choice
				$this->db->set('questiontitle', $defaultQuestionTitle);
				$this->db->set('input_type', 'NUMERIC');
				$this->db->set('parent_field_id', $insertedParentField);
				$this->db->insert($this->survey_questions_table_name);
			} else if ($format_type == 'LONG_ANSWER'){
				// create one survey question field of type multiple choice
				$this->db->set('questiontitle', $defaultQuestionTitle);
				$this->db->set('input_type', 'LONG_ANSWER');
				$this->db->set('parent_field_id', $insertedParentField);
				$this->db->insert($this->survey_questions_table_name);
			} else { // if it is a format type with options
				$isOptionsQuestion = true;
				if ($format_type == 'MULTIPLE_CHOICE'){
					// create one survey question field of type multiple choice
					$this->db->set('questiontitle', $defaultQuestionTitle);
					$this->db->set('input_type', 'MULTIPLE_CHOICE');
					$this->db->set('parent_field_id', $insertedParentField);
					$this->db->insert($this->survey_questions_table_name);				
				}  else if ($format_type == 'DROPDOWN'){
					// create one survey question field of type multiple choice
					$this->db->set('questiontitle', $defaultQuestionTitle);
					$this->db->set('input_type', 'DROPDOWN');
					$this->db->set('parent_field_id', $insertedParentField);
					$this->db->insert($this->survey_questions_table_name);
				} else if ($format_type == 'CHECKBOX'){
					// create one survey question field of type multiple choice
					$this->db->set('questiontitle', $defaultQuestionTitle);
					$this->db->set('input_type', 'CHECKBOXES');
					$this->db->set('parent_field_id', $insertedParentField);
					$this->db->insert($this->survey_questions_table_name);
				} else {
					// Does not match any questions or options (i.e. Page Break)
					$isOptionsQuestion = false;
				}
				
				if ($isOptionsQuestion){
					// get inserted parent field id
					$insertedQuestion = $this->db->insert_id();
						
					// add default options
					$this->add_option($insertedQuestion, 'First Option');
					$this->add_option($insertedQuestion, 'Second Option');
					$this->add_option($insertedQuestion, 'Third Option');
				}
			}		
			
			
		// finish database transaction
		$this->db->trans_complete();
				
		return $insertedParentField;
	}
	
	// question level ordering
	function set_parent_field_order($questionid, $order){
		$this->db->set('survey_order', $order);
		$this->db->where('parent_field_id', $questionid);
		$this->db->update($this->parent_field_table_name);
	}
	
	// display level ordering
	function set_survey_question_order($questionid, $order){
		$this->db->set('survey_order', $order);
		$this->db->where('questionid', $questionid);
		$this->db->update($this->survey_questions_table_name);
	}
	
	public function get_all_parent_fields($survey_id){
		$this->db->select($this->parent_field_table_name.'.*');
		$this->db->from($this->parent_field_table_name);
		$this->db->from($this->survey_parent_field_table_name);
		$this->db->where($this->survey_parent_field_table_name.'.surveyid', $survey_id);
		$this->db->where($this->survey_parent_field_table_name.'.parent_field_id = '.$this->parent_field_table_name.'.parent_field_id');
		$this->db->order_by($this->parent_field_table_name.'.survey_order');
		$query = $this->db->get();
		if ($query->num_rows() > 0) return $query->result_array();
		return NULL;
	}

	public function get_children_fields_by_parent_id($parent_field_ids){
		$this->db->where_in('parent_field_id', $parent_field_ids);
		$query = $this->db->get($this->survey_questions_table_name);
		if ($query->num_rows() > 0) return $query->result_array();
		return NULL;
	}
	
	public function get_children_field_by_parent_id($parent_field_id){
		$this->db->where('parent_field_id', $parent_field_id);
		$query = $this->db->get($this->survey_questions_table_name);
		if ($query->num_rows() > 0) return $query->result_array();
		return NULL;
	}
	
	function get_survey_questions_by_surveyid($surveyid){
		$this->db->select($this->survey_questions_table_name.'.*');
		$this->db->from($this->surveys_table_name);
		$this->db->from($this->survey_parent_field_table_name);
		$this->db->from($this->parent_field_table_name);
		$this->db->from($this->survey_questions_table_name);
		$this->db->where($this->surveys_table_name.'.surveyid = '.$surveyid); // surveys join
		$this->db->where($this->surveys_table_name.'.surveyid = '.$this->survey_parent_field_table_name.'.surveyid'); // survey parent field join
		$this->db->where($this->parent_field_table_name.'.parent_field_id = '.$this->survey_parent_field_table_name.'.parent_field_id'); // parent field join
		$this->db->where($this->parent_field_table_name.'.parent_field_id = '.$this->survey_questions_table_name.'.parent_field_id');
		$query = $this->db->get();
		return $query->result_array();
	}
	
	/**
	 * Get options which map to question id
	 * @param integer $questionid
	 */
	public function get_options_by_questionid($questionid){
		$this->db->where('questionid', $questionid);
		$this->db->order_by("option_order", "asc");
		$query = $this->db->get($this->survey_question_options_table_name);
		if ($query->num_rows() > 0) return $query->result_array();
		return NULL;
	}
	
	/**
	 * Caution: Deletes children survey questions and question options permanently.
	 * @param integer $parent_field_id
	 */
	function delete_parent_field($parent_field_id){
		// start database transaction
		$this->db->trans_start();
		
		// grab all questions under parent field
		$this->db->select('questionid');
		$this->db->where('parent_field_id', $parent_field_id);
		$query = $this->db->get($this->survey_questions_table_name);
		// delete all options of questions
		if ($query->num_rows() > 0){
			$questions = $query->result_array();
			$question_ids = array();
			foreach ($questions as $question){
				$question_ids[] = $question['questionid'];
			}
			$this->db->where_in('questionid',  $question_ids);
			$this->db->delete($this->survey_question_options_table_name);
			
		}
		// delete all questions
		$this->db->where('parent_field_id', $parent_field_id);
		$this->db->delete($this->survey_questions_table_name);
		
		// delete parent field from survey relation
		$this->db->where('parent_field_id', $parent_field_id);
		$this->db->delete($this->survey_parent_field_table_name);

		// delete parent field
		$this->db->where('parent_field_id', $parent_field_id);
		$this->db->delete($this->parent_field_table_name);
		
		// finish database transaction
		$this->db->trans_complete();
		
	}

	function delete_option($itemid){
		$this->db->where('itemid', $itemid);
		$this->db->delete($this->survey_question_options_table_name);
	}
	
	function hasOptions($questionid){
		$this->db->where('questionid', $questionid);
		$query = $this->db->get($this->survey_question_options_table_name);
		if ($query->num_rows() > 0) return true;
		return false;
	}
	
	function add_default_option($questionid){
		$this->db->set('questionid', $questionid);
		$this->db->set('itemdescription', "Other");
		$this->db->insert($this->survey_question_options_table_name);
		return  $this->db->insert_id();
	}
	
	function add_option($questionid, $title = "Edit Option..."){
		// start database transaction
		$this->db->trans_start();
		
		// Create Option
		$this->db->set('questionid', $questionid);
		$this->db->set('itemdescription', $title);
		$this->db->insert($this->survey_question_options_table_name);
		$optionid =  $this->db->insert_id();
		
		// Set option order
		$this->db->set('option_order', $optionid);
		$this->db->where('itemid', $optionid);
		$this->db->update($this->survey_question_options_table_name);
		
		// finish database transaction
		$this->db->trans_complete();
		
		return $optionid;
	}
	
	function update_option($itemid, $title){
		$this->db->set('itemdescription', $title);
		$this->db->where('itemid', $itemid);
	
		$this->db->update($this->survey_question_options_table_name);
		return $this->db->affected_rows() > 0;
	}
	
	/**
	 * Inserts a record of the user completing a particular survey
	 * @param integer $user_id
	 * @param integer $survey_id
	 */
	function set_survey_completed($user_id, $survey_id){
		$this->db->set('surveyid', $survey_id);
		$this->db->set('userid', $user_id);
		$this->db->insert($this->users_completed_surveys_table_name);
	}

	
	/**
	 * Returns list of surveys the user has completed
	 * @param integer $user_id
	 */
	function get_completed_surveys($user_id, $debug = false){
		$this->db->select('surveyid');
		$this->db->where('userid', $user_id);
		$query = $this->db->get($this->users_completed_surveys_table_name);
		if ($debug){
			return $this->db->last_query();
				
		} else {
			if ($query->num_rows() > 0) return $query->result_array();
			return null;
		}

	}
	
	/**
	 * Returns list of users that have completed the survey
	 * @param integer $survey_id
	 */
	function get_completed_participants($survey_id, $getUsername = false, $debug = false){
		if ($getUsername){
			$this->db->select($this->users_table_name.'.username');
		} else {
			$this->db->select($this->users_completed_surveys_table_name.'.userid');
		}
		$this->db->from($this->users_completed_surveys_table_name);
		$this->db->from($this->users_table_name);
		$this->db->where($this->users_table_name.'.id = '.$this->users_completed_surveys_table_name.'.userid');
		$this->db->where($this->users_completed_surveys_table_name.'.surveyid', $survey_id);
		if ($getUsername){
			$this->db->group_by($this->users_table_name.'.username');
		} else {
			$this->db->group_by($this->users_completed_surveys_table_name.'.userid');
		}
		$query = $this->db->get();
		if ($debug){
			return $this->db->last_query();
	
		} else {
			return $query->result_array();
		}
	
	}
	
	/**
	 * Returns list of users that have completed the survey
	 * @param integer $survey_id
	 */
	function get_not_completed_participants($survey_id, $debug = false){
		$completed_users = $this->convertResultArray($this->get_completed_participants($survey_id), 'userid');
		
		$this->db->select($this->users_table_name.'.username');
		$this->db->from($this->users_table_name); // user groups
		$this->db->from($this->user_groups_table_name); // user groups
		$this->db->from($this->groups_table_name); // groups
		$this->db->from($this->registered_surveys_table_name); // registered groups to surveys
		$this->db->from($this->surveys_table_name); // surveys
		$this->db->where($this->groups_table_name.'.id = '.$this->user_groups_table_name.'.groups_id'); // groups join
		$this->db->where($this->registered_surveys_table_name.'.groupid = '.$this->groups_table_name.'.id'); // registered surveys join
		$this->db->where($this->surveys_table_name.'.surveyid = '.$this->registered_surveys_table_name.'.surveyid'); // surveys join
		$this->db->where($this->surveys_table_name.'.surveyid = '.$survey_id); //match based on surveyid
		if (ISSET ($completed_users) && !empty($completed_users)){
			$this->db->where_not_in($this->user_groups_table_name.'.user_id', $completed_users); //match based on surveyid
		}
		$this->db->where($this->users_table_name.'.id = '.$this->user_groups_table_name.'.user_id'); //match based on surveyid
		
		$this->db->group_by($this->users_table_name.'.username');
		$query = $this->db->get();
		if ($debug){
			return $this->db->last_query();
		
		} else {
			return $query->result_array();
		}
	
	}
	
	/**
	 * returns true if the survey is anonymous, otherwise false
	 * @param integer $surveyid
	 * returns boolean
	 */
	function is_survey_anonymous($surveyid){
		$this->db->select('*');
		$this->db->where('surveyid', $surveyid);
		$this->db->where('anonymous', 1);
		$query = $this->db->get($this->surveys_table_name);
		if ($query->num_rows() > 0) return true;
		return false;
	}
	
	/**
	 * returns the survey responses that match up to a particular survey_response_uuid
	 * @param string $survey_response_uuid
	 * @param boolean $debug
	 * @return result_array or NULL
	 */
	function get_user_response_by_uuid($survey_response_uuid, $filterBySubmitted = false, $debug = false){
		$this->db->select($this->survey_responses_table_name.'.questionid');
		$this->db->select($this->survey_responses_table_name.'.answerdescription');
		$this->db->select($this->survey_responses_table_name.'.response_id');
		$this->db->from($this->survey_responses_table_name);
		$this->db->where($this->survey_responses_table_name.'.survey_response_uuid', $survey_response_uuid);
		if ($filterBySubmitted){
			$this->db->where($this->survey_responses_table_name.'.submitted_response', 1);
		}
		$query = $this->db->get();
		if ($debug){
			return $this->db->last_query();
		} else {
			if ($query->num_rows() > 0) return $query->result_array();
			return NULL;
		}
	}
	
	
	/**
	 * 
	 * @param integer $questionid
	 * @param boolean $filterBySubmitted
	 * @param boolean $debug
	 * @return NULL or result_array
	 */
	function get_responses_by_questionid($questionid, $filterBySubmitted = false, $debug = false){
		$this->db->select($this->survey_responses_table_name.'.questionid');
		$this->db->select($this->survey_responses_table_name.'.answerdescription');
		$this->db->select($this->survey_responses_table_name.'.response_id');
		$this->db->from($this->survey_responses_table_name);
		$this->db->where($this->survey_responses_table_name.'.questionid = '.$questionid);
		if ($filterBySubmitted){
			$this->db->where($this->survey_responses_table_name.'.submitted_response', 1);
		}
		$query = $this->db->get();
		if ($debug){
			return $this->db->last_query();
		} else {
			if ($query->num_rows() > 0) return $query->result_array();
			return NULL;
		}
	}
	
	/**
	 *
	 * @param integer $questionid
	 * @param boolean $filterBySubmitted
	 * @param boolean $debug
	 * @return NULL or result_array
	 */
	function get_responses_with_count_by_questionid($questionid, $filterBySubmitted = false, $orderByCount = false, $debug = false){
		$this->db->select($this->survey_responses_table_name.'.answerdescription');
		$this->db->select("COUNT(".$this->survey_responses_table_name.'.answerdescription'.") as count");
		$this->db->from($this->survey_responses_table_name);
		$this->db->where($this->survey_responses_table_name.'.questionid = '.$questionid);
		if ($filterBySubmitted){
			$this->db->where($this->survey_responses_table_name.'.submitted_response', 1);
		}
		$this->db->group_by($this->survey_responses_table_name.'.answerdescription');
		if ($orderByCount){
			$this->db->order_by('count', 'desc');
		} else {
			$this->db->order_by($this->survey_responses_table_name.'.answerdescription');
		}
		$query = $this->db->get();
		if ($debug){
			return $this->db->last_query();
		} else {
			if ($query->num_rows() > 0) return $query->result_array();
			return NULL;
		}
	}
	
	function get_user_response($user_id, $survey_id, $debug = false){
		$this->db->select($this->survey_responses_table_name.'.questionid');
		$this->db->select($this->survey_responses_table_name.'.answerdescription');
		$this->db->select($this->survey_responses_table_name.'.response_id');
		$this->db->from($this->surveys_table_name);
		$this->db->from($this->survey_responses_table_name);
		$this->db->from($this->survey_parent_field_table_name);
		$this->db->from($this->parent_field_table_name);
		$this->db->from($this->survey_questions_table_name);
		$this->db->where($this->surveys_table_name.'.surveyid = '.$survey_id); // surveys join
		$this->db->where($this->surveys_table_name.'.surveyid = '.$this->survey_parent_field_table_name.'.surveyid'); // survey parent field join
		$this->db->where($this->parent_field_table_name.'.parent_field_id = '.$this->survey_parent_field_table_name.'.parent_field_id'); // parent field join
		$this->db->where($this->parent_field_table_name.'.parent_field_id = '.$this->survey_questions_table_name.'.parent_field_id');
		$this->db->where($this->survey_responses_table_name.'.userid', $user_id);
		$this->db->where($this->survey_questions_table_name.'.questionid = '.$this->survey_responses_table_name.'.questionid');
		$query = $this->db->get();
		if ($debug){
			return $this->db->last_query();
		} else {
			if ($query->num_rows() > 0) return $query->result_array();
			return NULL;
		}
	}
	
	/**
	 * returns whether the unlinking user from responses was a success
	 * @param integer $user_id
	 * @param text  $survey_response_UUID
	 * @param boolean $debug
	 * @return boolean
	 */
	function unlink_user_from_responses($user_id, $survey_response_UUID, $debug = false){
		$this->db->set('userid', null);
		$this->db->set('unlinked', true);
		$this->db->where('survey_response_uuid', $survey_response_UUID);
		$this->db->where('userid', $user_id);
		$query = $this->db->update($this->survey_responses_table_name);
		if ($debug){
			return $this->db->last_query();
		}
	}
	
	/**
	 * Returns whether the user has completed a particular survey
	 * @param integer $user_id
	 * @param integer $surveyid
	 * @param boolean $debug
	 * @return boolean
	 */
	function has_completed_survey($user_id, $surveyid, $debug = false){
		$this->db->select('*');
		$this->db->where('userid', $user_id);
		$this->db->where('surveyid', $surveyid);
		$query = $this->db->get($this->users_completed_surveys_table_name);
		if ($debug){
			return $this->db->last_query();
		
		} else {
			if ($query->num_rows() > 0) return true;
			return false;
		}
	}
	
	function is_registered_user($user_id, $surveyid, $debug = false){
		$this->db->select('*');
		$this->db->from($this->user_groups_table_name); // user groups
		$this->db->join($this->groups_table_name, $this->groups_table_name.'.id = '.$this->user_groups_table_name.'.groups_id'); // groups join
		$this->db->join($this->registered_surveys_table_name, $this->registered_surveys_table_name.'.groupid = '.$this->groups_table_name.'.id'); // registered surveys join
		$this->db->join($this->surveys_table_name, $this->surveys_table_name.'.surveyid = '.$this->registered_surveys_table_name.'.surveyid'); // surveys join
		$this->db->where('user_id', $user_id); // user groups filtering
		$this->db->where($this->surveys_table_name.'.disabled !=', 1); // disabled survey filtering
		$this->db->where($this->surveys_table_name.'.surveyid', $surveyid); //match based on surveyid
		$query = $this->db->get();
		if ($debug){
			return $this->db->last_query();
		
		} else {
			if ($query->num_rows() > 0) return true;
			return false;
		}
	}
	
	function get_all_registered_surveys($user_id, $isCompleted, $debug = false){
		$isCompletedQuery = $this->get_completed_surveys($user_id);
		$this->db->distinct();
		$this->db->select($this->surveys_table_name.'.surveyid');
		$this->db->select('surveyname');
		$this->db->select('survey_description');
		$this->db->select('anonymous');
		$this->db->select('survey_state');
		$this->db->from($this->user_groups_table_name); // user groups
		$this->db->join($this->groups_table_name, $this->groups_table_name.'.id = '.$this->user_groups_table_name.'.groups_id'); // groups join
		$this->db->join($this->registered_surveys_table_name, $this->registered_surveys_table_name.'.groupid = '.$this->groups_table_name.'.id'); // registered surveys join
		$this->db->join($this->surveys_table_name, $this->surveys_table_name.'.surveyid = '.$this->registered_surveys_table_name.'.surveyid'); // surveys join
		$this->db->where('user_id', $user_id); // user groups filtering
		$this->db->where($this->surveys_table_name.'.disabled !=', 1); // disabled survey filtering
		if (ISSET($isCompletedQuery)){
			$isCompletedArray = array();
			foreach ($isCompletedQuery as $surveyid){
				$isCompletedArray[] = $surveyid['surveyid'];
			}
			if($isCompleted){
				$this->db->where_in($this->surveys_table_name.'.surveyid', $isCompletedArray);
			} else {
				$this->db->where_not_in($this->surveys_table_name.'.surveyid', $isCompletedArray);
			}
		} else if ($isCompleted) {
			return null; // early exit, there are no surveys completed by the user
		}
		$this->db->order_by('surveyname', "desc"); // sort joined tuples based on groupname descending
		$query = $this->db->get();
		
		if ($debug){
			return $this->db->last_query();
				
		} else {
			if ($query->num_rows() > 0) return $query->result_array();
			return NULL;
		}
			
	}
	
	/**
	 * Updates user's response to a question if it exits, otherwise creates a new one
	 * @param integer $user_id
	 * @param integer $question_id
	 * @param text $answer
	 * @param text $survey_response_uuid
	 * 
	 */
	function save_or_update_user_response($user_id, $question_id, $answer, $survey_response_uuid, $debug = false){
		// check if a response exits
		$this->db->where('userid', $user_id);
		$this->db->where('questionid', $question_id);
		$query = $this->db->get($this->survey_responses_table_name);
		if ($query->num_rows() > 0){
			// if a response exists, update it
			$this->db->set('answerdescription', $answer);
			$this->db->set('survey_response_uuid', $survey_response_uuid);
			$this->db->where('userid', $user_id);
			$this->db->where('questionid', $question_id);
			$query = $this->db->update($this->survey_responses_table_name);
		} else {
			// if a response doesn't exist, insert a new one
			$this->db->set('answerdescription', $answer);
			$this->db->set('survey_response_uuid', $survey_response_uuid);
			$this->db->set('userid', $user_id);
			$this->db->set('questionid', $question_id);
			$query = $this->db->insert($this->survey_responses_table_name);
		}
		if ($debug){
			return $this->db->last_query();
		}
	}
	
	/**
	 * 
	 * @param result_array $queryResultArray
	 * @param string $fieldname
	 */
	function convertResultArray($queryResultArray, $fieldname){
		$convertedArray = array();
		
		if (ISSET($queryResultArray)){
			foreach ($queryResultArray as $tuple){
				$convertedArray[] = $tuple[$fieldname];
			}
		}
		return $convertedArray;
	}
	
	/**
	 * sets the flag that the response is ready for use in survey statistics
	 * @param unknown_type $survey_response_uuid
	 */
	function set_submitted_responses($survey_response_uuid, $debug = false){
		$this->db->set('submitted_response', 1);
		$this->db->where('survey_response_uuid', $survey_response_uuid);
		$query = $this->db->update($this->survey_responses_table_name);
		if ($debug){
			return $this->db->last_query();
		}
	}
	
	function find_unlinked_responses_by_uuid($survey_id, $debug = false){
		// find all relevant question ids to a $survey_id
		// find all relevant survey responses
		// where submitted_response = 1
		// where unlinked_response = 1
		// group_by survey_response_uuid
		
		$this->db->select($this->survey_responses_table_name.'.survey_response_uuid');
		$this->db->select($this->survey_responses_table_name.'.userid as username');
		$this->db->from($this->surveys_table_name);
		$this->db->from($this->survey_parent_field_table_name);
		$this->db->from($this->parent_field_table_name);
		$this->db->from($this->survey_questions_table_name);
		$this->db->from($this->survey_responses_table_name);
		$this->db->where($this->surveys_table_name.'.surveyid = '.$survey_id);
		$this->db->where($this->surveys_table_name.'.surveyid = '.$this->survey_parent_field_table_name.'.surveyid');
		$this->db->where($this->parent_field_table_name.'.parent_field_id = '.$this->survey_parent_field_table_name.'.parent_field_id');
		$this->db->where($this->parent_field_table_name.'.parent_field_id = '.$this->survey_questions_table_name.'.parent_field_id');
		$this->db->where($this->survey_responses_table_name.'.questionid = '.$this->survey_questions_table_name.'.questionid');
		$this->db->where($this->survey_responses_table_name.'.submitted_response', 1);
		$this->db->where($this->survey_responses_table_name.'.unlinked', 1);
		$this->db->group_by($this->survey_responses_table_name.'.survey_response_uuid');
		$this->db->group_by($this->survey_responses_table_name.'.userid');
		$query = $this->db->get();
		if ($debug){
			return $this->db->last_query();
		}else {
			return $query->result_array();
		}
	}
	
	function find_responses_by_uuid($survey_id, $debug = false){
		// find all relevant question ids to a $survey_id
		// find all relevant survey responses
		// where submitted_response = 1
		// group_by survey_response_uuid
		
		$this->db->select($this->survey_responses_table_name.'.survey_response_uuid');
		$this->db->select($this->users_table_name.'.username');
		$this->db->from($this->users_table_name);
		$this->db->from($this->surveys_table_name);
		$this->db->from($this->survey_parent_field_table_name);
		$this->db->from($this->parent_field_table_name);
		$this->db->from($this->survey_questions_table_name);
		$this->db->from($this->survey_responses_table_name);
		$this->db->where($this->surveys_table_name.'.surveyid = '.$survey_id);
		$this->db->where($this->surveys_table_name.'.surveyid = '.$this->survey_parent_field_table_name.'.surveyid');
		$this->db->where($this->parent_field_table_name.'.parent_field_id = '.$this->survey_parent_field_table_name.'.parent_field_id');
		$this->db->where($this->parent_field_table_name.'.parent_field_id = '.$this->survey_questions_table_name.'.parent_field_id');
		$this->db->where($this->survey_responses_table_name.'.questionid = '.$this->survey_questions_table_name.'.questionid');
		$this->db->where($this->survey_responses_table_name.'.submitted_response', 1);
		$this->db->where($this->survey_responses_table_name.'.userid = '.$this->users_table_name.'.id');
		$this->db->group_by($this->survey_responses_table_name.'.survey_response_uuid');
		$this->db->group_by($this->users_table_name.'.username');
		$this->db->order_by($this->users_table_name.'.username');
		$query = $this->db->get();
		if ($debug){
			return $this->db->last_query();
		}else {
			return $query->result_array();
		}
	}

}