<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Extends the Tank Auth Users model with minimal support for groups
 *
 * @author John.Wright
*/
class ta_Groups_Users extends Users {

	//may be issues if you use table name prefix in config... hasn't been tested
	//see parent constructor.
	protected $table_name	= 'users';  // user accounts
	protected $user_groups_table_name = 'user_groups'; // user groups

	function __construct()
	{
		parent::__construct();

		$ci =& get_instance();
		$this->user_groups_table_name = $ci->config->item('db_table_prefix', 'tank_auth').$this->user_groups_table_name;
	}

	function get_filtered_participating_users($group_id, $filter){
		$this->db->select('*');
		$this->db->from($this->table_name);
		$this->db->join($this->user_groups_table_name, $this->table_name.'.id = '.$this->user_groups_table_name.'.user_id');
		$this->db->where('groups_id', $group_id);
		$this->db->like('LOWER('.$this->table_name.'.username)', strtolower($filter));
		$this->db->order_by("username", "desc");
	
		$query = $this->db->get();
		if ($query->num_rows() > 0) return $query->result_array();
		return NULL;
	}
	
	function get_participating_users($group_id){
		$this->db->select('*');
		$this->db->from($this->table_name);
		$this->db->join($this->user_groups_table_name, $this->table_name.'.id = '.$this->user_groups_table_name.'.user_id');
		$this->db->where('groups_id', $group_id);
		$this->db->order_by("username", "desc");
	
		$query = $this->db->get();
		if ($query->num_rows() > 0) return $query->result_array();
		return NULL;
	}
		
	/**
	 * Set group_id for user. (group_id concerning admin/user authorisation)
	 *
	 * @param	int
	 * @param	int
	 * @return	bool
	 */
	function promoteUser($user_id)
	{
		$this->db->set('group_id', 100);
		$this->db->where('id', $user_id);

		$this->db->update($this->table_name);
		return $this->db->affected_rows() > 0;
	}

	/**
	 * Set groups_id for user. (groups_id concerning group categorisation for email/survey purposes)
	 *
	 * @param	int
	 * @param	int
	 * @return	bool
	 */
	function set_groups_id($user_id, $groups_id)
	{
		// check if user is already a part of the group
		$this->db->where('user_id', $user_id);
		$this->db->where('groups_id', $groups_id);
		$query = $this->db->get($this->user_groups_table_name);
		if ($query->num_rows() > 0){
			// do nothing, user is already part of the group
			return true;
		} else {
			$this->db->set('user_id', $user_id);
			$this->db->set('groups_id', $groups_id);
			return $this->db->insert($this->user_groups_table_name);
		}
	}
	

	function remove_participating_user($user_id, $groups_id)
	{
		$this->db->where('user_id', $user_id);
		$this->db->where('groups_id', $groups_id);
		$this->db->delete($this->user_groups_table_name);
	}
	
	/**
	 * Get user groups by id
	 *
	 * @return	object
	 */
	function get_all_groups($user_id)
	{
		$this->db->where('user_id', $user_id);
		$query = $this->db->get($this->user_groups_table_name);
		if ($query->num_rows() > 0) return $query->result_array();
		return NULL;
	}

	/**
	 * Get all users
	 *
	 * @return	object
	 */
	function get_all_users()
	{
		$this->db->order_by($this->table_name.'.group_id', 'asc');
		$this->db->order_by($this->table_name.'.username', 'asc');
		$query = $this->db->get($this->table_name);		
		if ($query->num_rows() > 0) return $query->result_array();
		return NULL;
	}
	
	/**
	 * Get user by filter
	 *
	 * @return	object
	 */
	function get_filtered_users($filter)
	{
		$this->db->like('LOWER('.$this->table_name.'.username)', strtolower($filter));
		$this->db->or_like('LOWER('.$this->table_name.'.email)', strtolower($filter));
		$this->db->order_by($this->table_name.'.group_id', 'asc');
		$this->db->order_by($this->table_name.'.username', 'asc');
		$query = $this->db->get($this->table_name);
		if ($query->num_rows() > 0) return $query->result_array();
		return NULL;
	}

	/**
	 * Returns an array of users
	 * @param array $user_ids
	 */
	public function get_users_by_id($user_ids){
		$this->db->where_in('id', $user_ids);
		$this->db->order_by("username", "desc");
		$query = $this->db->get($this->table_name);
		if ($query->num_rows() > 0) return $query->result_array();
		return NULL;
	}
	
	/**
	 * Returns a user
	 * @param array $user_ids
	 */
	public function get_users_by_email($email){		
		$this->db->where('LOWER('.$this->table_name.'.email)', strtolower($email));
		$this->db->order_by("username", "desc");
		$query = $this->db->get($this->table_name);
		if ($query->num_rows() > 0) return $query->result_array();
		return NULL;
	}

}