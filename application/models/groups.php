<?php
class groups extends CI_Model {
	protected $groups_table_name = 'groups'; // groups
	protected $user_groups_table_name = 'user_groups'; // user groups


	public function __construct()
	{
		parent::__construct();
		$this->load->database();

		$ci =& get_instance();
		$this->user_groups_table_name = $ci->config->item('db_table_prefix', 'tank_auth').$this->user_groups_table_name;
		$this->groups_table_name = $ci->config->item('db_table_prefix', 'tank_auth').$this->groups_table_name;

	}
	

	/**
	 * Update values of a group
	 * @param int $groups_id
	 * @param string $title
	 * @param string $group_description
	 * @return boolean
	 */
	function update_group($groups_id, $title, $group_description)
	{
		$this->db->set('groupname', $title);
		$this->db->set('text', $group_description);
		$this->db->where('id', $groups_id);

		$this->db->update($this->groups_table_name);
		return $this->db->affected_rows() > 0;
	}

	
	/**
	 * Get user by filter
	 *
	 * @return	object
	 */
	function get_filtered_groups($filter)
	{
		$this->db->like('LOWER('.$this->groups_table_name.'.groupname)', strtolower($filter));
		$this->db->order_by($this->groups_table_name.'.is_default_group', 'asc');
		$this->db->order_by($this->groups_table_name.'.groupname', 'desc');
		$query = $this->db->get($this->groups_table_name);
		if ($query->num_rows() > 0) return $query->result_array();
		return NULL;
	}
	
	public function get_all_groups()
	{
		$this->db->order_by("is_default_group", "asc");
		$this->db->order_by("groupname", "desc");
		$query = $this->db->get($this->groups_table_name);
		if ($query->num_rows() > 0) return $query->result_array();
		return NULL;
	}
	
	/**
	 * Get user by filter
	 *
	 * @return	object
	 */
	function get_filtered_groups_with_user($filter, $userid)
	{
		$this->db->like('LOWER('.$this->groups_table_name.'.groupname)', strtolower($filter));
		$this->db->select($this->groups_table_name.'.*');
		$this->db->from($this->groups_table_name);
		$this->db->from($this->user_groups_table_name);
		$this->db->where($this->user_groups_table_name.".groups_id = ".$this->groups_table_name.'.id');
		$this->db->where($this->user_groups_table_name.".user_id", $userid);
		$this->db->order_by("is_default_group", "asc");
		$this->db->order_by("groupname", "desc");
		$query = $this->db->get();
		if ($query->num_rows() > 0) return $query->result_array();
		return NULL;
	}
	
	public function get_all_groups_with_user($userid, $debug = false)
	{
		$this->db->select($this->groups_table_name.'.*');
		$this->db->from($this->groups_table_name);
		$this->db->from($this->user_groups_table_name);
		$this->db->where($this->user_groups_table_name.".groups_id = ".$this->groups_table_name.'.id');
		$this->db->where($this->user_groups_table_name.".user_id", $userid);
		$this->db->order_by($this->groups_table_name.".is_default_group", "asc");
		$this->db->order_by($this->groups_table_name.".groupname", "desc");
		$query = $this->db->get();
		if ($debug){
			return $this->db->last_query();
		} else {
			return $query->result_array();
		}
	}

	/**
	 * returns a single group
	 * @param int $groups_id
	 * @return NULL
	 */
	public function get_group_by_id($groups_id){
		$this->db->where('id', $groups_id);
		$query = $this->db->get($this->groups_table_name);
		if ($query->num_rows() == 1) return $query->row_array();
		return NULL;
	}
	
	/**
	 * Returns an array of groups
	 * @param array $groups_ids
	 */
	public function get_groups_by_id($groups_ids){
		$this->db->where_in('id', $groups_ids);
		$this->db->order_by("groupname", "desc");
		$query = $this->db->get($this->groups_table_name);
		if ($query->num_rows() > 0) return $query->result_array();
		return NULL;
	}
	
	public function delete_group($groups_id){
		$this->db->where('id', $groups_id);
		return $query = $this->db->delete($this->groups_table_name);
	}

	public function insert_group($groupname, $group_description, $isDefaultGroup = 0, $debug = false)
	{
		$this->db->set('groupname', $groupname);
		$this->db->set('text', $group_description);
		$this->db->set('is_default_group', $isDefaultGroup);

		$this->db->insert($this->groups_table_name);
		
		if ($debug){
			return $this->db->last_query();
		} else {
			return  $this->db->insert_id();
		}
	}

	public function set_groupname($group_id, $groupname){
		$this->db->set('groupname', $groupname);
		$this->db->where('id', $group_id);
		 
		$this->db->update($this->groups_table_name);
		return $this->db->affected_rows() > 0;
	}

	/**
	 * Get user groups by id
	 *
	 * @return	array of user_group objects
	 */
	function get_all_user_groups($groups_id)
	{
		$this->db->where('groups_id', $groups_id);
		$query = $this->db->get($this->user_groups_table_name);
		if ($query->num_rows() > 0) return $query->result_array();
		return NULL;
	}
	
	/**
	 * Removes a user_group row
	 */
	function remove_user($groups_id, $user_id){
		$this->db->where('groups_id', $groups_id);
		$this->db->where('user_id', $user_id);
		return $this->db->delete($this->user_groups_table_name);
	}
}