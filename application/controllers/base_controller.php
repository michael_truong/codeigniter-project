<?php

// application/controllers/base_controller.php

class Base_controller extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		//$this->load->library('tank_auth');
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->library('security');
		$this->load->library(
				'tank_auth_groups', '', 'tank_auth'
		);
		
		// modify title
		$_SESSION['survey_system_title']= "Codeigniter Survey";
		
		if (!$this->tank_auth->is_logged_in()){	
			$encoded_uri = $this->grabEncodedRequestURI();
			redirect('/auth/login/'.$encoded_uri);
		}elseif($this->tank_auth->is_logged_in(FALSE)){  // logged in, not activated
			redirect('/auth/send_again/');				
		}else{
			//Logged IN Stuff Here
			$_SESSION['current_username']=$this->tank_auth->get_username();
				
		}
	}
	
	function grabEncodedRequestURI(){
		// /sitename/index.php
		$path = explode('/', trim($_SERVER['SCRIPT_NAME'], '/'));
			
		// /sitename/controller/function or /sitename/index.php/controller/function
		$uri  = explode('/', trim($_SERVER['REQUEST_URI'], '/'));
		
		if (isset($uri[1])){ // check if $uri is a list (more than 1 value), otherwise presume it is default controller with no arguments
			foreach ($path as $key => $val) {
				if ($val == $uri[$key]) {
					unset($uri[$key]);
				} else {
					break;
				}
			}		
			// controller/function
			$uri = implode('/', $uri);
		} else {
			// clear uri in the case that it is the default controller to redirect to
			$uri = ""; 
		}

		$encoded_uri = preg_replace('"/"', '_', $uri);
		return $encoded_uri;
	}
}

?>