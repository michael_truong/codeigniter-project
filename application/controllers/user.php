<?php

// application/controllers/user.php
require 'application/controllers/base_controller.php';

class user extends base_controller {

	function __construct()
	{
		parent::__construct();

		$this->load->model('tank_auth/TA_groups_users', "", "users");
		$this->load->model('groups');
		$this->load->model('surveys');
		

		$this->_init();

	}

	function _init(){
		$this->output->set_template('dashboard');
		$this->load->section('nav', 'templates/user_header');
		$this->load->css('assets/css/default.css');

	}

	function index()
	{
		redirect('user/dashboard');

	}

	function dashboard()
	{
		// display user dashboard


		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();

		$this->load->view('pages/welcome', $data);
	}

	function userGroupDetails(){
		$user_id	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		$data['userid'] = $user_id;
		$this->load->view('user/user_group_details', $data);
	}
	
	function surveyParticipation(){
		$user_id	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		$data['active_surveys'] = $this->surveys->get_all_registered_surveys($user_id, false);		
		$data['completed_surveys'] = $this->surveys->get_all_registered_surveys($user_id, true);		
		
		//$data['debug'] = $this->surveys->get_completed_surveys($user_id, true);		
		//$data['what'] = $this->surveys->get_all_registered_surveys($user_id, false, true);
		$this->load->view('user/survey_participation', $data);
	}
}

?>