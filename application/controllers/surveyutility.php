<?php

// application/controllers/admin-utility.php
require 'application/controllers/user.php';

class surveyutility extends base_controller {


	function __construct()
	{
		parent::__construct();

		$this->load->model('tank_auth/TA_groups_users', "", "users");
		$this->load->model('groups');
		$this->load->model('surveys');

	}


	function grabChild(){
		$parentid =  $this->input->post('parentid');
		$parenttype =  $this->input->post('parenttype');
		echo json_encode($this->surveys->get_children_field_by_parent_id($parentid));
	}
	
	function grabOptions(){
		$questionid =  $this->input->post('questionid');
		$input_type =  $this->input->post('input_type');
		echo json_encode($this->surveys->get_options_by_questionid($questionid));
	}
	
	function ajax_validate()
	{
		$type = $this->input->post('type');
		$id = $this->input->post('id');
		
		$rule = 'xss_clean';
		if($type == 'numeric'){
			$rule = $rule."|numeric";
		}
		$this->form_validation->set_rules($id, $type, $rule);
		if($this->form_validation->run())
		{
			$validates = true;
		}
		else {
			$validates = false;
		}

		$response = array(
				'validates' => $validates,
				'error' => form_error($id), 
				//'validates' => false, // for debugging purposes
				//'error' => "debug error string",
				'id' => $id, 
				'rule' => $rule,
				'type' => $type);
		echo json_encode($response);
		exit;
	}

}

?>