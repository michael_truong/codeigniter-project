<?php

// application/controllers/admin_base.php
require 'application/controllers/base_controller.php';

class Admin_Base extends base_controller {

	function __construct()
	{
		parent::__construct();

		if(!$this->tank_auth->is_admin())
		{
			redirect('auth/login');
		}
	}
}

?>