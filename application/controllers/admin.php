<?php

// application/controllers/admin.php
require 'application/controllers/admin_base.php';

class Admin extends Admin_Base {

	function __construct()
	{
		parent::__construct();

		$this->load->model('tank_auth/TA_groups_users', "", "users");
		$this->load->model('groups');
		$this->load->model('surveys');
		$this->_init();
	}

	function _init(){
		$this->output->set_template('dashboard');
		$this->load->section('nav', 'templates/admin_header');

		$this->load->css('assets/css/default.css');

	}

	function index()
	{
		redirect('admin/dashboard');
	}

	function dashboard()
	{
		// display admin dashboard
		$data['username']	= $this->tank_auth->get_username();
		$data['user_id']	= $this->tank_auth->get_user_id();

		$this->load->view('pages/welcome', $data);
	}

	function listSystemUsers(){
		$this->load->view('admin/list_users');
	}

	
	function addGroupMember($groups_id){
		$data['group'] = $this->groups->get_group_by_id($groups_id);
		$data['groupname'] = $data['group']['groupname'];
		$this->load->view('admin/add_group_member', $data);
	}
	
	function addBatchGroupMembers($groups_id){
		$data['group'] = $this->groups->get_group_by_id($groups_id);
		$data['groupname'] = $data['group']['groupname'];
		$this->load->view('admin/add_batch_group_members', $data);
	}
	
	function createUser(){
		$use_username = $this->config->item('use_username', 'tank_auth');
		$data['use_username'] = $use_username;
		
		$this->load->view('admin/create_user', $data);
	}
	
	function createUserBatch(){
		$use_username = $this->config->item('use_username', 'tank_auth');
		$data['use_username'] = $use_username;
		
		$this->load->view('admin/create_batch_users', $data);
	}

	function groupAdmin(){

		$this->load->view('admin/group_admin');
	}
	
	function viewGroup($groups_id){
		$data['group'] = $this->groups->get_group_by_id($groups_id);
		$data['groupname'] = $data['group']['groupname'];

		$this->load->view('admin/view_group', $data);
	}

	function editGroup($groups_id, $result = ""){
		$data['group'] = $this->groups->get_group_by_id($groups_id);
		$data['groupname'] = $data['group']['groupname'];
		$data['result'] = $result;
		$this->load->view('admin/edit_group', $data);
	}

	function deleteGroup($groups_id){
		$success = $this->groups->delete_group($groups_id);
		// return to group admin page
		redirect('admin/groupAdmin');
	}

	function editGroupDetails(){
		// set rules
		// $this->form_validation->set_rules(EXACT_NAME, HUMAN_READABLE_NAME, RULE);
		$this->form_validation->set_rules('title', "Group Title", 'required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('group_description', "Group Description", 'xss_clean');
		$this->form_validation->set_rules('groupname', "Group Name", 'xss_clean');
		$this->form_validation->set_rules('groups_id', "Group Id", 'xss_clean');

		if ($this->form_validation->run() == FALSE)
		{
			$result = "form validation failed";
			$groupname = $this->form_validation->set_value('groupname');
			$groups_id = $this->form_validation->set_value('groups_id');
		}
		else
		{
			$title = $this->form_validation->set_value('title');
			$group_description = $this->form_validation->set_value('group_description');
			$groupname = $this->form_validation->set_value('groupname');
			$groups_id = $this->form_validation->set_value('groups_id');

			// save input into database
			$success = $this->groups->update_group($groups_id, $title, $group_description);

			$result = array ('title' => $title,
					'Group description' => $group_description,
					'Groupname' => $groupname,
					'Groups_id' => $groups_id,
					'success' => $success);

		}

		// reroute back to edit group
		$this->editGroup($groups_id, $result);
	}

	function createSurvey(){

		$this->form_validation->set_rules('title', "Survey Title", 'required|min_length[3]|xss_clean|is_unique[surveys.surveyname]');
		$this->form_validation->set_rules('survey_description', "Survey Description", 'xss_clean');
			
		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('admin/create_survey');
		}
		else
		{
			$title = $this->form_validation->set_value('title');
			$survey_description = $this->form_validation->set_value('survey_description');
			$anonymous = (isset($_POST['anonymous'])) ? 1 : 0;
				
			if (!ISSET ($survey_description)) $survey_description = "";
			// save input into database
			$success = $this->surveys->insert_survey($title, $survey_description, $anonymous);

			$result = array ('title' => $title,
					'Group description' => $survey_description,
					'success' => $success);

			if ($success){
				// TODO may need to do the below operations in a transaction
				
				// link the newly created survey with the owner
				$survey_id = $success;
				$this->surveys->set_survey_owner($survey_id, $this->tank_auth->get_user_id());
				// create the default group to be linked with the survey
				$groups_id = $this->groups->insert_group($title.": survey participants", "Default group for survey ".$title, 1);
				$this->surveys->add_group_participant($groups_id, $survey_id);
				// add the newly created default group to the survey
				$this->surveys->update_default_group($survey_id, $groups_id);
				
				// return to group admin page
				// $this->surveyAdmin();
				redirect("admin/editSurvey/".$survey_id);
			} else {
				$data['result'] = $result;
				$this->load->view('admin/create_survey', $data);
			}

		}

	}

	// views the responses to the particular survey
	function viewSurveyResponses($survey_id){
		$data['survey'] = $this->surveys->get_survey_by_id($survey_id);
		
		$this->load->view('admin/view_survey_responses', $data);
		
	}
	
	function manageParticipants($survey_id){
		$data['survey'] = $this->surveys->get_survey_by_id($survey_id);
		$this->load->view('admin/manage_participants', $data);
	}
	
	function editSurvey($survey_id, $result = ""){
		$data['result'] = $result;
		$data['survey'] = $this->surveys->get_survey_by_id($survey_id);

		// get all parent fields related to the survey view
		$parent_fields = $this->surveys->get_all_parent_fields($survey_id);
		// generate array of parent field ids
		if (ISSET ($parent_fields)){
			$parent_field_ids = array();
			foreach ($parent_fields as $parent_field){
				$parent_field_ids[] = $parent_field['parent_field_id'];
			}
			$data['survey_question_list'] = $this->surveys->get_children_fields_by_parent_id($parent_field_ids);
		}
		
		$data['parent_field_list'] = $parent_fields;
		
		$this->load->view('admin/edit_survey', $data);
	}


	function editSurveyDetails(){
		// set rules
		// $this->form_validation->set_rules(EXACT_NAME, HUMAN_READABLE_NAME, RULE);
		$this->form_validation->set_rules('title', "Survey Title", 'required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('survey_description', "Survey Description", 'xss_clean');
		$this->form_validation->set_rules('surveyname', "Survey Name", 'xss_clean');
		$this->form_validation->set_rules('surveyid', "Survey Id", 'xss_clean');

		if ($this->form_validation->run() == FALSE)
		{
			$result = "form validation failed";
			$surveyname = $this->form_validation->set_value('surveyname');
			$surveyid = $this->form_validation->set_value('surveyid');
		}
		else
		{
			$title = $this->form_validation->set_value('title');
			$survey_description = $this->form_validation->set_value('survey_description');
			$surveyname = $this->form_validation->set_value('surveyname');
			$surveyid = $this->form_validation->set_value('surveyid');
			$anonymous = (isset($_POST['anonymous'])) ? 1 : 0;
			// save input into database
			$success = $this->surveys->update_survey_details($surveyid, $title, $survey_description, $anonymous);
			$result = array ('title' => $title,
					'Survey description' => $survey_description,
					'Surveyname' => $surveyname,
					'Surveyid' => $surveyid,
					'anonymous' => $anonymous,
					'success' => $success);

		}

		// reroute back to edit survey
		// $this->editSurvey($surveyid, $result);
		redirect('admin/editSurvey/'.$surveyid);
	}
	
	function editSimpleInputQuestion(){
		// set rules
		// $this->form_validation->set_rules(EXACT_NAME, HUMAN_READABLE_NAME, RULE);
		$this->form_validation->set_rules('questiontitle', "Question Title", 'required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('help_text', "Help Text", 'xss_clean');
		$this->form_validation->set_rules('questionid', "Question Id", 'xss_clean');
		$this->form_validation->set_rules('surveyid', "Survey Id", 'xss_clean');
		
		if ($this->form_validation->run() == FALSE)
		{
			$result = "form validation failed";
		}
		else
		{
			$questiontitle = $this->form_validation->set_value('questiontitle');
			$help_text = $this->form_validation->set_value('help_text');
			$questionid = $this->form_validation->set_value('questionid');
			$required = (isset($_POST['required_field'])) ? 1 : 0;
			// save input into database
			$success = $this->surveys->update_single_line_question($questionid, $questiontitle, $help_text, $required);
			$result = array ('question title' => $questiontitle,
					'help text' => $help_text,
					'question id' => $questionid,
					'required' => $required,
					'success' => $success);
		
		}
		
		$surveyid = $this->form_validation->set_value('surveyid');
		
		// reroute back to edit survey
		// $this->editSurvey($surveyid, $result);
		redirect('admin/editSurvey/'.$surveyid);
		
	}
	
	function editOptionsInputQuestion(){
		// set rules
		// $this->form_validation->set_rules(EXACT_NAME, HUMAN_READABLE_NAME, RULE);
		$this->form_validation->set_rules('questiontitle', "Question Title", 'required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('help_text', "Help Text", 'xss_clean');
		$this->form_validation->set_rules('questionid', "Question Id", 'xss_clean');
		$this->form_validation->set_rules('surveyid', "Survey Id", 'xss_clean');
	
		if ($this->form_validation->run() == FALSE)
		{
			$result = "form validation failed";
		}
		else
		{
			$questiontitle = $this->form_validation->set_value('questiontitle');
			$help_text = $this->form_validation->set_value('help_text');
			$questionid = $this->form_validation->set_value('questionid');
			$required = (isset($_POST['required_field'])) ? 1 : 0;
			// save input into database
			$success = $this->surveys->update_single_line_question($questionid, $questiontitle, $help_text, $required);
			$result = array ('question title' => $questiontitle,
					'help text' => $help_text,
					'question id' => $questionid,
					'required' => $required,
					'success' => $success);
	
		}
	
		$surveyid = $this->form_validation->set_value('surveyid');
	
		// reroute back to edit survey
		// $this->editSurvey($surveyid, $result);
		redirect('admin/editSurvey/'.$surveyid);
		
	}
	

	function editNoInputQuestion(){
		// set rules
		// $this->form_validation->set_rules(EXACT_NAME, HUMAN_READABLE_NAME, RULE);
		$this->form_validation->set_rules('questiontitle', "Question Title", 'required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('help_text', "Help Text", 'xss_clean');
		$this->form_validation->set_rules('questionid', "Question Id", 'xss_clean');
		$this->form_validation->set_rules('surveyid', "Survey Id", 'xss_clean');
	
		if ($this->form_validation->run() == FALSE)
		{
			$result = "form validation failed";
		}
		else
		{
			$questiontitle = $this->form_validation->set_value('questiontitle');
			$help_text = $this->form_validation->set_value('help_text');
			$questionid = $this->form_validation->set_value('questionid');
			// save input into database
			$success = $this->surveys->update_single_line_question($questionid, $questiontitle, $help_text, false);
			$result = array ('question title' => $questiontitle,
					'help text' => $help_text,
					'question id' => $questionid,
					'success' => $success);
	
		}
	
		$surveyid = $this->form_validation->set_value('surveyid');
	
		// reroute back to edit survey
		// $this->editSurvey($surveyid, $result);
		redirect('admin/editSurvey/'.$surveyid);
		
	}
	
	function addFormatting($surveyid, $format_type){
		$this->surveys->addParentField($surveyid, $format_type);
		$result = array('format type' => $format_type);
		// $this->editSurvey($surveyid, $result);
		redirect('admin/editSurvey/'.$surveyid);
		
	}

	function addStandardQuestion($surveyid, $question_type){
		$this->surveys->addParentField($surveyid, $question_type);
		$result = array('question type' => $question_type);
		// $this->editSurvey($surveyid, $result);
		redirect('admin/editSurvey/'.$surveyid);
		
	}

	function deleteSurvey($survey_id){
		$this->surveys->delete_survey($survey_id);
		$this->surveyAdmin();
	}

	function disableSurvey($survey_id){
		$this->surveys->disable_survey($survey_id);
		$this->surveyAdmin();
	}

	function enableSurvey($survey_id){
		$this->surveys->enable_survey($survey_id);
		$this->surveyAdmin();
	}

	function startSurvey($survey_id){
		$this->surveys->start_survey($survey_id);
		$this->surveyAdmin();
	}

	function stopSurvey($survey_id){
		$this->surveys->stop_survey($survey_id);
		$this->surveyAdmin();
	}

	function surveyAdmin(){
		$data['user_id']	= $this->tank_auth->get_user_id();
		$this->load->view('admin/survey_admin', $data);
	}
	
	function viewSurveySummary($survey_id){
		$data['survey_id']	= $survey_id;
		$data['survey'] = $this->surveys->get_survey_by_id($survey_id);
		$this->load->view('admin/view_survey_summary', $data);
	}
	
	function viewIndividualResponses($survey_id){
		$data['survey'] = $this->surveys->get_survey_by_id($survey_id);
		$data['response_query'] = $this->surveys->find_responses_by_uuid($survey_id, true);
		$this->load->view('admin/view_individual_response', $data);
		
	}
	
	/**
	 * Similar to viewSurvey, but with the responses loaded based on response_uuid
	 * @param integer $surveyid
	 */
	function viewSurveyResponse($surveyid, $response_uuid){
		$data['survey_id']	= $surveyid;
		$data['survey'] = $this->surveys->get_survey_by_id($surveyid);
		// get all parent fields related to the survey view
		$data['parent_field_list'] = $this->surveys->get_all_parent_fields($surveyid);
		$data['survey_responses'] = $this->surveys->get_user_response_by_uuid($response_uuid);
		$this->load->view('survey/survey_view', $data);	
	}
	
}

?>