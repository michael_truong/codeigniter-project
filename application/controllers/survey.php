<?php

// application/controllers/user.php
require 'application/controllers/base_controller.php';

class survey extends base_controller {

	function __construct()
	{
		parent::__construct();

		$this->load->model('tank_auth/TA_groups_users', "", "users");
		$this->load->model('groups');
		$this->load->model('surveys');	
		$this->load->library("pagination");
		

		$this->_init();

	}

	function _init(){
		$this->output->set_template('survey');
		$this->load->css('assets/css/default.css');

	}

	function index()
	{
		// if accessed without a function, return to the user dashboard
		redirect('user/dashboard');
	}
	
	/**
	 * View the survey with submit functionality disabled
	 * @param string $survey_id
	 */
	function viewSurvey($survey_id){
		$data['survey_id']	= $survey_id;
		$data['survey'] = $this->surveys->get_survey_by_id($survey_id);
		// get all parent fields related to the survey view
		$data['parent_field_list'] = $this->surveys->get_all_parent_fields($survey_id);
		$this->load->view('survey/survey_view', $data);
	}
	
	/**
	 * Respond to the survey, with save and submit functionality enabled
	 * @param string $survey_id
	 */
	function respondToSurvey($survey_id){
		$user_id	= $this->tank_auth->get_user_id();
		
		if ($this->is_user_participating($user_id, $survey_id)){
			$data['survey_id']	= $survey_id;
			$data['survey'] = $this->surveys->get_survey_by_id($survey_id);
			// get all parent fields related to the survey view
			$data['parent_field_list'] = $this->surveys->get_all_parent_fields($survey_id);

			$data['survey_responses'] = $this->surveys->get_user_response($user_id, $survey_id);
			
			$this->load->section('nav', 'templates/survey_header', $data);
			$this->load->view('survey/survey_view', $data);
		} else {
			// user is not allowed to participate, redirect to the dashboard
			redirect("user/dashboard");
		}
			

		
	}
	
	/**
	 * Similar to viewSurvey, but with the user_id responses loaded
	 * @param integer $surveyid
	 */
	function viewSurveyResponse($surveyid){
		$user_id	= $this->tank_auth->get_user_id();
		$data['survey_id']	= $surveyid;
		$data['survey'] = $this->surveys->get_survey_by_id($surveyid);
		// get all parent fields related to the survey view
		$data['parent_field_list'] = $this->surveys->get_all_parent_fields($surveyid);
		
		$data['survey_responses'] = $this->surveys->get_user_response($user_id, $surveyid);
		
		$this->load->view('survey/survey_view', $data);	}
	
	/**
	 * returns true if the user is allowed to respond to a survey, otherwise false
	 * @param integer $user_id
	 * @param integer $surveyid
	 * return boolean
	 */
	function is_user_participating($user_id, $surveyid){
		$canParticipate = true; // assume true unless proved otherwise
		if ($this->surveys->has_completed_survey($user_id, $surveyid)){
			// check if the user has completed the survey. If they have, they cant redo survey.
			$canParticipate = false;
		} else if (!$this->surveys->is_registered_user($user_id, $surveyid)){
			$canParticipate = false;
		}
		
		return $canParticipate;
	}
	

	/**
	 * function that processes which submit button was clicked from the client
	 * @param integer $survey_id
	 */
	function surveySubmit($survey_id){
		if($this->input->post('submit') == "Save Progress") {
// 			if ($this->saveSurvey($survey_id)){
// 				// something is wrong
// 				$this->respondToSurvey($survey_id);
// 			} else {
// 				// for debugging purposes
// 				// $this->output->unset_template();
// 				// echo "got to save survey";
// 				redirect("user/dashboard");
// 			}

			// save all input and continue even if some inputs are invalid (fail form_validation)
			$this->saveSurvey($survey_id);
			redirect("user/dashboard");
		} else if($this->input->post('submit') == "Submit Survey") {
			if ($this->submitSurvey($survey_id)){
				// something is wrong
				$this->respondToSurvey($survey_id);
			} else {
				// for debugging purposes
				// $this->output->unset_template();
				// echo "got to submit survey";
				redirect("user/dashboard");
			}
		} else {
			// something is wrong
			$this->respondToSurvey($survey_id);
		}
	}
	
	/**
	 * Saves the responses on the survey form. Returns whether the survey has errors
	 * @param integer $survey_id
	 * @param string $survey_response_UUID
	 * returns boolean $hasErrors
	 */
	function saveSurvey($survey_id, $survey_response_UUID = null, $isSubmit = false){
		$hasErrors = false;
		if (!ISSET($survey_response_UUID)){
			$survey_response_UUID = md5(uniqid().uniqid()); // unique UUID and 32chars
		}
		$user_id	= $this->tank_auth->get_user_id();
		// iterate through all survey_id parent fields
		$parent_fields = $this->surveys->get_all_parent_fields($survey_id);
		if (ISSET ($parent_fields)){
			// build form_validation
			foreach ($parent_fields as $parent_field){
				$survey_questions = $this->surveys->get_children_field_by_parent_id($parent_field['parent_field_id']);
				if (ISSET($survey_questions)){
					foreach ($survey_questions as $survey_question){
						$this->build_form_validation($survey_question, $isSubmit);
					}
				}
			}
			// run form_validation and save responses
			foreach ($parent_fields as $parent_field){
				$survey_questions = $this->surveys->get_children_field_by_parent_id($parent_field['parent_field_id']);
				if (ISSET($survey_questions)){
					foreach ($survey_questions as $survey_question){
						if(!$this->process_question_input($survey_question, $user_id, $survey_response_UUID, $isSubmit)){
							// note: if optimisation is required, early exit here by returning true
							$hasErrors = true;
						}
					}
				}
			}
		}
		return $hasErrors;
	}
	

	/**
	 * Builds the form_validation rules
	 * @param integer $survey_question
	 * @param boolean $isSubmit
	 */
	function build_form_validation($survey_question, $isSubmit){
		$required = ($isSubmit && ($survey_question['required'] == 1)) ? true : false;
	
		if ($survey_question['input_type'] == "NUMERIC"){
			$validation_rule = 'xss_clean';
			$validation_rule = $validation_rule.'|numeric';
			if ($required){
				$validation_rule = $validation_rule."|required";
			}
			$this->form_validation->set_rules('input-'.$survey_question['questionid'], $survey_question['questiontitle'], $validation_rule);
		} else if ($survey_question['input_type'] == "CHECKBOXES"){
			if ($required){
				$validation_rule = "required";
				$this->form_validation->set_rules('input-'.$survey_question['questionid'].'[]', $survey_question['questiontitle'], $validation_rule);
			} else {
				// checkboxes are processed via $_POST because there are no rules
			}
		} else if ($survey_question['input_type'] == "MULTIPLE_CHOICE"){
			if ($required){
				$validation_rule = "required";
				$this->form_validation->set_rules('input-'.$survey_question['questionid'], $survey_question['questiontitle'], $validation_rule);
			} else {
				// multiple choice (radio buttons) are processed via $_POST because there are no rules
			}
		} else if ($survey_question['input_type'] == "TEXT"){
			$validation_rule = 'xss_clean';
			if ($required){
				$validation_rule = $validation_rule."|required";
			}
			$this->form_validation->set_rules('input-'.$survey_question['questionid'], $survey_question['questiontitle'], $validation_rule);
		} else if ($survey_question['input_type'] == "LONG_ANSWER"){
			$validation_rule = 'xss_clean';
			if ($required){
				$validation_rule = $validation_rule."|required";
			}
			$this->form_validation->set_rules('input-'.$survey_question['questionid'], $survey_question['questiontitle'], $validation_rule);
		} else if ($survey_question['input_type'] == "DROPDOWN"){
			if ($required){
				$validation_rule = "callback_select_validate[".$survey_question['questiontitle']."]";
				$this->form_validation->set_rules('input-'.$survey_question['questionid'], $survey_question['questiontitle'], $validation_rule);
			} else {
				// dropdowns are processed via $_POST because there are no rules
			}
		} else {
			// it is type none, or unidentified
		}
	
	}
	
	/**
	 * Processes specific questions on the form
	 * @param sql_query_row $survey_question
	 * @param integer $user_id
	 * @param string $survey_response_UUID
	 * @return boolean
	 */
	function process_question_input($survey_question, $user_id, $survey_response_UUID, $isSubmit){
		$required = ($isSubmit && ($survey_question['required'] == 1)) ? true : false;
		
		$valid_input = true;
		
		$answer = '';
		
		// handle text input
		if (($survey_question['input_type'] == "TEXT")
				|| ($survey_question['input_type'] == "LONG_ANSWER")
				|| ($survey_question['input_type'] == "NUMERIC")){
			if ($this->form_validation->run() == FALSE)
			{
				// some values are invalid
				$valid_input = false;
				// TODO may need to do some smart security stuff here to get around not using form_validation
					
				// attempt to save using post
				$answer = $this->input->post('input-'.$survey_question['questionid']);
			}
			else
			{
				$answer = $this->form_validation->set_value('input-'.$survey_question['questionid']);
			}
		}
		// handle checkbox options
		else if ($survey_question['input_type'] == "CHECKBOXES"){
			if ($required && ($this->form_validation->run() == FALSE))
			{
				// some values are invalid
				$valid_input = false;
			}
			// save value regardless of valid form
			$answer = '';
			if (ISSET($_POST['input-'.$survey_question['questionid']])){
				foreach($_POST['input-'.$survey_question['questionid']] as $optionValue){
					if (trim($answer)!=''){
						$answer = $answer.',';
					}
					$answer = $answer.$optionValue;
				}
			}

		} else if ($survey_question['input_type'] == "MULTIPLE_CHOICE"){
			if ($required && ($this->form_validation->run() == FALSE))
			{
				// some values are invalid
				$valid_input = false;
			}
			if(ISSET($_POST['input-'.$survey_question['questionid']])){
				$answer = $this->input->post('input-'.$survey_question['questionid']);
			}
		} else if ($survey_question['input_type'] == "DROPDOWN"){
			if ($required && ($this->form_validation->run() == FALSE))
			{
				// some values are invalid
				$valid_input = false;
			}
			$answer = $this->input->post('input-'.$survey_question['questionid']);
		}

		// normalise answers. NULL is treated as an empty string
		if (is_null($answer)){
			$answer = '';
		}
		
		// save to survey_responses with questionid, survey_response_UUID and user_id
		$this->surveys->save_or_update_user_response($user_id, $survey_question['questionid'], $answer, $survey_response_UUID);
		
		
		return $valid_input;

	}
	
	/**
	 * function that saves the survey form, and sets the survey as completed for the user
	 * @param integer $survey_id
	 */
	function submitSurvey($survey_id){
		$survey_response_UUID = md5(uniqid().uniqid()); // unique UUID and 32chars
		$hasErrors = $this->saveSurvey($survey_id, $survey_response_UUID, true);
		if (!$hasErrors){
			// create the survey_completed tuple
			$user_id	= $this->tank_auth->get_user_id();
			$this->surveys->set_survey_completed($user_id, $survey_id);
			// upon completion, set the responses to submitted
			$this->surveys->set_submitted_responses($survey_response_UUID);
			// if the survey is anonymous, unlink the userid from survey responses
			$this->unlink_responses_if_anonymous($user_id, $survey_id, $survey_response_UUID);
		}
		return $hasErrors;
	}
	
	/**
	 * Checks if a survey is anonymous, if it is, loop through its questions and unlink user_id
	 * @param integer $user_id
	 * @param integer $survey_id
	 */
	function unlink_responses_if_anonymous($user_id, $survey_id, $survey_response_UUID){
		if($this->surveys->is_survey_anonymous($survey_id)){
			// unlink only if the survey is anonymous
			$this->surveys->unlink_user_from_responses($user_id, $survey_response_UUID);
			// for debugging
			// echo $this->surveys->unlink_user_from_responses($user_id, $survey_response_UUID, true);
		}
	}
	
	function select_validate($selectValue, $questionTitle)
	{
		// 'none' is the first option and the text says something like "-Choose one-"
		if($selectValue == '')
		{
			$this->form_validation->set_message('select_validate', 'The '.$questionTitle.' field is required.');
			return false;
		}
		else // user picked something
		{
			return true;
		}
	}
	
}

?>