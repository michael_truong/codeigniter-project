<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require 'application/controllers/base_controller.php';

class Welcome extends base_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	
	public function __construct()
	{
		parent::__construct();
		
		
	}
	
	public function index()
	{
		
		
		if (!$this->tank_auth->is_logged_in()){
			redirect('/auth/login/');
		}else{

			if ( ! file_exists('application/views/pages/welcome.php'))
			{
				// Whoops, we don't have a page for that!
				show_404();
			}
			
			if(!$this->tank_auth->is_admin())
			{
				redirect('user/dashboard');
			} else {
				redirect('admin/dashboard');
				
			}	
		}
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */