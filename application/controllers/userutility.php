<?php

// application/controllers/base_controller.php
require 'application/controllers/base_controller.php';

class userutility extends base_controller {


	function __construct()
	{
		parent::__construct();

		$this->load->model('tank_auth/TA_groups_users', "", "users");
		$this->load->model('groups');
		$this->load->model('surveys');

	}
	
	function getGroupList(){
		$userid = $this->input->post('userid');
		$filter =  $this->input->post('filter');
		if (empty($filter)){
			// return the list of groups that the user owns
			echo json_encode($this->groups->get_all_groups_with_user($userid));
		} else {
			// return the list of groups that match the filter
			echo json_encode($this->groups->get_filtered_groups_with_user($filter, $userid));
		}
	}

}

?>