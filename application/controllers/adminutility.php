<?php

// application/controllers/admin-utility.php
require 'application/controllers/admin_base.php';

class AdminUtility extends Admin_Base {


	function __construct()
	{
		parent::__construct();

		$this->load->model('tank_auth/TA_groups_users', "", "users");
		$this->load->model('groups');
		$this->load->model('surveys');
		$this->lang->load('tank_auth');
		

	}
	
	function generateEmailList(){
		$group_id = $this->input->post('group_id');
		$userArray = $this->users->get_participating_users($group_id);
		if (ISSET ($userArray)){
			foreach ($userArray as $user){
				echo $user['email'].', ';
			}
		} else {
			echo "Group has no members";
		}

	}
	
	function swapParentFieldUp($order){
		echo $this->surveys->swap_parent_field_up($order);
	}
	
	function swapParentFieldDown($order){
		echo $this->surveys->swap_parent_field_down($order);
	}
	
	function swapOptionUp($order){
		echo $this->surveys->swap_option_up($order);
	}
	
	function swapOptionDown($order){
		echo $this->surveys->swap_option_down($order);
	}
	
	function editGroupDetails(){
		// set rules
		// $this->form_validation->set_rules(EXACT_NAME, HUMAN_READABLE_NAME, RULE);
		$this->form_validation->set_rules('group_description', "Group Description", 'xss_clean');
		$group_id = $this->input->post('group_id');
		$title = $this->input->post('title');
		
		$jsonObject = array();
			
		if ($this->form_validation->run()) {								// validation ok
			$group_description = $this->form_validation->set_value('group_description');
			if (!ISSET($group_description)){
				$group_description = '';
			}
			if ($this->groups->update_group($group_id, $title, $group_description)) {	// success
				$jsonObject['success'] = true;
				$jsonObject['message'] = '<p class="text-success">Success!</p>';
			} else {
				$jsonObject['success'] = false;
				$jsonObject['message'] = '<p class="text-error">Failure to update group.</p>';
			}
		} else {
			$jsonObject['success'] = false;
			$jsonObject['message'] = validation_errors('<p class="text-error">');				
		}
		
		if ($jsonObject['success'] == true){
			$jsonObject['group_description'] = $this->form_validation->set_value('group_description');
		} else {
			$jsonObject['group_description'] = '';
		}
		
		echo json_encode($jsonObject);
	}
	
	function createGroup(){
		$logSuccess = $this->input->post('logSuccess');
		$this->form_validation->set_rules('title', "Group Title", 'required|min_length[3]|xss_clean|is_unique[groups.groupname]');
		$this->form_validation->set_rules('group_description', "Group Description", 'trim|xss_clean');
			
		if ($this->form_validation->run()) {								// validation ok	
			$group_description = $this->form_validation->set_value('group_description');
			if (!ISSET($group_description)){
				$group_description = '';
			}
			if ($this->groups->insert_group($this->form_validation->set_value('title'), 
					$group_description) > 0) {									// success
				if ($logSuccess == 'true'){
					echo '<p class="text-success">';
					echo 'Success! Refresh the group filter to see the group.';
					echo "</p>";
				}
			} else {
				echo '<p class="text-error">';
				echo 'Failure to insert group into the database';
				echo "</p>";
			}
		} else {
			echo validation_errors('<p class="text-error">');
		}
	
	}
	
	/**
	 * Used for creating users through the admin functions
	 */
	function createUser(){
		$use_username = $this->config->item('use_username', 'tank_auth');
		$email_activation = $this->config->item('email_activation', 'tank_auth');
		$logSuccess = $this->input->post('logSuccess');
		
		if ($use_username) {
			$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean|min_length['.$this->config->item('username_min_length', 'tank_auth').']|max_length['.$this->config->item('username_max_length', 'tank_auth').']|alpha_dash');
		}
		$this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');
		if ($this->form_validation->run()) {								// validation ok
			if (!is_null($data = $this->tank_auth->create_user(
					$use_username ? $this->form_validation->set_value('username') : '',
					$this->form_validation->set_value('email'),
					//generate 8 char password and store it
    				substr(uniqid(),2,8),
					$email_activation))) {									// success
				if ($logSuccess == 'true'){
					echo '<p class="text-success">';
					echo 'Success!';
					echo "</p>";
				}
			} else {
					$errors = $this->tank_auth->get_error_message();
					foreach ($errors as $k => $v){
						$data['errors'][$k] = $this->lang->line($v);
						if ($k == "username" || $k=="email"){
							echo '<p class="text-warning">';
						} else {
							echo '<p class="text-error">';
						}
						echo $this->lang->line($v);
						echo '</p>';
					}
			}
		} else {
			echo validation_errors('<p class="text-error">');
		}
		
	}
	
	/**
	 * Used for displaying survey question options
	 */
	function searchOptions(){
		$questionid=  $this->input->post('questionid');
		echo json_encode($this->surveys->get_options_by_questionid($questionid));
	}
	
	function promoteUser(){
		$userid = $this->input->post('userid');
		echo json_encode($this->users->promoteUser($userid));
	}
	
	/**
	 * Used for searches on the user database
	 */
	function updateOption($questionid, $itemid){
		$title=  $this->input->post('title');
		$this->surveys->update_option($itemid, $title);
		echo $questionid;
	}
	
	/**
	 * Used for adding users to groups
	 * @param int $user_id
	 * @param int $groups_id
	 */
	function addUserToGroup($groups_id, $user_id){
		if ($this->users->set_groups_id($user_id, $groups_id)){
			echo "Successfully added user ".$user_id." to group ".$groups_id;
		} else {
			echo "Failed to add user".$user_id." to group ".$groups_id;
		}
	}
	
	/**
	 * Used for adding users to groups
	 */
	function addUserToGroupUsingEmail(){
		$groups_id = $this->input->post('groupid');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');
		if ($this->form_validation->run()) {								// validation ok
			if (!is_null($users = $this->users->get_users_by_email(
					$this->form_validation->set_value('email')))) {									// success
				if ($this->users->set_groups_id($users[0]['id'], $groups_id)){
					//success
				}
			} else {
				echo '<p class="text-warning">';
				echo 'Member does not exist';
				echo '</p>';
			}
		} else {
			echo validation_errors('<p class="text-error">');
		}
	}
	
	/**
	 * Used for adding users to groups
	 * @param int $user_id
	 * @param int $groups_id
	 */
	function addUserToParticipants(){
		$survey_id=  $this->input->post('survey_id');
		$user_id=  $this->input->post('user_id');
		$group_id = $this->input->post('group_id');
		if ($this->users->set_groups_id($user_id, $group_id)){
			echo "Successfully added user ".$user_id." as a participant for ".$survey_id;
		} else {
			echo "Failed to add user ".$user_id." as a participant for ".$survey_id;
		}
	}
	
	/**
	 * Used for adding users to groups
	 * @param int $user_id
	 * @param int $groups_id
	 */
	function addGroupToParticipants(){
		$survey_id=  $this->input->post('survey_id');
		$group_id=  $this->input->post('group_id');
		if ($this->surveys->add_group_participant($group_id, $survey_id)){
			echo "Successfully added group ".$group_id." as a participant for ".$survey_id;
		} else {
			echo "Failed to add group ".$group_id." as a participant for ".$survey_id;
		}
	}
	
	/**
	 * Used for removing users from groups
	 * @param int $user_id
	 * @param int $groups_id
	 */
	function removeUserFromParticipants(){
		$survey_id=  $this->input->post('survey_id');
		$user_id=  $this->input->post('user_id');
		$group_id = $this->input->post('group_id');
		$this->users->remove_participating_user($user_id, $group_id);
	}
	
	/**
	 * Used for removing users from groups
	 * @param int $user_id
	 * @param int $groups_id
	 */
	function removeGroupFromParticipants(){
		$survey_id=  $this->input->post('survey_id');
		$group_id=  $this->input->post('group_id');
		$this->surveys->remove_participating_group($group_id, $survey_id);
	}
	
	
	function reloadGroupParticipants(){
		$survey_id=  $this->input->post('survey_id');
		echo json_encode($this->surveys->get_participating_groups($survey_id));
	}
	
	function reloadUserParticipants(){
		$survey_id =  $this->input->post('survey_id');
		$group_id = $this->input->post('group_id');
		// users registered to a survey as participants are linked to the default group
		echo json_encode($this->users->get_participating_users($group_id));
	}
	
	function getSurveyList(){
		$filter =  $this->input->post('filter');
		if (empty($filter)){
			// return the list of surveys that the user owns
			echo json_encode($this->surveys->get_all_surveys($this->tank_auth->get_user_id()));
		} else {
			// return the list of surveys that match the filter that the user owns
			echo json_encode($this->surveys->get_filtered_surveys($this->tank_auth->get_user_id(), $filter));
		}

	}
	
	function getGroupList(){
		$filter =  $this->input->post('filter');
		if (empty($filter)){
			// return the list of groups that the user owns
			echo json_encode($this->groups->get_all_groups());
		} else {
			// return the list of groups that match the filter
			echo json_encode($this->groups->get_filtered_groups($filter));
		}
	}
		

	function getGroupUserList(){
		$groups_id = $this->input->post('groupid');
		$filter =  $this->input->post('filter');
		if (empty($filter)){
			// return the list of surveys that the user owns
				echo json_encode($this->users->get_participating_users($groups_id));
		} else {
			// return the list of surveys that match the filter that the user owns
			echo json_encode($this->users->get_filtered_participating_users($groups_id, $filter));
		}
	}
	
	function removeFromGroup(){
		$groups_id = $this->input->post('groupid');
		$user_id =  $this->input->post('userid');
		$success = $this->groups->remove_user($groups_id, $user_id);
		echo $success;
	}
	
	function getUserList(){
		$filter =  $this->input->post('filter');
		if (empty($filter)){
			// return the list of surveys that the user owns
			echo json_encode($this->users->get_all_users());
		} else {
			// return the list of surveys that match the filter that the user owns
			echo json_encode($this->users->get_filtered_users($filter));
		}
	
	}
	
	/**
	 * Adds a new option for question with questionid under option with itemid
	 * @param integer $questionid
	 */
	function addOption($questionid){
		$this->surveys->add_option($questionid);
		echo $questionid; //respond with the question that needs to be reloaded
	}
	
	/**
	 * Removes an option with itemid for question with questionid
	 * @param integer $questionid
	 * @param integer $itemid
	 */
	function removeOption($questionid, $itemid){
		$this->surveys->delete_option($itemid);
		// if no options exist anymore, add default option "other"
		if (!($this->surveys->hasOptions($questionid))){
			$this->surveys->add_default_option($questionid);
		}
		
		echo $questionid; //respond with the question that needs to be reloaded
	}
	
	function deleteField($survey_id, $parent_field_id){
		$this->surveys->delete_parent_field($parent_field_id);
		redirect('admin/editSurvey/'.$survey_id);
	}
	
	function viewIndividualResponses(){
		$survey_id =  $this->input->post('survey_id');
		$anonymous = $this->input->post('anonymous');
		if ($anonymous == 0){
			echo json_encode($this->surveys->find_responses_by_uuid($survey_id));
		} else {
			echo json_encode($this->surveys->find_unlinked_responses_by_uuid($survey_id));
		}
	}
	
	function loadQuestionNavigator(){
		$survey_id =  $this->input->post('survey_id');
		echo json_encode($this->surveys->get_survey_questions_by_surveyid($survey_id));
	}
	
	
	function getCompletedParticipants(){
		$survey_id =  $this->input->post('survey_id');
		echo json_encode($this->surveys->get_completed_participants($survey_id, true));
	}
	
	function getNotCompletedParticipants(){
		$survey_id =  $this->input->post('survey_id');
		 echo json_encode($this->surveys->get_not_completed_participants($survey_id));
		// echo $this->surveys->get_not_completed_participants($survey_id, true);
	}
	
	function renderQuestionSummary(){
		$questionid =  $this->input->post('question_id');
		$input_type =  $this->input->post('input_type');
		if (($input_type == "TEXT") || ($input_type == "LONG_ANSWER")){
			$this->renderTextSummary($questionid);
		} else if ($input_type == "NUMERIC"){
			$this->renderNumericSummary($questionid);
		} else if (($input_type == "DROPDOWN") || ($input_type == "MULTIPLE_CHOICE")){
			$this->simpleOptionsSummary($questionid);
		} else if ($input_type == "CHECKBOXES"){
			$this->complexOptionsSummary($questionid);
		}
		
	}
	
	function renderTextSummary($questionid){
		// get all submitted survey responses with question_id = $questionid
		$responses = $this->surveys->get_responses_by_questionid($questionid, true);
		
		$emptyResponseCount = 0;
		$numberOfResponses = 0;
		
		if (ISSET ($responses)){
			// build the response table
			$responseEcho = '';
			$responseEcho = $responseEcho."<div class='dynamic-survey-div-scroll'><table class='table table-hover'><tbody>";
			foreach ($responses as $response){
				$numberOfResponses = $numberOfResponses + 1;
				if (empty($response['answerdescription'])){
					$emptyResponseCount = $emptyResponseCount + 1;
				} else {
					$responseEcho = $responseEcho."<tr><td>".$response['answerdescription']."</td></tr>";
				}
			}
			$responseEcho = $responseEcho."<tbody></table></div>";
			
			echo '<p class="muted">'.($numberOfResponses-$emptyResponseCount).' valid responses from '.$numberOfResponses.' total responses</p>';
			
// 			// echo out the empty response count
// 			if ($emptyResponseCount > 0){
// 				echo '<div class="alert alert-error">';
// 				echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
// 				echo "Number of empty responses: ".$emptyResponseCount;
// 				echo '</div>';
// 			}
			
			// echo out the table
			echo $responseEcho;
		}
	}
	
	function renderNumericSummary($questionid){
		// get all submitted survey responses with question_id = $questionid
		$responses = $this->surveys->get_responses_with_count_by_questionid($questionid, true);
				
		$emptyResponseCount = 0;
		$numberOfResponses = 0;
		
		// echo out a table
		if (ISSET ($responses)){
			// build the response table
			$responseEcho = '';
			$responseEcho = $responseEcho."<div class='dynamic-survey-div-scroll'><table class='table table-hover'>";
			$responseEcho = $responseEcho."<thead><tr><th>Response</th><th>Count</th></tr></thead><tbody>";
			foreach ($responses as $response){
				if ((empty($response['answerdescription']) && ($response['count'] > 0))){
					$emptyResponseCount = $response['count'];
				} else {
					$responseEcho = $responseEcho."<tr><td>".$response['answerdescription']."</td><td>".$response['count']."</td></tr>";
				}
				$numberOfResponses = $numberOfResponses + $response['count'];
			}
			$responseEcho = $responseEcho."<tbody></table></div>";
			
			echo '<p class="muted">'.($numberOfResponses-$emptyResponseCount).' valid responses from '.$numberOfResponses.' total responses</p>';
						
// 			// echo out the empty response count
// 			if ($emptyResponseCount > 0){
// 				echo '<div class="alert alert-error">';
// 				echo "Number of empty responses: ".$emptyResponseCount;
// 				echo '</div>';
// 			}
				
			// echo out the table
			echo $responseEcho;
		}
	}
	
	function simpleOptionsSummary($questionid){
		// get all submitted survey responses with question_id = $questionid
		$responses = $this->surveys->get_responses_with_count_by_questionid($questionid, true, true);
		// echo out a table with progress bars
		if (ISSET ($responses)){
			$totalResponses = 0;
			$maxResponse = 0;
			$emptyResponseCount = 0;
			foreach ($responses as $response){
				if (empty($response['answerdescription'])){
					$emptyResponseCount = $response['count'];
				} else {
					$totalResponses = $totalResponses + $response['count'];
					if ($response['count'] > $maxResponse){
						$maxResponse = $response['count'];
					}
				}
			}
			
			echo '<p class="muted">'.$totalResponses.' valid responses from '.($totalResponses+$emptyResponseCount).' total responses</p>';
							
// 			// echo out the empty response count
// 			if ($emptyResponseCount > 0){
// 				echo '<div class="alert alert-error">';
// 				echo "Number of empty responses: ".$emptyResponseCount;
// 				echo '</div>';
// 			}
			
			if ($totalResponses > 0){
				echo "<div class='dynamic-survey-div-scroll'><table class='table table-hover'>";
				echo "<thead><tr><th>Response</th><th>Count</th></tr></thead><tbody>";
				foreach ($responses as $response){
					if (!empty($response['answerdescription'])){
						echo "<tr><td>";
						echo $response['answerdescription'];
						echo "<br>";
						echo "<br>";
						echo '<div class="progress">';
						$percentage = $response['count']*100/$totalResponses;
						$relativePercentage = $response['count']*100/$maxResponse;
						echo '<div class="bar" style="width: '.$relativePercentage.'%;"></div>';
						echo '</div>';
						echo "</td><td>";
						echo $response['count']." (".$percentage."%)";
						echo "</td></tr>";
					}
				}
				echo "<tbody></table></div>";
			} else {
				echo "There are no responses for this question";
			}
		}
	}
	
	function complexOptionsSummary($questionid){
		// get all submitted survey responses with question_id = $questionid
		$unprocessedResponses = $this->surveys->get_responses_by_questionid($questionid, true);		
		// echo out a table with progress bars
		if (ISSET ($unprocessedResponses)){
			
			// explode the responses, and build a responses array
			$responses = array();		
			foreach ($unprocessedResponses as $unprocessedResponse){
				$value = $unprocessedResponse['answerdescription'];
				$valueArray = explode(',',$value); // converts "1,2,3" into array(1,2,3)
				foreach ($valueArray as $value){
					if (array_key_exists($value, $responses)){
						$responses[$value]['count'] = $responses[$value]['count'] + 1;
					} else {
						$responses[$value]['answerdescription'] =  $value;
						$responses[$value]['count'] =  1;
					}
				}
			}
			
			$totalResponses = 0;
			$maxResponse = 0;
			$emptyResponseCount = 0;
				
			foreach ($responses as $response){
				if (empty($response['answerdescription'])){
					$emptyResponseCount = $response['count'];
				} else {
					$totalResponses = $totalResponses + $response['count'];
					if ($response['count'] > $maxResponse){
						$maxResponse = $response['count'];
					}
				}
			}
			
			echo '<p class="muted">'.$totalResponses.' valid responses from '.($totalResponses+$emptyResponseCount).' total responses</p>';
						
// 			// echo out the empty response count
// 			if ($emptyResponseCount > 0){
// 				echo '<div class="alert alert-error">';
// 				echo "Number of empty responses: ".$emptyResponseCount;
// 				echo '</div>';
// 			}
			
			if ($totalResponses > 0){
				echo "<div class='dynamic-survey-div-scroll'><table class='table table-hover'>";
				echo "<thead><tr><th>Response</th><th>Count</th></tr></thead><tbody>";
				foreach ($responses as $response){
					if (!empty($response['answerdescription'])){
						echo "<tr><td>";
						echo $response['answerdescription'];
						echo "<br>";
						echo "<br>";
						echo '<div class="progress">';
						$percentage = $response['count']*100/$totalResponses;
						$relativePercentage = $response['count']*100/$maxResponse;
						echo '<div class="bar" style="width: '.$relativePercentage.'%;"></div>';
						echo '</div>';
						echo "</td><td>";
						echo $response['count']." (".$percentage."%)";
						echo "</td></tr>";
					}
				}
				echo "<tbody></table></div>";
			} else {
				echo "There are no responses for this question";
			}
		}
	}
	
}

?>