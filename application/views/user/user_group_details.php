


<?php 
$createGroupAttributes = array('title' => 'Create group!', 'class' => 'btn btn-primary btn-large');
?>
<div id=content>

	<div id="group-user-hero-unit" class="codeigniter-hero-unit">
		<div class="row-fluid">
			<div class="span8">
				<h2>
					<span class="subject"><?php echo $username ?> </span> Group Details
				</h2>
			</div>
			<div id="filter-div" class="span4">
				<h4 class="text-info">Filter Groups</h4>
				<label for="group-search">Group Search: </label> <input type="text"
					name="group-search" id="group-search" /> <br> <span
					id="filter-groups" class="btn">Filter Groups</span> <span
					id="show-all-groups" class="btn">Clear filter</span>
			</div>
		</div>
	</div>

	<div id="alert-div" class="row-fluid"></div>

	<div id="group-user-table-div"
		class="div-container-border dynamic-survey-div-scroll">
		<table id="group-user-table" class="table table-hover">
			<thead>
				<tr>
					<th>Group Name</th>
					<th>Group Type</th>
					<th>Description</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>

</div>

<script type="text/javascript">
    site_url = '<?=site_url()?>';
    userid = '<?php echo $userid?>';
</script>
<script>
    $(document).ready(function(){
    	displayAllGroups();
        
    	$("#filter-div").on("click", "#show-all-groups",function(){
        	displayAllGroups();
    	});

    	$("#filter-div").on("click", "#filter-groups",function(){
        	displayGroupsWithFilter();
    	});
    	
		function displayAlert(message){
			clearAlerts();
			var alert = '<div class="alert">'
				+ '<button type="button" class="close" data-dismiss="alert">&times;</button>'
			  	+ '<strong>' + message + '</strong>'
				+ '</div>';
			$("#alert-div").html(alert); //add alert
		}

		function clearAlerts(){
			$("#alert-div").html(""); //clear alerts
		}

		function displayGroupsWithFilter(){
			var textSearch = $("#group-search").val();
			if (textSearch == ''){
				// no search string, display all surveys
				displayAllGroups();
			} else {
				displayGroups(textSearch);
			}
		}

		function displayAllGroups(){
			displayGroups('');
		}
    	
    	function displayGroups(filter){
    			// load groupss
    		     $.ajax({
    	            type: "post",
    	            dataType: 'json',
    	            url: site_url+'/userutility/getGroupList',
    	            cache: false,            
    	            data: {filter : filter, userid : userid},
    	            success: function(response){
    	                $('#group-user-table > tbody').html(""); // clear existing controls
    	            	var obj = response;
    	                if(obj.length>0){
    	                    try{
    	                        $.each(obj, function(i,group_item){    
        	                        // open table row
    	                        	var tablerow = '<tr>' ;

									// groupname
									tablerow = tablerow + '<td>' + group_item.groupname + '</td>';

									// group type
									if (group_item.is_default_group == 1){
										tablerow = tablerow + '<td><span class="label">Default Group</span></td>';
									} else {
										tablerow = tablerow + '<td><span class="label label-info">Custom Group</span></td>';
									}

									// description
									tablerow = tablerow + '<td>' + group_item.text + '</td>';
																
									// close tablerow
									tablerow = tablerow + '</tr>';

    	                            	
    	                            $('#group-user-table > tbody').append( tablerow );
    	                        }); 
    	                        // successful loop, clear alerts
    	                        clearAlerts();
    	                    }catch(e) {     
    	                        alert('Exception while request..');
    	                    }       
    	                }else{
    	                	displayAlert("No groups found."); 
    	                }                       
    	            },
    	            error: function(){                      
    	                alert('Error while request..');
    	            }
    	        });
    	}

    	
    });    

</script>
