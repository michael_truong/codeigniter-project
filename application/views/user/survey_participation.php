
<div class="codeigniter-hero-unit">
	<h2>
		Manage Survey Participation for
		<span class="subject"><?php echo $username ?></span>
	</h2>
</div>
<!--  <p> debug stuff <?php var_dump($what)?></p>
<p> debug stuff <?php var_dump($debug)?></p>
-->

<div class="row-fluid">

	<div id="active-surveys" class="container div-container-border span6">
		<h4 class="text-info">Active Surveys</h4>
		<div class="edit-field-div-scroll">
		
		<?php if(ISSET ($active_surveys)){?>
			<table class="table table-hover">
				<thead>
					<th>Survey Name</th>
					<th>State</th>
					<th>Operations</th>
				</thead>
				<tbody>
					<?php foreach ($active_surveys as $survey): ?>
					<?php if ($survey['survey_state'] == "STARTED"){?>
						<tr>
							<td><i> <?php echo $survey['surveyname'] ?>
							</i> <?php if ($survey['anonymous']){?>
								<a class="text-info">(Anonymous)</a>
							<?php }?>
							</td>

							<td>Not Started/Partially Finished</td>
							<td><?php echo anchor("survey/respondToSurvey/".$survey['surveyid'], 'Start/Resume
						Survey', array('class' => 'btn btn-small')); ?></td>
						</tr>
					<?php }?>
					<?php endforeach ?>
				</tbody>

			</table>


			<?php } else {?>
			<p>This member is not registered for any surveys.</p>




			<?php }?>
		</div>



	</div>

	<div id="completed-surveys"
		class="container div-container-border span6">
		<h4 class="text-info">Completed Surveys</h4>
		<div class="edit-field-div-scroll">

			<?php if(ISSET ($completed_surveys)){?>

			<table class="table table-hover">
				<thead>
					<th>Survey Name</th>
					<th>Operations</th>
				</thead>
				<tbody>
					<?php foreach ($completed_surveys as $survey): ?>
					<tr>
						<td><i> <?php echo $survey['surveyname'] ?>
						</i> <?php if ($survey['anonymous']){?>
								<a class="text-info">(Anonymous)</a>
							<?php }?>
						</td>

						<td>
						<?php if (!$survey['anonymous']){?>
							<?php echo anchor("survey/viewSurveyResponse/".$survey['surveyid'], 'View Response', array('class' => 'btn btn-small')); ?></
						<?php }?>
						</td>
					</tr>
					<?php endforeach ?>
				</tbody>

			</table>


			<?php } else {?>
			<p>This member is has not completed any surveys.</p>




			<?php }?>

		</div>
	</div>

</div> 

  
  