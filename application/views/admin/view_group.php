<div id=content>
		<div class="codeigniter-hero-unit">
			<div class="row-fluid">
				<div class="span8">
					<h2>
						<span class="subject"><?php echo $groupname ?></span>
						Members
					</h2>
					<p class="text-info">
						<?php echo $group['text']?>
					<p>
					<p>
						<?php echo anchor("/admin/viewGroup/".$group['id'], 'View
							 Group Members', array('class' => 'btn')); ?>
						<?php echo anchor("admin/addGroupMember/".$group['id'], 'Add member', array('class' => 'btn')); ?> 
						<?php echo anchor("admin/addBatchGroupMembers/".$group['id'], 'Add batch members', array('class' => 'btn')); ?>
					</p>
					<p>
						<?php echo anchor("admin/editGroup/".$group['id'], 'Manage Group', array('class' => 'btn')); ?>
					</p>
				</div>
				<div id="filter-div" class="span4">
					<h4 class="text-info">Filter Group Users</h4>
					<label for="user-search">User Group Search: </label> <input type="text"
									name="user-search" id="user-search" />
					<br>
					<span id="filter-users" class="btn">Filter Group Users</span>
					<span id="show-all-users" class="btn">Clear filter</span>
				</div>
			</div>
		</div>
		
		
	<div id="alert-div" class="row-fluid"></div>
	
	<div id="user-admin-table-div" class="div-container-border dynamic-survey-div-scroll">
		<table id="user-admin-table" class="table table-hover">
		<thead>
			<tr>
			<th>User Name</th>
			<th>User Email</th>
			<th>Operations</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
    site_url = '<?=site_url()?>';
    groupid = '<?php echo $group['id']?>';
</script>
<script>
    $(document).ready(function(){
    	displayAllUsers();
        
    	$("#filter-div").on("click", "#show-all-users",function(){
        	displayAllUsers();
    	});

    	$("#filter-div").on("click", "#filter-users",function(){
        	displayUsersWithFilter();
    	});

    	$("#user-admin-table-div").on("click",".remove-user",function(){
        	removeUser($(this));
    	});

    	function removeUser(removeButton){
			var userid = removeButton.attr('user');			
			$.ajax({
	            type: "post",
	            dataType: 'json',
	            url: site_url+'/adminutility/removeFromGroup',
	            cache: false,            
	            data: {groupid:groupid, userid : userid},
	            success: function(response){
		            $("#row-"+removeButton.attr('user')).remove();
	            },
	            error: function(){                      
	                alert('Error while request..');
	            }
	        });
        }
    	
		function displayAlert(message){
			clearAlerts();
			var alert = '<div class="alert">'
				+ '<button type="button" class="close" data-dismiss="alert">&times;</button>'
			  	+ '<strong>' + message + '</strong>'
				+ '</div>';
			$("#alert-div").html(alert); //add alert
		}

		function clearAlerts(){
			$("#alert-div").html(""); //clear alerts
		}

		function displayUsersWithFilter(){
			var textSearch = $("#user-search").val();
			if (textSearch == ''){
				// no search string, display all surveys
				displayAllUsers();
			} else {
				displayUsers(textSearch);
			}
		}

		function displayAllUsers(){
			displayUsers('');
		}
    	
    	function displayUsers(filter){
    			// load surveys
    		     $.ajax({
    	            type: "post",
    	            dataType: 'json',
    	            url: site_url+'/adminutility/getGroupUserList',
    	            cache: false,            
    	            data: {groupid:groupid, filter : filter},
    	            success: function(response){
    	                $('#user-admin-table > tbody').html(""); // clear existing controls
    	            	var obj = response;
    	                if(obj.length>0){
    	                    try{
    	                        $.each(obj, function(i,user_item){    
        	                        // open table row
    	                        	var tablerow = '<tr id="row-'+user_item.id+'">' ;

									// add username
									tablerow = tablerow + '<td>'+user_item.username;

									// append priviledge level
									if (user_item.group_id == 100){
										tablerow = tablerow + '<em class="text-info"> (admin)</em></td>';
										
									} else {
										tablerow = tablerow +'</td>';
									}
									
    	                        	// add email
									tablerow = tablerow + '<td>'+user_item.email+'</td>';

									// add operations
									tablerow = tablerow + '<td><a class="btn btn-small btn-danger remove-user" user="'+user_item.id+'">Unlink User</a></td>';
											
									// close tablerow
									tablerow = tablerow + '</tr>';

    	                            	
    	                            $('#user-admin-table > tbody').append( tablerow );
    	                        }); 
    	                        // successful loop, clear alerts
    	                        clearAlerts();
    	                    }catch(e) {     
    	                        alert('Exception while request..');
    	                    }       
    	                }else{
    	                	displayAlert("No user found."); 
    	                }                       
    	            },
    	            error: function(){                      
    	                alert('Error while request..');
    	            }
    	        });
    	}

    	
    });    

</script>