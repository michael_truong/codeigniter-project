<?php 
$group_title = array(
		'label' => 'Group Title',
		'name'	=> 'title',
		'id'	=> 'title',
		'maxlength'	=> 80,
		'size'	=> 30,
);
$group_description = array(
		'label' => "Group Description",
		'name'	=> 'group_description',
		'id'	=> 'group_description',
		'rows'	=> 5,
		'columns'		=> 10,
);
?>

<div id=content>

	<div id="group-admin-hero-unit" class="codeigniter-hero-unit">
		<div class="row-fluid">
			<div class="span8">
				<h2>Group Admin</h2>
				As an administrator, you may manage groups from this page.
				<p></p>
			</div>
			<div id="filter-div" class="span4">
				<h4 class="text-info">Filter Groups</h4>
				<label for="group-search">Group Search: </label> <input type="text"
								name="group-search" id="group-search" />
				<br>
				<span id="filter-groups" class="btn">Filter Groups</span>
				<span id="show-all-groups" class="btn">Clear filter</span>
			</div>
		</div>
	</div>

	<div id="alert-div" class="row-fluid"></div>
			
	<div class="row-fluid">	
		<div id="group-admin-table-div" class="span8 div-container-border dynamic-survey-div-scroll">
			<table id="group-admin-table" class="table table-hover">
			<thead>
				<tr>
				<th>Group Name</th>
				<th>Group Type</th>
				<th>Operations</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
			</table>
		</div>
		<div id="group-edit" class="span4">
			<div id="create-group-div" class="div-container-border">
				<h4 class="text-info">Create Group</h4>
				<table>
					<tr>
						<td><?php echo form_label('Group Title', $group_title['id']); ?></td>
						<td><?php echo form_input($group_title); ?></td>
						<td style="color: red;"><?php echo form_error($group_title['name']); ?><?php echo isset($errors[$group_title['name']])?$errors[$group_title['name']]:''; ?></td>
					</tr>
					<tr>
						<td><?php echo form_label('Group Description', $group_description['id']); ?></td>
						<td><?php echo form_textarea($group_description); ?></td>
						<td style="color: red;"><?php echo form_error($group_description['name']); ?><?php echo isset($errors[$group_description['name']])?$errors[$group_description['name']]:''; ?></td>
					</tr>
				</table>
				<span id="create-group-btn" class="btn btn-primary">Create Group</span>
			</div>
			<section class="div-container-border">
				<h4 class="text-info">Log</h4>
				<div id="create-group-log" class="edit-field-div-scroll">
				</div>
			</section>
		</div>
	</div>
</div>

<script type="text/javascript">
    site_url = '<?=site_url()?>';
</script>
<script>
    $(document).ready(function(){
    	displayAllGroups();
        
    	$("#filter-div").on("click", "#show-all-groups",function(){
        	displayAllGroups();
    	});

    	$("#filter-div").on("click", "#filter-groups",function(){
        	displayGroupsWithFilter();
    	});
    	
		function displayAlert(message){
			clearAlerts();
			var alert = '<div class="alert">'
				+ '<button type="button" class="close" data-dismiss="alert">&times;</button>'
			  	+ '<strong>' + message + '</strong>'
				+ '</div>';
			$("#alert-div").html(alert); //add alert
		}

		function clearAlerts(){
			$("#alert-div").html(""); //clear alerts
		}

		function displayGroupsWithFilter(){
			var textSearch = $("#group-search").val();
			if (textSearch == ''){
				// no search string, display all surveys
				displayAllGroups();
			} else {
				displayGroups(textSearch);
			}
		}

		function displayAllGroups(){
			displayGroups('');
		}
    	
    	function displayGroups(filter){
    			// load groupss
    		     $.ajax({
    	            type: "post",
    	            dataType: 'json',
    	            url: site_url+'/adminutility/getGroupList',
    	            cache: false,            
    	            data: {filter : filter},
    	            success: function(response){
    	                $('#group-admin-table > tbody').html(""); // clear existing controls
    	            	var obj = response;
    	                if(obj.length>0){
    	                    try{
    	                        $.each(obj, function(i,group_item){    
        	                        // open table row
    	                        	var tablerow = '<tr>' ;

									// groupname
									tablerow = tablerow + '<td>' + group_item.groupname + '</td>';

									// group type
									if (group_item.is_default_group == 1){
										tablerow = tablerow + '<td><span class="label">Default Group</span></td>';
									} else {
										tablerow = tablerow + '<td><span class="label label-info">Custom Group</span></td>';
									}

									// operations
									tablerow = tablerow + '<td>';
									tablerow = tablerow + '<a href="' + site_url + '/admin/viewGroup/' + group_item.id +'" class="btn"> Manage Members </a>';
									tablerow = tablerow + '<a href="' + site_url + '/admin/editGroup/' + group_item.id +'" class="btn"> Manage Group </a>';
									tablerow = tablerow + '</td>';
																			
																
									// close tablerow
									tablerow = tablerow + '</tr>';

    	                            	
    	                            $('#group-admin-table > tbody').append( tablerow );
    	                        }); 
    	                        // successful loop, clear alerts
    	                        clearAlerts();
    	                    }catch(e) {     
    	                        alert('Exception while request..');
    	                    }       
    	                }else{
    	                	displayAlert("No groups found."); 
    	                }                       
    	            },
    	            error: function(){                      
    	                alert('Error while request..');
    	            }
    	        });
    	}

    	
    });    

</script>
<script>
    $(document).ready(function(){        
    	$("#create-group-div").on("click", "#create-group-btn",function(){
        	createGroup();
    	});

    	function createGroup(){
			title = $('#title').val();
			group_description = $('#group_description').val();
	    	$('#create-group-log').text(''); // clear previous log
            
    		     $.ajax({
    	            type: "post",
    	            dataType: 'html',
    	            url: site_url+'/adminutility/createGroup',
    	            cache: false,            
    	            data: {title : title, group_description : group_description, logSuccess: true},
    	            success: function(response){
        	            if (response.length != 0){ // only log messages that are relevant
      	            	  var now = new Date(); 
    	            	  var then = now.getFullYear()+'-'+(now.getMonth()+1)+'-'+now.getDay(); 
    	            	      then += ' '+now.getHours()+':'+now.getMinutes()+':'+now.getSeconds();

    	    	    	    	$('#create-group-log').append('<p class="muted">'+ then +' for input (title => '+title+', description => '+group_description+')</p>'); // append time
    	      	            	$('#create-group-log').append(response); // append new logs
            	        }
    	            },
    	            error: function(){                      
    	                alert('Error while request..');
    	            }
    	        });
    	}
    });    

</script>

