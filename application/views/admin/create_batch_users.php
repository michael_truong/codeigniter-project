<style>
  #progress_bar {
    margin: 10px 0;
    padding: 3px;
    border: 1px solid #000;
    font-size: 14px;
    clear: both;
    opacity: 0;
    -moz-transition: opacity 1s linear;
    -o-transition: opacity 1s linear;
    -webkit-transition: opacity 1s linear;
  }
  #progress_bar.loading {
    opacity: 1.0;
  }
  #progress_bar .percent {
    background-color: #99ccff;
    height: auto;
    width: 0;
  }
</style>


	<div class="codeigniter-hero-unit">
		<div class="row-fluid">
			<div class="span8">
			<h2>Create Multiple Users</h2>
			<p>
				<?php echo anchor("admin/listSystemUsers", 'View
					 System Users', array('class' => 'btn')); ?>
				<?php echo anchor("admin/createUser", 'Create
					Single User', array('class' => 'btn')); ?> 
				<?php echo anchor("admin/createUserBatch", 'Create
						Multiple Users', array('class' => 'btn')); ?>
			</p>
			</div>
			<div id="create-user-div" class="span4">
				<h4 class="text-info">Create Multiple Users from File</h4>
				<table>
					<tr>
						<td>				
							<p class="muted">
								<?php if ($use_username) { ?>
									Upload a comma separated file with usernames and email addresses, and the users will be created with a system provided password.
								<?php } else { ?>
									Upload a comma separated file with email addresses, and the users will be created with a system provided password.
								<?php } ?>
							</p>
						</td>
					</tr>
				</table>
				<input type="file" id="files" name="file" /> <br>
				<button class="btn btn-danger" onclick="abortRead();">Cancel read</button>
			</div>
		</div>
	</div>
	
<div id="alert-div" class="row-fluid"></div>
	
<div id="progress_bar"><div class="percent">0%</div></div>

<div  class="div-container-border">
	<h4 class="text-info">Log (in reverse chronological order)</h4>
	<div id="create-user-log" class="edit-field-div-scroll">
	</div>
</div>

<script type="text/javascript">
    site_url = '<?=site_url()?>';
    use_username = <?php echo $use_username?>;
    uploadErrors = 0;
    uploadWarnings = 0;
</script>
<!-- FILE READER SCRIPTS -->
<script>
	var reader;
	var progress = document.querySelector('.percent');

    $(document).ready(function(){    

    	checkForFileAPI();
		$(document).on('change','#files', handleFileSelect);

		function checkForFileAPI(){
			// Check for the various File API support.
			if (window.File && window.FileReader && window.FileList && window.Blob) {
			  // Great success! All the File APIs are supported.
			} else {
			  alert('The File APIs are not fully supported in this browser.');
			}
		}

		  function abortRead() {
		    reader.abort();
		  }

		  function displayAlert(message){
				clearAlerts();
				var alert = '<div class="alert alert-info">'
					+ '<button type="button" class="close" data-dismiss="alert">&times;</button>'
				  	+ '<strong>' + message + '</strong>'
					+ '</div>';
				$("#alert-div").html(alert); //add alert
			}

			function clearAlerts(){
				$("#alert-div").html(""); //clear alerts
			}

		  function errorHandler(evt) {
		    switch(evt.target.error.code) {
		      case evt.target.error.NOT_FOUND_ERR:
		        alert('File Not Found!');
		        break;
		      case evt.target.error.NOT_READABLE_ERR:
		        alert('File is not readable');
		        break;
		      case evt.target.error.ABORT_ERR:
		        break; // noop
		      default:
		        alert('An error occurred reading this file.');
		    };
		  }

		  function updateProgress(evt) {
		    // evt is an ProgressEvent.
		    if (evt.lengthComputable) {
		      var percentLoaded = Math.round((evt.loaded / evt.total) * 100);
		      // Increase the progress bar length.
		      if (percentLoaded < 100) {
		        progress.style.width = percentLoaded + '%';
		        progress.textContent = percentLoaded + '%';
		      }
		    }
		  }

		  function handleFileSelect(evt) {
		    // Reset progress indicator on new file selection.
		    progress.style.width = '0%';
		    progress.textContent = '0%';

		    reader = new FileReader();
		    reader.onerror = errorHandler;
		    reader.onprogress = updateProgress;
		    reader.onabort = function(e) {
		      alert('File read cancelled');
		    };
		    reader.onloadstart = function(e) {
		      document.getElementById('progress_bar').className = 'loading';
		    };
		    reader.onload = function(e) {
		      // Ensure that the progress bar displays 100% at the end.
		      progress.style.width = '100%';
		      progress.textContent = '100%';
		      setTimeout("document.getElementById('progress_bar').className='';", 2000);

		      	var text = reader.result;
		      	var lines = text.split(/[\r\n]+/g); // tolerate both Windows and Unix linebreaks

		      	lines.forEach(function(line) { 
		      		grabFileLine(line);
				 });
				 
            	displayAlert("Finished batch user creation."); 

		    };

		    // Read in the file as a binary string.
		    reader.readAsText(evt.target.files[0]);
		  }

			function grabFileLine(line){
				// foreach line
		    	var username = '';
		    	var email = '';
		    	var inputArray = line.split(',');
		    	if (use_username){
					username = inputArray[0];
		            email = inputArray[1];
		        } else {
		            email = inputArray[0];
		        }
		        createUser(username, email);
			}
			
			function createUser(username, email){
					// load surveys
				     $.ajax({
			            type: "post",
			            dataType: 'html',
			            url: site_url+'/adminutility/createUser',
			            cache: false,            
			            data: {username : username, email : email, logSuccess: false},
			            success: function(response){
		    	            if (response.length != 0){ // only log messages that are relevant
								handleResponse(response, username, email);
		    	            }
			 	        },
			            error: function(){                      
			                alert('Error while request..');
			            }
			        });
			}

			function handleResponse(response, username, email){
				// figure out type of response
				var log = false;
				if (response.indexOf("text-error") != -1){ // error
				    if (uploadErrors < 100){
					    uploadErrors = uploadErrors + 1;
					    log = true;
					} else if (uploadErrors == 100){
					    uploadErrors = uploadErrors + 1;
	                	displayAlert("Over 100 upload errors logged, error logging will cease."); 
					}
				} else if (response.indexOf("text-warning") != -1){ // warning
				    if (uploadWarnings < 100){
				    	uploadWarnings = uploadWarnings + 1;
					    log = true;
					} else if (uploadWarnings == 100){
						uploadWarnings = uploadWarnings + 1;
	                	displayAlert("Over 100 upload warnings logged, warning logging will cease."); 
					}
				} else { // success
					log = true;
				}

				if (log == true){
				  	  var now = new Date(); 
					  var then = now.getFullYear()+'-'+(now.getMonth()+1)+'-'+now.getDay(); 
					      then += ' '+now.getHours()+':'+now.getMinutes()+':'+now.getSeconds();

					$('#create-user-log').prepend('<p class="muted">'+ then +' for input (username => '+username+', email => '+email+')</p>'); // prepend time
					$('#create-user-log').prepend(response); // prepend new logs
				}
			}
			
    });    	
</script>