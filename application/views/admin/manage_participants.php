<script type="text/javascript">
    site_url = '<?=site_url()?>';
</script>
<script type="text/javascript">
    $(document).ready(function(){
		// load user and group participants
		reloadUserParticipants();
		reloadGroupParticipants();
		

        
      $("#user-search").keyup(function(){
        if($("#user-search").val().length>3){
        $.ajax({
            type: "post",
            dataType: 'json',
            url: site_url+'/adminutility/getUserList',
            cache: false,               
            data:'filter='+$("#user-search").val(),
            success: function(response){
                $('#finalUserResult > tbody').html("");
                var obj = response;
                if(obj.length>0){
                    try{
                        $.each(obj, function(i,val){    
                        	var tablerow = '<tr><td>' 
                            	+ val.username 
                            	+ '</td><td>' 
                            	+ val.email
                            	+ '</td><td>' 
                            	+ '<a value="'+val.id+'"class="add-user btn"> Add to survey participants </a>'
                            	+ '</td></tr>';

                        	$('#finalUserResult > tbody').append( tablerow );
                     
                        }); 
                    }catch(e) {     
                        alert('Exception while request..');
                    }       
                }else{
                	var tablerow = '<tr><td>' 
						+ "No Data Found" 
                    	+ '</td><td></td><td></td></tr>';
                    $('#finalUserResult > tbody').append(tablerow);      
                }                       

            },
            error: function(){                      
                alert('Error while request..');
            }
        });
        }
        return false;
      });


      $("#group-search").keyup(function(){
        if($("#group-search").val().length>3){
        $.ajax({
            type: "post",
            dataType: 'json',
            url: site_url+'/adminutility/getGroupList',
            cache: false,               
            data:'filter='+$("#group-search").val(),
            success: function(response){
                $('#finalGroupResult > tbody').html("");
                var obj = response;
                if(obj.length>0){
                    try{
                        $.each(obj, function(i,val){   
							// dont display default groups
                        	if (!(val.is_default_group == 1)) {                        		
  	                            
	                        	var tablerow = '<tr><td>' 
	                            	+ val.groupname
	                            	+ '</td><td>' 
	                            	+ val.text
	                            	+ '</td><td>' 
	                            	+ '<a value="'+val.id+'"class="add-group btn"> Add to survey participants </a>'
	                            	+ '</td></tr>';
	
	                        	$('#finalGroupResult > tbody').append( tablerow );
  							}
                        }); 
                    }catch(e) {     
                        alert('Exception while request..');
                    }       
                }else{
                	var tablerow = '<tr><td>' 
						+ "No Data Found" 
                    	+ '</td><td></td><td></td></tr>';
                    $('#finalGroupResult > tbody').append(tablerow);      
                }                       

            },
            error: function(){                      
                alert('Error while request..');
            }
        });
        }
        return false;
      });

	  function reloadUserParticipants(){
          $.ajax({
              type: "post",
              dataType: 'json',
              url: site_url+ '/adminutility/reloadUserParticipants',
              data: { survey_id : <?php echo $survey['surveyid'] ?>, group_id: <?php echo $survey['default_group'] ?>},
              cache: false,               
              success: function(response){
                  $('#user-participants-table > tbody').html("");
                  var obj = response;
                  if(obj.length>0){
                      try{
                          $.each(obj, function(i,val){    
                          	var tablerow = '<tr><td>' 
                              	+ val.username 
                              	+ '</td><td>' 
                              	+ val.email
                              	+ '</td><td>' 
                              	+ '<a value="'+val.id+'"class="remove-user btn btn-small btn-danger"> Remove </a>'
                              	+ '</td></tr>';

                          	$('#user-participants-table > tbody').append( tablerow );
                       
                          }); 
                      }catch(e) {     
                          alert('Error description: ' + e.message + '\n\n');
                      }       
                  }else{
                  	var tablerow = '<tr><td>' 
  						+ "No Data Found" 
                      	+ '</td><td></td><td></td></tr>';
                      $('#user-participants-table > tbody').append(tablerow);      
                  }                       

              },
              error: function(){                      
                  alert('Error while request..');
              }
          });
	  }

	  function reloadGroupParticipants(){
		  $.ajax({
              type: "post",
              dataType: 'json',
              url: site_url+ '/adminutility/reloadGroupParticipants',
              data: { survey_id : <?php echo $survey['surveyid'] ?>},
              cache: false,               
              success: function(response){
                  $('#group-participants-table > tbody').html("");
                  var obj = response;
                  if(obj.length>0){
                      try{
                          $.each(obj, function(i,val){   
	                        // dont display the default group
                               
  							if (!(val.is_default_group == 1)){
	                          	var tablerow = '<tr><td>' 
	                              	+ val.groupname 
	                              	+ '</td><td>' 
	                              	+ val.text
	                              	+ '</td><td>'
									+ '<a value="'+val.id+'"class="remove-group btn btn-small btn-danger"> Remove </a>'
	                            	+ '</td></tr>';
	                            
	                          	$('#group-participants-table > tbody').append( tablerow );
							}
  	                       
                          }); 
                      }catch(e) {     
                          alert('Error description: ' + e.message + '\n\n');
                      }       
                  }else{
                  	var tablerow = '<tr><td>' 
  						+ "No Group Participants Found" 
                      	+ '</td><td></td><td></td></tr>';
                      $('#group-participants-table > tbody').append(tablerow);      
                  }                       

              },
              error: function(){                      
                  alert('Error while request..');
              }
          });
	  }

	  $("#group-participants-table").on("click", ".remove-group", function(){
          $.ajax({
              type: "post",
              dataType: 'html',
              url: site_url+ '/adminutility/removeGroupFromParticipants',
              data: { survey_id : <?php echo $survey['surveyid'] ?>, group_id : $(this).attr("value")},
              cache: false,               
              success: function(response){
            	  reloadGroupParticipants();
              },
              error: function(){                      
                  alert('Error while request..');
              }
          });
   	  });

	  $("#user-participants-table").on("click", ".remove-user", function(){
          $.ajax({
              type: "post",
              dataType: 'html',
              url: site_url+ '/adminutility/removeUserFromParticipants',
              data: {group_id: <?php echo $survey['default_group'] ?> , survey_id : <?php echo $survey['surveyid'] ?>, user_id : $(this).attr("value")},
              cache: false,               
              success: function(response){
            	  reloadUserParticipants();                    
              },
              error: function(){                      
                  alert('Error while request..');
              }
          });
 	  });
      
      $("#user-search-div").on("click", ".add-user",function(){
          $.ajax({
              type: "post",
              dataType: 'html',
              url: site_url+ '/adminutility/addUserToParticipants',
              data: {group_id: <?php echo $survey['default_group'] ?> , survey_id : <?php echo $survey['surveyid'] ?>, user_id : $(this).attr("value")},
              cache: false,               
              success: function(response){
            	  reloadUserParticipants();                    
              },
              error: function(){                      
                  alert('Error while request..');
              }
          });
        }); 

      $("#group-search-div").on("click", ".add-group",function(){
          $.ajax({
              type: "post",
              dataType: 'html',
              url: site_url+ '/adminutility/addGroupToParticipants',
              data: { survey_id : <?php echo $survey['surveyid'] ?>, group_id : $(this).attr("value")},
              cache: false,               
              success: function(response){
            	  reloadGroupParticipants();
              },
              error: function(){                      
                  alert('Error while request..');
              }
          });
        }); 
            
    });
</script>

<div id=content>
	<div id="main">


		<div class="codeigniter-hero-unit">
			<h2>
				Manage Participants for 
				<span class="subject"><?php echo $survey['surveyname'] ?></span>
			</h2>
			<p>As an administrator, you can manage the survey participants.</p>
					<?php if ($survey['survey_state'] == 'PENDING'){?>
						<?php echo anchor("admin/editSurvey/".$survey['surveyid'], 'Edit
						Survey', array('class' => 'btn')); ?> 
					<?php }?>
		</div>

		<div class="row-fluid">
				<div id="user-participants-div" class="div-container-border span5">
					<h4 class="text-info">User Participants</h4>
					<div class="edit-field-div-scroll">
						<table id="user-participants-table" class="table table-hover">
							<thead>
								<th>User</th>
								<th>Email</th>
								<th>Operations</th>
		
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>

				<div id="add-user-participants" class="div-container-border span7">
					<h4 class="text-info">Add User Participants</h4>
					<label for="user-search">User Search: </label> <input type="text"
						name="user-search" id="user-search" />
					<div id="user-search-div" class="div-scroll">
						<table id="finalUserResult" class="table table-hover">
							<thead>
								<th>User</th>
								<th>Email</th>
								<th>Operations</th>
		
							</thead>
							<tbody>
							</tbody>
						</table>
		
		
					</div>
				</div>
				
		</div>
		<div class="row-fluid">
				<div id="group-participants-div" class="div-container-border span5">
					<h4 class="text-info">Group Participants</h4>
					<div class="edit-field-div-scroll">
						<table id="group-participants-table" class="table table-hover">
							<thead>
								<th>Group</th>
								<th>Description</th>
								<th>Operations</th>
		
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
				
				<div id="add-group-participants" class="div-container-border span7">
					<h4 class="text-info">Add Group Participants</h4>
					<label for="group-search">Group Search: </label> <input type="text"
						name="group-search" id="group-search" />
					<div id="group-search-div" class="div-scroll">
						<table id="finalGroupResult" class="table table-hover">
							<thead>
								<th>Group</th>
								<th>Description</th>
								<th>Operations</th>
		
							</thead>
							<tbody>
							</tbody>
						</table>
		
		
					</div>
				</div>
		</div>
		


		<div id="result" style="color: #606;">
			<?php if (isset($result)) print_r($result); ?>
		</div>

		<?php echo validation_errors(); ?>

	</div>
	<br>

</div>
