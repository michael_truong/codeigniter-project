<?php
if ($use_username) {
	$username = array(
		'name'	=> 'username',
		'id'	=> 'username',
		'value' => set_value('username'),
		'maxlength'	=> $this->config->item('username_max_length', 'tank_auth'),
		'size'	=> 30,
	);
}
$email = array(
	'name'	=> 'email',
	'id'	=> 'email',
	'value'	=> set_value('email'),
	'maxlength'	=> 80,
	'size'	=> 30,
);
?>
	<div class="codeigniter-hero-unit">
		<div class="row-fluid">
			<div class="span8">
			<h2>Create Single User</h2>
			<p>
				<?php echo anchor("admin/listSystemUsers", 'View
					 System Users', array('class' => 'btn')); ?>
				<?php echo anchor("admin/createUser", 'Create
					Single User', array('class' => 'btn')); ?> 
				<?php echo anchor("admin/createUserBatch", 'Create
						Multiple Users', array('class' => 'btn')); ?>
			</p>
			</div>
			<div id="create-user-div" class="span4">
				<h4 class="text-info">Create Single User</h4>
				<p class="muted">Fill in the required fields, and the user will be created with a system provided password.</p>
				<table>
					<?php if ($use_username) { ?>
					<tr>
						<td><?php echo form_label('Username', $username['id']); ?></td>
						<td><?php echo form_input($username); ?></td>
						<td style="color: red;"><?php echo form_error($username['name']); ?><?php echo isset($errors[$username['name']])?$errors[$username['name']]:''; ?></td>
					</tr>
					<?php } ?>
					<tr>
						<td><?php echo form_label('Email Address', $email['id']); ?></td>
						<td><?php echo form_input($email); ?></td>
						<td style="color: red;"><?php echo form_error($email['name']); ?><?php echo isset($errors[$email['name']])?$errors[$email['name']]:''; ?></td>
					</tr>
				</table>
				<span id="create-user-btn" class="btn btn-primary">Create</span>
			</div>
		</div>
	</div>

<div  class="div-container-border">
	<h4 class="text-info">Log</h4>
	<div id="create-user-log" class="edit-field-div-scroll">
	</div>
</div>

<script type="text/javascript">
    site_url = '<?=site_url()?>';
    use_username = <?php echo $use_username?>;
</script>
<script>
    $(document).ready(function(){        
    	$("#create-user-div").on("click", "#create-user-btn",function(){
        	createUser();
    	});

    	function createUser(){
        	var username = '';
        	if (use_username){
				username = $('#username').val();
            }
            email = $('#email').val();
    			// load surveys
    		     $.ajax({
    	            type: "post",
    	            dataType: 'html',
    	            url: site_url+'/adminutility/createUser',
    	            cache: false,            
    	            data: {username : username, email : email, logSuccess: true},
    	            success: function(response){
        	            if (response.length != 0){ // only log messages that are relevant
      	            	  var now = new Date(); 
    	            	  var then = now.getFullYear()+'-'+(now.getMonth()+1)+'-'+now.getDay(); 
    	            	      then += ' '+now.getHours()+':'+now.getMinutes()+':'+now.getSeconds();

    	    	    	    	$('#create-user-log').append('<p class="muted">'+ then +' for input (username => '+username+', email => '+email+')</p>'); // append time
    	      	            	$('#create-user-log').append(response); // append new logs
            	        }
    	            },
    	            error: function(){                      
    	                alert('Error while request..');
    	            }
    	        });
    	}
    });    

</script>