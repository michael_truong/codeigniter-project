<?php
$survey_title = array(
		'label' => 'Survey Title',
		'name'	=> 'title',
		'value' => $survey['surveyname'],
		'maxlength'	=> 80,
		'size'	=> 30,
);
$survey_description = array(
		'label' => "Survey Description",
		'name'	=> 'survey_description',
		'value' => $survey['survey_description'],
		'rows'	=> 5,
		'columns'		=> 10,
);
$anonymous = array(
		'label' => 'Anonymous Survey',
		'name'	=> 'anonymous',
		'value'	=> $survey['anonymous'],
		'checked' => $survey['anonymous'],
		'style' => 'margin:0;padding:0',
);
$form_attributes = array('class' => 'edit_form');
$hidden_fields = array('surveyname' => $survey['surveyname'], 'surveyid' => $survey['surveyid']);
?>
<div id=content>
	<div id="main">


		<div class="codeigniter-hero-unit">
			<h2>
				Edit
				<span class="subject"><?php echo $survey['surveyname'] ?></span>
			</h2>
			<p>As an administrator, you can update the survey title and description.</p>
			<p><?php echo anchor("survey/viewSurvey/".$survey['surveyid'], 'View
					Survey', array('class' => 'btn')); ?> 
				<?php echo anchor("admin/manageParticipants/".$survey['surveyid'], 'Manage
						Participants', array('class' => 'btn')); ?>
					<?php if (!$survey['disabled']){?>
			
						<?php if ($survey['survey_state'] == 'PENDING'){?>
							<?php echo anchor("admin/startSurvey/".$survey['surveyid'], 'Start
							Survey', array('class' => 'btn btn-success')); ?> 
						<?php } else if ($survey['survey_state'] == 'STARTED') {?>
							<?php echo anchor("admin/stopSurvey/".$survey['surveyid'], 'Stop
							Survey', array('class' => 'btn btn-warning')); ?> 
						<?php } else if ($survey['survey_state'] == 'STOPPED') {?>
							<?php echo anchor("admin/startSurvey/".$survey['surveyid'], 'Start
							Survey', array('class' => 'btn btn-success')); ?> 
						<?php }?>
					<?php }?>
					</p>

		</div>
		<div class="row-fluid">
			<div class="span5 div-container-border">
				<div class="tabbable">
					<ul class="nav nav-tabs">
						<li id="addFieldsToggle" class="active"><a href="#addFields" data-toggle="tab">Add
								Fields</a>
						</li>
						<li><a id="fieldSettingsToggle" href="#fieldSettings" data-toggle="tab">Field Settings</a>
						</li>
						<li><a id="surveySettingsToggle" href="#surveySettings" data-toggle="tab">Survey Settings</a>
						</li>
					</ul>
					<div class="tab-content">
						<div id="addFields" class="tab-pane active">
							<h4 class="text-info">Formatting</h4>
							<div class="row-fluid">
								<div class="span6">
									<?php echo anchor("admin/addFormatting/".$survey['surveyid']."/SECTION", 'Section Break', array('class' => 'btn btn-small btn-block')); ?>

								</div>
								<div class="span6">
									<?php echo anchor("admin/addFormatting/".$survey['surveyid']."/PAGE", 'Page Break', array('class' => 'btn btn-small btn-block')); ?>

								</div>
							</div>
							<h4 class="text-info">Standard Questions</h4>
							<div class="row-fluid">
								<div class="span6">
									<?php echo anchor("admin/addStandardQuestion/".$survey['surveyid']."/SINGLE_LINE_TEXT", 'Single Line Text', array('class' => 'btn btn-small btn-block')); ?>
									<?php echo anchor("admin/addStandardQuestion/".$survey['surveyid']."/LONG_ANSWER", 'Long Answer', array('class' => 'btn btn-small btn-block')); ?>
									<?php echo anchor("admin/addStandardQuestion/".$survey['surveyid']."/DROPDOWN", 'Dropdown', array('class' => 'btn btn-small btn-block')); ?>

								</div>

								<div class="span6">
									<?php echo anchor("admin/addStandardQuestion/".$survey['surveyid']."/NUMBER", 'Number', array('class' => 'btn btn-small btn-block')); ?>
									<?php echo anchor("admin/addStandardQuestion/".$survey['surveyid']."/CHECKBOX", 'Checkbox', array('class' => 'btn btn-small btn-block')); ?>
									<?php echo anchor("admin/addStandardQuestion/".$survey['surveyid']."/MULTIPLE_CHOICE", 'Multiple Choice', array('class' => 'btn btn-small btn-block')); ?>

								</div>

							</div>

						</div>
						<div id="fieldSettings" class="tab-pane">
								<table id="field-settings-table" style="width:100%">
									<tbody>
									<tr><td><h4 class="text-info">Edit Fields</h4>
									</td></tr>
									<tr><td>Select a row to edit</td></tr>
									</tbody>
								</table>
						</div>
						<div id="surveySettings" class="tab-pane">
							<h4 class="text-info">Survey Settings</h4>
						
							<div>
								<?php echo form_open('admin/editSurveyDetails', $form_attributes, $hidden_fields); ?>

								<table>

									<tr>
										<td><?php echo form_label($survey_title['label']); ?>
										</td>
										<td><?php echo form_input($survey_title); ?></td>
									</tr>
									<tr>
										<td><?php echo form_label($survey_description['label']); ?>
										</td>
										<td><?php echo form_textarea($survey_description); ?></td>
									</tr>
									<tr>
										<td><?php echo form_label($anonymous['label']); ?>
										</td>
										<td><?php echo form_checkbox($anonymous); ?></td>
									</tr>
								</table>

								<?php echo form_submit('submit', 'Update Survey Details'); ?>
								<?php echo form_close(); ?>


							</div>



						</div>
					</div>
					<!-- /.tab-content -->
				</div>
				<!-- /.tabbable -->
			</div>
			<div class="span7">
				<div id='dynamic-survey-view' class='dynamic-survey-div-scroll'>
						<?php if(ISSET ($parent_field_list)){?>
						<table class="table table-hover">
							<thead>
								<th>
								<div class="row-fluid">
									<div class="span5">Survey Question</div>
									<div class="span4">Field Type</div>
									<div class="span3">Operations</div>
								
								</div>
							
								</th>
							</thead>
							<tbody>
								<?php foreach ($parent_field_list as $parent_field): ?>

								<tr>
									<td>
										<!--render page row -->
										 <?php if($parent_field['field_type'] == 'PAGE') {?>
													<div class='row-fluid'>
															<div class='span5'></div>
															<div class="populate-parent-field-children span4">
																<span class="field-type-pill <?php if ($parent_field['field_type'] == 'PAGE'){ echo "page-break-pill"; }else if ($parent_field['field_type'] == 'SECTION'){ echo "section-break-pill";}else{echo "standard-question-pill";}?>"><?php echo $parent_field['field_type'] ?>
																</span>
															</div>
															<div class="span3">
																<p>
																	<a class="btn btn-small btn-swap-up" order="<?php echo $parent_field['survey_order']?>"><i class="icon-circle-arrow-up" ></i></a>
																	<a class="btn btn-small btn-swap-down" order="<?php echo $parent_field['survey_order']?>"><i class="icon-circle-arrow-down" ></i></a>
																</p>
																<p>
																	<?php echo anchor("adminutility/deleteField/".$survey['surveyid']."/".$parent_field['parent_field_id'], 'Delete Field', array('class' => 'btn btn-small btn-danger')); ?>
																</p>
															</div>

														</div>
										<?php }?>
									
										<!--render survey question rows -->
											<?php if(ISSET ($parent_field_list) && ISSET($survey_question_list)){?>
												<?php foreach ($survey_question_list as $survey_question):?>
													<?php if ($survey_question['parent_field_id'] == $parent_field['parent_field_id']){?>
														<div class='row-fluid'>
															<div class='populate-parent-field-children span5' >
																<a class="question-title" value="<?php echo $survey_question['questionid']?>"> <?php echo $survey_question['questiontitle']?></a>
															</div>
															<div class="populate-parent-field-children span4">
																<span class="field-type-pill <?php if ($parent_field['field_type'] == 'PAGE'){ echo "page-break-pill"; }else if ($parent_field['field_type'] == 'SECTION'){ echo "section-break-pill";}else{echo "standard-question-pill";}?>"><?php echo $parent_field['field_type'] ?>
																</span>
															</div>
															<div class="populate-parent-field-children span3">
																<p>
																	<a class="btn btn-small btn-swap-up" order="<?php echo $parent_field['survey_order']?>"><i class="icon-circle-arrow-up" ></i></a>
																	<a class="btn btn-small btn-swap-down" order="<?php echo $parent_field['survey_order']?>"><i class="icon-circle-arrow-down" ></i></a>
																	<a class="btn btn-small btn-edit-field" value="<?php echo $survey_question['questionid']?>" questiontype="<?php echo $survey_question['input_type']?>" help_text="<?php echo $survey_question["help_text"]?>" questiontitle="<?php echo $survey_question['questiontitle']?>" is_required="<?php echo $survey_question['required']?>">Edit</a>
																</p>
																<p>
																	<?php echo anchor("adminutility/deleteField/".$survey['surveyid']."/".$parent_field['parent_field_id'] , 'Delete Field', array('class' => 'btn btn-small btn-danger')); ?>
																</p>
															</div>

														</div>
													<?php }?>
			
												<?php endforeach ?>
											<?php }?>
											
									</td>
								</tr>
								<?php endforeach ?>
							</tbody>

						</table>



						<?php } else {?>
						<p>This group has no parent fields</p>
						<?php }?>

					</div>

			</div>


		</div>
		<div id="result" style="color: #606;">
			<?php if (isset($result)) print_r($result); ?>
		</div>

		<?php echo validation_errors(); ?>
	</div>

	<br>

</div>
<script type="text/javascript">
    site_url = '<?=site_url()?>';
	defaultQuestionTitle = 'Edit Question...';
	defaultSectionTitle = 'Edit Section Label...';
</script>
<script>
    $(document).ready(function(){

    	$("#dynamic-survey-view").on("click", ".question-title",function(){
        	// toggle edit click
        	$("#dynamic-survey-view").find('.btn-edit-field[value='+$(this).attr('value')+']').click();
    	});
        
    	$("#dynamic-survey-view").on("click", ".btn-edit-field",function(){    	
        	$(this).closest('tr').addClass("info"); // add info class to tr to highlight active row for editing
        	$(this).closest('tr').siblings().removeClass('info'); // remove info class from all other rows
            $('#field-settings-table > tbody').html(""); // clear existing controls

            var editFieldHeading = 	'<tr><td><h4 class="text-info">Edit Fields</h4></td></tr>';

            var hasOptions = false;
            
            // dynamically generate form depending on question type
            var generated_edit_survey_question_field_form = "";
            if ($(this).attr("questiontype") == "TEXT"){
            	generated_edit_survey_question_field_form = singleInputForm($(this));
            } else if($(this).attr("questiontype") == "LONG_ANSWER"){
            	generated_edit_survey_question_field_form = singleInputForm($(this));
            } else if($(this).attr("questiontype") == "NUMERIC"){
            	generated_edit_survey_question_field_form = singleInputForm($(this));
            } else if($(this).attr("questiontype") == "CHECKBOXES"){
            	generated_edit_survey_question_field_form = optionsInputForm($(this));
            	hasOptions = true;
            } else if($(this).attr("questiontype") == "MULTIPLE_CHOICE"){
            	generated_edit_survey_question_field_form = optionsInputForm($(this));
            	hasOptions = true;
            } else if($(this).attr("questiontype") == "DROPDOWN"){
            	generated_edit_survey_question_field_form = optionsInputForm($(this));
            	hasOptions = true;
            } else if($(this).attr("questiontype") == "NONE"){
            	generated_edit_survey_question_field_form = noInputForm($(this));
            } 
			
            // append form to table
        	$('#field-settings-table > tbody')
        		.append(editFieldHeading)
        		.append(generated_edit_survey_question_field_form);

        	// load options
        	if (hasOptions){
        		loadOptions($(this).attr("value"));
        	}
        	// toggle edit tab
        	toggleEditTab();

        	// focus on title input
        	$("#field-settings-table").find("input[name=questiontitle]").focus();
          }); 

        function toggleEditTab(){
           $( "#fieldSettingsToggle" ).click();
        }

        function singleInputForm(data)
        {            
        	var form =
    		'<tr><td>'
    		+ '<div class="edit-field-div-scroll"	>'
    		+ '<?=form_open('admin/editSimpleInputQuestion', 'class = "edit_form"'); ?>'
    		+ '<div style="display:none">'
    		+ '<input type="hidden" name="questionid" value="'+data.attr("value")+'" />'
            + '<input type="hidden" name="surveyid" value="'+'<?=$survey['surveyid'] ?>'+'" />'
            + '</div>'
    		+ '<table>'
    		+ '<tr>'
    		+ '<td>' + '<label for="questiontitle">Question</label>'
    		+ '</td>'
    		+ '<td>' + '<input type="text" name="questiontitle" value="'+data.attr('questiontitle')+'" />'+ '</td>'
    		+ '</tr>'
    		+ '<tr>'
    		+ '<td>' + '<label for="help_text">Help Text</label>'
    		+ '</td>'
    		+ '<td>' +'<textarea name="help_text" cols="40" rows="5" >'+data.attr('help_text')+'</textarea>'+ '</td>'
    		+ '</tr>'
    		+ '<tr>'
    		+ '<td>' +'<label for="required_field">Required Field</label>'
    		+ '</td>'
    		+ '<td>' + '<input type="checkbox" style="padding:0;margin:0;" name="required_field" value="'+data.attr('is_required')+'"';

    	if(data.attr('is_required') == '1'){ 
    		form = form + ' checked="checked" ';
    	} else {
    		form = form + 'not_required';
    	}

    	form = form + '/>' + '</td>'
    		+ '</tr>'
    		+ '</table>'
    		+ '<?=form_submit('submit', 'Update Question Field', 'class = "btn btn-primary"'); ?>'
    		+ '<?=form_close(); ?>'
    		+ '</div>' // end div scroll
    		+ '</td></tr>';
    		return form;

        }

        function noInputForm(data)
        {            
        	var form =
    		'<tr><td>'
    		+ '<?=form_open('admin/editNoInputQuestion', 'class = "edit_form"'); ?>'
    		+ '<div style="display:none">'
    		+ '<input type="hidden" name="questionid" value="'+data.attr("value")+'" />'
            + '<input type="hidden" name="surveyid" value="'+'<?=$survey['surveyid'] ?>'+'" />'
            + '</div>'
    		+ '<table>'
    		+ '<tr>'
    		+ '<td>' + '<label for="questiontitle">Label</label>'
    		+ '</td>'
    		+ '<td>' + '<input type="text" name="questiontitle" value="'+data.attr('questiontitle')+'" />'+ '</td>'
    		+ '</tr>'
    		+ '<tr>'
    		+ '<td>' + '<label for="help_text">Description</label>'
    		+ '</td>'
    		+ '<td>' +'<textarea name="help_text" cols="40" rows="5" >'+data.attr('help_text')+'</textarea>'+ '</td>'
    		+ '</tr>'
    		+ '<tr>'
    		+ '</table>'
    		+ '<?=form_submit('submit', 'Update Section Field', 'class = "btn btn-primary"'); ?>'
    		+ '<?=form_close(); ?>'
    		+ '</td></tr>';
    		return form;

        }

        function optionsInputForm(data)
        {            
        	var form =
    		'<tr><td>'
    		+ '<div class="edit-field-div-scroll"	>'
    		+ '<?=form_open('admin/editOptionsInputQuestion', 'class = "edit_form"'); ?>'
    		+ '<div style="display:none">'
    		+ '<input type="hidden" name="questionid" value="'+data.attr("value")+'" />'
            + '<input type="hidden" name="surveyid" value="'+'<?=$survey['surveyid'] ?>'+'" />'
            + '</div>'
    		+ '<table>'
    		+ '<tr>'
    		+ '<td>' + '<label for="questiontitle">Question</label>'
    		+ '</td>'
    		+ '<td>' + '<input type="text" name="questiontitle" value="'+data.attr('questiontitle')+'" />'+ '</td>'
    		+ '</tr>'
    		+ '<tr>'
    		+ '<td>' + '<label for="help_text">Help Text</label>'
    		+ '</td>'
    		+ '<td>' +'<textarea name="help_text" cols="40" rows="5" >'+data.attr('help_text')+'</textarea>'+ '</td>'
    		+ '</tr>'
    		+ '<tr>'
    		+ '<td>' +'<label for="required_field">Required Field</label>'
    		+ '</td>'
    		+ '<td>' + '<input type="checkbox" style="padding:0;margin:0;" name="required_field" value="'+data.attr('is_required')+'"';

    	if(data.attr('is_required') == '1'){ 
    		form = form + ' checked="checked" ';
    	} else {
    		form = form + 'not_required';
    	}

    	form = form + '/>' + '</td>'
    		+ '</tr>'
    		+ '</table>'
    		+ '<?=form_submit('submit', 'Update Question Field', 'class = "btn btn-primary"'); ?>'
    		+ '<?=form_close(); ?>'
    		+ '</div>' //end div scroll
    		+ '</td></tr>';

    	// add options section to the form
    	form = form
    		+ '<tr><td>'
    		+ '<h4 class="text-info">Edit Options</h4>'
        	+ '<span questionid="'+data.attr("value")+'" class="add-option btn-success btn"> Add Option </span>'
    		+ '<div class="edit-field-div-scroll">'	
    		+ '<table class="optionsTable table">'
    		+ '<tbody></tbody>'
    		+ '</table>'
    		+ '</div>'
    		+ '<td></tr>';
    		return form;

        }

        function loadOptions(surveyQuestionId){
			// load options
		     $.ajax({
	            type: "post",
	            dataType: 'json',
	            url: site_url+'/adminutility/searchOptions',
	            cache: false,               
	            data:'questionid='+surveyQuestionId,
	            success: function(response){
	                $('.optionsTable > tbody').html(""); // clear existing controls
	            	var obj = response;
	                if(obj.length>0){
	                    try{
	                        var items=[];   
	                        $.each(obj, function(i,val){    
	                        	var tablerow = '<tr><td>' 
	                        		+ '<div class="form-horizontal">'
									+ '<a class="btn btn-small option-swap-up" order="'+val.option_order+'"><i class="icon-circle-arrow-up" ></i></a>'
									+ '<a class="btn btn-small option-swap-down" order="'+val.option_order+'"><i class="icon-circle-arrow-down" ></i></a>'
	                            	+ '<input id="option-'+surveyQuestionId+'-'+val.itemid+'" questionid="'+surveyQuestionId+'"  itemid="'+val.itemid+'" class="option-title" type="text" name="optiontitle" value="'+val.itemdescription+'" />'
	                            	+ '<a  questionid="'+surveyQuestionId+'" itemid="'+val.itemid+'" class="update-option btn-warning btn btn-small"> Update </a>'
	                            	+ '<a questionid="'+surveyQuestionId+'" itemid="'+val.itemid+'" class="remove-option btn-danger btn btn-small"> <strong>-</strong> </a>'
									+ '</div>'
	                            	+ '</td></tr>';
	
	                            	$('.optionsTable > tbody').append( tablerow );
	                     
	                        }); 
	                    }catch(e) {     
	                        alert('Exception while request..');
	                    }       
	                }else{
	                	var tablerow = '<tr><td>' 
							+ "No Options Found" 
							+ '<a value="NO_OPTIONS"class="option-update btn-success btn btn-small"> <strong>+</strong> Add Option </a>'
	                    	+ '</td><td></td><td></td></tr>';
	                    $('.optionsTable > tbody').append(tablerow);      
	                }                       
	
	            },
	            error: function(){                      
	                alert('Error while request..');
	            }
	        });
			
        }


        $("#fieldSettings").on("click", ".add-option",function(){            
            $.ajax({
                type: "post",
                dataType: 'html',
                url: site_url+ '/adminutility/addOption/' +  $(this).attr("questionid"),
                cache: false,               
                success: function(response){
                    // response is expected to be the question id
                    loadOptions(response);
                },
                error: function(){                      
	                alert('Error while request..');
                }
            });
          }); 

        $("#dynamic-survey-view").on("click", ".btn-swap-up",function(){            
            $.ajax({
                type: "post",
                dataType: 'html',
                url: site_url+ '/adminutility/swapParentFieldUp/' +  $(this).attr("order"),
                cache: false,               
                success: function(response){
                    if (response != null){
                        //refresh page
                    	location.reload();
                    }
                },
                error: function(){                      
	                alert('Error while request..');
                }
            });
          }); 

        $("#dynamic-survey-view").on("click", ".btn-swap-down",function(){            
            $.ajax({
                type: "post",
                dataType: 'html',
                url: site_url+ '/adminutility/swapParentFieldDown/' +  $(this).attr("order"),
                cache: false,               
                success: function(response){
                    if (response != null){
                        //refresh page
                    	location.reload();
                    }
                },
                error: function(){                      
	                alert('Error while request..');
                }
            });
          }); 
        
        $("#fieldSettings").on("click", ".option-swap-up",function(){            
            $.ajax({
                type: "post",
                dataType: 'html',
                url: site_url+ '/adminutility/swapOptionUp/' +  $(this).attr("order"),
                cache: false,               
                success: function(response){
                    if (response != null){
                        // response is expected to be the question id
                        loadOptions(response);
                    }
                },
                error: function(){                      
	                alert('Error while request..');
                }
            });
          }); 

        $("#fieldSettings").on("click", ".option-swap-down",function(){            
            $.ajax({
                type: "post",
                dataType: 'html',
                url: site_url+ '/adminutility/swapOptionDown/' +  $(this).attr("order"),
                cache: false,               
                success: function(response){
                    if (response != null){
                        // response is expected to be the question id
                        loadOptions(response);
                    }
                },
                error: function(){                      
	                alert('Error while request..');
                }
            });
          }); 

        $("#fieldSettings").on("click", ".remove-option",function(){            
            $.ajax({
                type: "post",
                dataType: 'html',
                url: site_url+ '/adminutility/removeOption/' +  $(this).attr("questionid") + "/" + $(this).attr("itemid"),
                cache: false,               
                success: function(response){
                    // response is expected to be the question id
                    loadOptions(response);
                },
                error: function(){                      
	                alert('Error while request..');
                }
            });
          }); 


        $("#fieldSettings").on("click", ".update-option",function(){ 
			// find related option-title input
			$inputElement = $('#option-'+$(this).attr('questionid')+'-'+$(this).attr('itemid'));
			updateOption($inputElement);
		});

		function updateOption(input){			
            $.ajax({
                type: "post",
                dataType: 'json',
                url: site_url+ '/adminutility/updateOption/' +  input.attr("questionid") + "/" + input.attr("itemid"),
                cache: false,               
                data:'title='+input.val(),
                success: function(response){
                    // response is expected to be the question id
                    loadOptions(response);
                },
                error: function(){                      
                    alert('Error while request..');
                }
            });
          }
    
    });
    

</script>