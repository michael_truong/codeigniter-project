<div id=content>
	<div id="main">


		<div class="codeigniter-hero-unit">
			<h2>
				View Responses for <span class="subject"><?php echo $survey['surveyname'] ?></span>
			</h2>
			<p>As an administrator, you can view the survey responses.</p>
			<p>
				<?php echo anchor("admin/viewSurveyResponses/".$survey['surveyid'], 'View
					 Responses', array('class' => 'btn')); ?>
				<?php echo anchor("admin/viewSurveySummary/".$survey['surveyid'], 'View
					Summary', array('class' => 'btn')); ?> 
				<?php echo anchor("admin/viewIndividualResponses/".$survey['surveyid'], 'View
						Individual Responses', array('class' => 'btn')); ?>
			</p>

		</div>
		
		<div id="stacked-stats" class="row-fluid">
			<div class="div-container-border">
				<h4 class="text-info">Survey Completion</h4>
				<div id="stacked-stats-graph" class="progress">
				  	<div class="bar bar-danger" style="width: 0%;"></div>
  					<div class="bar bar-warning" style="width: 0%;"></div>
  					<div class="bar bar-success" style="width: 0%;"></div>
				</div>
				<span class="text-error">Not completed</span> <br>
				<span class="text-success">Completed</span>
			</div>
		</div>
		
		<div id="detailed-stats" class="row-fluid">
			<div class="span6 div-container-border">
				<h4 class="text-info">Pending Participants</h4>
				<div id="not-completed-participants" class="edit-field-div-scroll">
				<table id="not-completed-participants-table" class="table table-hover">
							<thead>
							<tr>
								<th>Participant</th>
								<th>Operations</th>						
							</tr>
							</thead>
							<tbody>
							</tbody>
				</table>
				</div>
			</div>	
			<div class="span6 div-container-border">
				<h4 class="text-info">Completed Participants</h4>
				<div id="completed-participants" class="edit-field-div-scroll">
				<table id="completed-participants-table" class="table table-hover">
							<thead>
							<tr>
								<th>Participant</th>
								<th>Operations</th>						
							</tr>
							</thead>
							<tbody>
							</tbody>
				</table>
				</div>
			</div>		
		</div>
		
	</div>
</div>

<script type="text/javascript">
    site_url = '<?=site_url()?>';
</script>
<script type="text/javascript"> 
var completedParticipants = 0;
var pendingParticipants = 0;
$(document).ready(function(){
	loadCompletedParticipants();
	loadNotCompletedParticipants();

	setInterval ( "updateStackedProgress()", 1000 );

});

function updateStackedProgress ()
{
  	// only update if it is a valid stacked progress
  	if ((completedParticipants + pendingParticipants) > 0){
  	  	var completedPercentage = (completedParticipants * 100)/(completedParticipants + pendingParticipants);
  	  	var pendingPercentage = (pendingParticipants * 100)/(completedParticipants + pendingParticipants);
  	  	
	  	$("#stacked-stats-graph").find(".bar-success").css('width', completedPercentage + '%');
	  	$("#stacked-stats-graph").find(".bar-danger").css('width', pendingPercentage + '%');
	  	
	}
}

function loadCompletedParticipants(){
	var survey_id = <?php echo $survey['surveyid']?>;
	$.ajax({
        type: "post",
        dataType: 'json',
        url: site_url+ '/adminutility/getCompletedParticipants',
        cache: false,               
        data: { survey_id : survey_id},
        success: function(response){
            $('#completed-participants-table > tbody').html(""); // clear existing controls
        	var obj = response;
            if(obj != null && obj.length>0){
                try{
                    $.each(obj, function(i,participant){    
                    	var tablerow = '<tr><td>' 
							+ participant.username
                        	+ '</td><td></td></tr>';

                        	$('#completed-participants-table > tbody').append( tablerow );
                 
                    }); 
                }catch(e) {     
                    alert('Exception while request..');
                }     

                completedParticipants = obj.length;
            }else{
            	var tablerow = '<tr><td>' 
					+ "No Completed Participants found" 
                	+ '</td><td></td></tr>';
                $('#completed-participants-table > tbody').append(tablerow);      
            }                  

        },
        error: function(){                      
            alert('Error while request..');
        }
    });
}

function loadNotCompletedParticipants(){
	var survey_id = <?php echo $survey['surveyid']?>;
	$.ajax({
        type: "post",
        dataType: 'json',
        url: site_url+ '/adminutility/getNotCompletedParticipants',
        cache: false,               
        data: { survey_id : survey_id},
        success: function(response){
            $('#not-completed-participants-table > tbody').html(""); // clear existing controls
        	var obj = response;
            if(obj != null && obj.length>0){
                try{
                    $.each(obj, function(i,participant){    
                    	var tablerow = '<tr><td>' 
							+ participant.username
                        	+ '</td><td></td></tr>';

                        	$('#not-completed-participants-table > tbody').append( tablerow );
                 
                    }); 
                }catch(e) {     
                    alert('Exception while request..');
                }      
                pendingParticipants = obj.length;
                 
            }else{
            	var tablerow = '<tr><td>' 
					+ "No Pending Completion Participants found" 
                	+ '</td><td></td></tr>';
                $('#not-completed-participants-table > tbody').append(tablerow);      
            }

        },
        error: function(){                      
            alert('Error while request..');
        }
    });
	
}
</script>