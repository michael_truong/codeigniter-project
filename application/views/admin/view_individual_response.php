<div id=content>
	<div id="main">


		<div class="codeigniter-hero-unit">
			<h2>
				View Individual Responses for <span class="subject"><?php echo $survey['surveyname'] ?></span>
			</h2>
			<p>As an administrator, you can view the survey individual responses.</p>
			<p>
				<?php echo anchor("admin/viewSurveyResponses/".$survey['surveyid'], 'View
					 Responses', array('class' => 'btn')); ?>
				<?php echo anchor("admin/viewSurveySummary/".$survey['surveyid'], 'View
					Summary', array('class' => 'btn')); ?> 
				<?php echo anchor("admin/viewIndividualResponses/".$survey['surveyid'], 'View
						Individual Responses', array('class' => 'btn')); ?>
			</p>

		</div>
		
		
		<div id="submitted-responses-div" class="row-fluid">
			<div class="span6 div-container-border">
				<h4 class="text-info">Submitted Responses</h4>
				<div id="submitted-responses-div-scroll" class="edit-field-div-scroll">
				<table id="submitted-responses-table" class="table table-hover">
							<thead>
							<tr>
								<th>Response Unique Identifier</th>
								<th>Respondent</th>
								<th>Operations</th>
							</tr>
							</thead>
							<tbody>
							</tbody>
				</table>
				</div>
			</div>	
		</div>
		
	</div>
</div>

<script type="text/javascript">
    site_url = '<?=site_url()?>';
</script>
<script type="text/javascript"> 

$(document).ready(function(){
	loadSubmittedResponses();
});


function loadSubmittedResponses(){
	var survey_id = <?php echo $survey['surveyid']?>;
	var anonymous = <?php echo $survey['anonymous']?>;
	$.ajax({
        type: "post",
        dataType: 'json',
        url: site_url+ '/adminutility/viewIndividualResponses',
        cache: false,               
        data: { survey_id : survey_id, anonymous : anonymous},
        success: function(response){
            $('#submitted-responses-table > tbody').html(""); // clear existing controls
        	var obj = response;
            if(obj != null && obj.length>0){
                try{
                    $.each(obj, function(i,response){    
                    	var tablerow = '<tr><td>' 
							+ response.survey_response_uuid
                        	+ '</td><td>'
							+ response.username
                        	+ '</td><td>'
                        	+ '<a class="btn btn-small" href="'+ site_url + '/admin/viewSurveyResponse/' + survey_id + '/' + response.survey_response_uuid +'">View Response</a>'
                        	+ '</td></tr>';

                        	$('#submitted-responses-table > tbody').append( tablerow );
                 
                    }); 
                }catch(e) {     
                    alert('Exception while request..');
                }     

                completedParticipants = obj.length;
            }else{
            	var tablerow = '<tr><td>' 
					+ "No Submitted Responses found" 
                	+ '</td><td></td><td></td></tr>';
                $('#submitted-responses-table > tbody').append(tablerow);      
            }                  

        },
        error: function(){                      
            alert('Error while request..');
        }
    });
}

</script>