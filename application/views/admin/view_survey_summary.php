<div id=content>
	<div id="main">


		<div class="codeigniter-hero-unit">
			<h2>
				View Survey Response Summary for <span class="subject"><?php echo $survey['surveyname'] ?></span>
			</h2>
			<p>As an administrator, you can view the survey responses in a summarised form.</p>
			<p>
				<?php echo anchor("admin/viewSurveyResponses/".$survey['surveyid'], 'View
					 Responses', array('class' => 'btn')); ?>
				<?php echo anchor("admin/viewSurveySummary/".$survey['surveyid'], 'View
					Summary', array('class' => 'btn')); ?> 
				<?php echo anchor("admin/viewIndividualResponses/".$survey['surveyid'], 'View
						Individual Responses', array('class' => 'btn')); ?>
			</p>

		</div>
		
		
		<div id="submitted-responses-div" class="row-fluid">
			<div class="span4 div-container-border">
				<h4 class="text-info">Question Navigator</h4>
				<div id="question-navigator-div-scroll" class="edit-field-div-scroll">
				<table id="question-navigator-table" class="table table-hover">
							<thead>
							<tr>
								<th>Question</th>
								<th>Operations</th>
							</tr>
							</thead>
							<tbody>
							</tbody>
				</table>
				</div>
			</div>	
			<div id="question-summary" class="span8 div-container-border">
				<h4 id="question-summary-title" class="text-info">Summary</h4>
				<div id="question-summary-content">
					Please select a question on the Question Navigator to view its summary.
				</div>
			</div>	
		</div>
		
	</div>
</div>

<script type="text/javascript">
    site_url = '<?=site_url()?>';
</script>
<script type="text/javascript"> 

$(document).ready(function(){
	loadQuestionNavigator();

	$('#submitted-responses-div').on('click', '.fetchSummary', function(){
		renderSummary($(this));
	});
});


function renderSummary(questionNavigatorButton){
	$('#question-summary-title').text(questionNavigatorButton.attr('questiontitle'));
	$.ajax({
        type: "post",
        dataType: 'html',
        url: site_url+ '/adminutility/renderQuestionSummary',
        cache: false,               
        data: { question_id : questionNavigatorButton.attr('questionid'), input_type : questionNavigatorButton.attr('input_type')},
        success: function(response){
        	$('#question-summary-content').html(""); // replace summary content with response
        	$('#question-summary-content').html(response);
        },
        error: function(){                      
            alert('Error while request..');
        }
    });
}

function loadQuestionNavigator(){
	var survey_id = <?php echo $survey['surveyid']?>;
	$.ajax({
        type: "post",
        dataType: 'json',
        url: site_url+ '/adminutility/loadQuestionNavigator',
        cache: false,               
        data: { survey_id : survey_id},
        success: function(response){
            $('#question-navigator-table > tbody').html(""); // clear existing controls
        	var obj = response;
            if(obj != null && obj.length>0){
                try{
                    $.each(obj, function(i,question){    
                        if (question.input_type != "NONE"){
                        	var tablerow = '<tr><td>' 
                            	+ '<span class="label">'+question.input_type+'</span><br>'
    							+ question.questiontitle
                            	+ '</td><td>'
                            	+ '<a class="btn btn-small fetchSummary" input_type="'+question.input_type+'" questiontitle="'+question.questiontitle+'" questionid="'+question.questionid+'">View Summary</a>'
                            	+ '</td></tr>';

                            	$('#question-navigator-table > tbody').append( tablerow );
                        }
                    }); 
                }catch(e) {     
                    alert('Exception while request..');
                }     

                completedParticipants = obj.length;
            }else{
            	var tablerow = '<tr><td>' 
					+ "No Survey Questions found" 
                	+ '</td><td></td></tr>';
                $('#question-navigator-table > tbody').append(tablerow);      
            }                  

        },
        error: function(){                      
            alert('Error while request..');
        }
    });
}

</script>