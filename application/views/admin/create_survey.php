<?php 
$survey_title = array(
		'label' => 'Survey Title',
		'name'	=> 'title',
		'id'	=> 'title',
		'maxlength'	=> 80,
		'size'	=> 30,
);
$survey_description = array(
		'label' => "Survey Description",
		'name'	=> 'survey_description',
		'id'	=> 'survey_description',
		'rows'	=> 5,
		'columns'		=> 10,
);
$anonymous = array(
		'label' => 'Anonymous Survey',
		'name'	=> 'anonymous',
		'style' => 'margin:0;padding:0',
);
$form_attributes = array('class' => 'create_form', 'id' => 'create-survey-form');
?>

<div id=content>
	<div id="main">
		<div class="codeigniter-hero-unit">
			<h2>Create a Survey</h2>
		</div>

		<div>
			<?php echo form_open('admin/createSurvey', $form_attributes); ?>

			<table>

				<tr>
					<td><?php echo form_label($survey_title['label'], $survey_title['id']); ?>
					</td>
					<td><?php echo form_input($survey_title); ?></td>
				</tr>
				<tr>
					<td><?php echo form_label($survey_description['label'], $survey_description['id']); ?>
					</td>
					<td><?php echo form_textarea($survey_description); ?></td>
				</tr>
				<tr>
					<td><?php echo form_label($anonymous['label']); ?>
					</td>
					<td><?php echo form_checkbox($anonymous); ?></td>
				</tr>
			</table>

			<?php echo form_submit('submit', 'Create Survey'); ?>
			<?php echo form_close(); ?>


		</div>

		<div id="result" style="color: #606;">
			<?php if (isset($result)){ 
				print_r($result);
} ?>
		</div>

		<?php echo validation_errors(); ?>

	</div>
	<br>

</div>
