

<?php 
$createGroupAttributes = array('title' => 'Create Survey!', 'class' => 'btn btn-primary btn-large');
?>
<div id=content>

	<div id="survey-admin-hero-unit" class="codeigniter-hero-unit">
		<div class="row-fluid">
			<div class="span8">
				<h2>Survey Admin</h2>
				As an administrator, you may manage surveys from this page.
				<p></p>
				<p>
					<?php echo anchor("admin/createSurvey", 'Create Survey', $createGroupAttributes); ?>
				</p>
			</div>
			<div id="filter-div" class="span4">
				<h4 class="text-info">Filter Surveys</h4>
				<label for="survey-search">Survey Search: </label> <input type="text"
								name="survey-search" id="survey-search" />
				<br>
				<span id="filter-surveys" class="btn">Filter Surveys</span>
				<span id="show-all-surveys" class="btn">Clear filter</span>
			</div>
		</div>
	</div>
	
	<div id="alert-div" class="row-fluid"></div>
	
	<div id="survey-admin-table-div" class="div-container-border dynamic-survey-div-scroll">
		<table id="survey-admin-table" class="table table-hover">
		<thead>
			<tr>
			<th>Survey State</th>
			<th>Survey Name</th>
			<th>Survey Transition</th>
			<th>Operations</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
		</table>
	</div>

</div>
<script type="text/javascript">
    site_url = '<?=site_url()?>';
</script>
<script>
    $(document).ready(function(){
    	displayAllSurveys();
        
    	$("#filter-div").on("click", "#show-all-surveys",function(){
        	displayAllSurveys();
    	});

    	$("#filter-div").on("click", "#filter-surveys",function(){
        	displaySurveysWithFilter();
    	});
    	
		function displayAlert(message){
			clearAlerts();
			var alert = '<div class="alert">'
				+ '<button type="button" class="close" data-dismiss="alert">&times;</button>'
			  	+ '<strong>' + message + '</strong>'
				+ '</div>';
			$("#alert-div").html(alert); //add alert
		}

		function clearAlerts(){
			$("#alert-div").html(""); //clear alerts
		}

		function displaySurveysWithFilter(){
			var textSearch = $("#survey-search").val();
			if (textSearch == ''){
				// no search string, display all surveys
				displayAllSurveys();
			} else {
				displaySurveys(textSearch);
			}
		}

		function displayAllSurveys(){
			displaySurveys('');
		}
    	
    	function displaySurveys(filter){
    			// load surveys
    		     $.ajax({
    	            type: "post",
    	            dataType: 'json',
    	            url: site_url+'/adminutility/getSurveyList',
    	            cache: false,            
    	            data: {filter : filter},
    	            success: function(response){
    	                $('#survey-admin-table > tbody').html(""); // clear existing controls
    	            	var obj = response;
    	                if(obj.length>0){
    	                    try{
    	                        $.each(obj, function(i,survey_item){    
        	                        // open table row
    	                        	var tablerow = '<tr>' ;

    	                        	// add survey state and survey name
    	                        	tablerow = tablerow
										+ '<td>' + survey_item.survey_state + '</td>'
										+ '<td><i>' + survey_item.surveyname + '</i></td>';

									// add survey state
									if (survey_item.disabled == '0'){
										tablerow = tablerow + '<td>';
										if(survey_item.survey_state == 'PENDING'){
											tablerow = tablerow + '<a href="' + site_url + '/admin/startSurvey/' + survey_item.surveyid +'" class="btn btn-success"> Start Survey </a>';
										} else if (survey_item.survey_state == 'STARTED'){
											tablerow = tablerow + '<a href="' + site_url + '/admin/stopSurvey/' + survey_item.surveyid +'" class="btn btn-warning"> Stop Survey </a>';
										} else if (survey_item.survey_state == 'STOPPED'){
											tablerow = tablerow + '<a href="' + site_url + '/admin/startSurvey/' + survey_item.surveyid +'" class="btn btn-success"> Start Survey </a>';
										}
										tablerow = tablerow + '</td>';
									} else {
									tablerow = tablerow + '<td>DISABLED SURVEY</td>';
									}

									// add survey operations
									tablerow = tablerow + '<td>';
									tablerow = tablerow + '<a href="' + site_url + '/survey/viewSurvey/' + survey_item.surveyid +'" class="btn"> View Survey </a>';
									tablerow = tablerow + '<a href="' + site_url + '/admin/manageParticipants/' + survey_item.surveyid +'" class="btn"> Manage Participants </a>';
									if (survey_item.survey_state == 'PENDING'){
										tablerow = tablerow + '<a href="' + site_url + '/admin/editSurvey/' + survey_item.surveyid +'" class="btn"> Edit Survey </a>';
									} else {
										tablerow = tablerow + '<a href="' + site_url + '/admin/viewSurveyResponses/' + survey_item.surveyid +'" class="btn"> View Responses </a>';
									}
									if (survey_item.disabled == '0'){
										tablerow = tablerow + '<a href="' + site_url + '/admin/disableSurvey/' + survey_item.surveyid +'" class="btn btn-warning"> Disable Survey </a>';
									} else {
										tablerow = tablerow + '<a href="' + site_url + '/admin/enableSurvey/' + survey_item.surveyid +'" class="btn btn-success"> Enable Survey </a>';
									}
									tablerow = tablerow + '</td>';
																			
									// close tablerow
									tablerow = tablerow + '</tr>';

    	                            	
    	                            $('#survey-admin-table > tbody').append( tablerow );
    	                        }); 
    	                        // successful loop, clear alerts
    	                        clearAlerts();
    	                    }catch(e) {     
    	                        alert('Exception while request..');
    	                    }       
    	                }else{
    	                	displayAlert("No surveys found."); 
    	                }                       
    	            },
    	            error: function(){                      
    	                alert('Error while request..');
    	            }
    	        });
    	}

    	
    });    

</script>
