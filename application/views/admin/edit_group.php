<div id=content>
	<div id="main">
		<div class="codeigniter-hero-unit">
			<div class="row-fluid">
				<div class="span8">
					<h2>
						Manage Group
						<span class="subject"><?php echo $group['groupname'] ?></span>
					</h2>
					As an administrator, you can update the group description and generate group email lists.
					<p>
						<?php echo anchor("/admin/viewGroup/".$group['id'], 'Manage
									 Members', array('class' => 'btn')); ?>
					</p>
				</div>
				<div id="filter-div" class="span4">
					<h4 class="text-info">Manage Group Operations</h4>
					<p class="text-error">If the group is no longer required, it can be deleted. This is a
					permanent action.</p>
					<p>
						<?php echo anchor("admin/deleteGroup/".$group['id'], 'Delete Group!', array('class' => 'btn btn-danger')); ?>
					</p>
				</div>
			</div>
		</div>
		
		<div class="row-fluid">
			<div id="group-edit" class="span6">
				<div id="update-description" class="div-container-border">
					<h4 class="text-info">Update Group Description</h4>
					<Section>
						Current Group Description:
						<p id="current-description" class="muted"><?php echo $group['text']?></p>
					</Section>
					<section>
						<label for="group-description">Group Description: </label> 
						<textarea name="group-description" id="group-description" style="width:80%; height:25%"><?php echo $group['text']?></textarea>
					</section>
					<section>
						<span id="update-group-btn" class="btn btn-primary">Update</span>
					</section>
				</div>
				<section class="div-container-border">
					<h4 class="text-info">Update Group Log</h4>
					<div id="update-group-log" class="edit-field-div-scroll">
					</div>
				</section>
			</div>
			
			<div id="group-email" class="span6">
				<div id="send-email-div" class="div-container-border">
					<h4 class="text-info">Generate Group Email List</h4>
					<section>
						<span id="email-group-btn" class="btn btn-primary">Generate Emails</span>
					</section>
					<section>
						<label for="email-content">Email List:</label> 
						<textarea style="width:80%; height:25%" name="email-content" id="email-content"></textarea>
					</section>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
    site_url = '<?=site_url()?>';
    group_id = '<?php echo $group['id']?>';
    group_title = '<?php echo $group['groupname'] ?>';
</script>
<script>
    $(document).ready(function(){ 

    	$("#group-email").on("click", "#email-group-btn",function(){
        	generateEmailList();
    	});
        
    	$("#update-description").on("click", "#update-group-btn",function(){
        	updateGroup();
    	});

		function generateEmailList(){
	    	$('#email-content').val(''); // clear previous list
	    	$.ajax({
	            type: "post",
	            dataType: 'html',
	            url: site_url+'/adminutility/generateEmailList',
	            cache: false,            
	            data: {group_id : group_id},
	            success: function(response){
	    	    	$('#email-content').val(response); // append the email list
	            },
	            error: function(){                      
	                alert('Error while request..');
	            }
	        });
		}
    	
    	function updateGroup(){
			group_description = $('#group-description').val();
	    	$('#update-group-log').text(''); // clear previous log
            
    		     $.ajax({
    	            type: "post",
    	            dataType: 'json',
    	            url: site_url+'/adminutility/editGroupDetails',
    	            cache: false,            
    	            data: {group_description : group_description, group_id : group_id, title : group_title},
    	            success: function(response){
    	    	    	$('#update-group-log').append(response.message); // clear previous log
    	    	    	if (response.success == true){
							$('#current-description').text(response.group_description);
        	    	    }
    	            },
    	            error: function(){                      
    	                alert('Error while request..');
    	            }
    	        });
    	}
    });    

</script>