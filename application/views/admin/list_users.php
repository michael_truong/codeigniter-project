<div id=content>
		<div class="codeigniter-hero-unit">
			<div class="row-fluid">
				<div class="span8">
					<h2>System Users</h2>
					As an administrator, you may manage system users from this page.
					<p>
						<?php echo anchor("admin/listSystemUsers", 'View
							 System Users', array('class' => 'btn')); ?>
						<?php echo anchor("admin/createUser", 'Create
							Single User', array('class' => 'btn')); ?> 
						<?php echo anchor("admin/createUserBatch", 'Create
								Multiple Users', array('class' => 'btn')); ?>
					</p>
				</div>
				<div id="filter-div" class="span4">
					<h4 class="text-info">Filter Users</h4>
					<label for="user-search">User Search: </label> <input type="text"
									name="user-search" id="user-search" />
					<br>
					<span id="filter-users" class="btn">Filter Users</span>
					<span id="show-all-users" class="btn">Clear filter</span>
				</div>
			</div>
		</div>
		
		
	<div id="alert-div" class="row-fluid"></div>
	
	<div id="user-admin-table-div" class="div-container-border dynamic-survey-div-scroll">
		<table id="user-admin-table" class="table table-hover">
		<thead>
			<tr>
			<th>User Name</th>
			<th>User Email</th>
			<th>Activated Status</th>
			<th>Banned Status</th>
			<th>Operations</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
    site_url = '<?=site_url()?>';
</script>
<script>
    $(document).ready(function(){
    	displayAllUsers();
        
    	$("#filter-div").on("click", "#show-all-users",function(){
        	displayAllUsers();
    	});

    	$("#filter-div").on("click", "#filter-users",function(){
        	displayUsersWithFilter();
    	});

    	$("#user-admin-table-div").on("click",".promote-user",function(){
        	promoteUser($(this));
    	});

    	function promoteUser(promoteButton){
			var userid = promoteButton.attr('user');
			$.ajax({
	            type: "post",
	            dataType: 'json',
	            url: site_url+'/adminutility/promoteUser',
	            cache: false,            
	            data: {userid : userid},
	            success: function(response){
		            $("#row-"+promoteButton.attr('user')).append("<em class='text-info'> (admin)</em>");
	            	promoteButton.remove();                     
	            },
	            error: function(){                      
	                alert('Error while request..');
	            }
	        });
        }
    	
		function displayAlert(message){
			clearAlerts();
			var alert = '<div class="alert">'
				+ '<button type="button" class="close" data-dismiss="alert">&times;</button>'
			  	+ '<strong>' + message + '</strong>'
				+ '</div>';
			$("#alert-div").html(alert); //add alert
		}

		function clearAlerts(){
			$("#alert-div").html(""); //clear alerts
		}

		function displayUsersWithFilter(){
			var textSearch = $("#user-search").val();
			if (textSearch == ''){
				// no search string, display all surveys
				displayAllUsers();
			} else {
				displayUsers(textSearch);
			}
		}

		function displayAllUsers(){
			displayUsers('');
		}
    	
    	function displayUsers(filter){
    			// load surveys
    		     $.ajax({
    	            type: "post",
    	            dataType: 'json',
    	            url: site_url+'/adminutility/getUserList',
    	            cache: false,            
    	            data: {filter : filter},
    	            success: function(response){
    	                $('#user-admin-table > tbody').html(""); // clear existing controls
    	            	var obj = response;
    	                if(obj.length>0){
    	                    try{
    	                        $.each(obj, function(i,user_item){    
        	                        // open table row
    	                        	var tablerow = '<tr>' ;

									// add username
									tablerow = tablerow + '<td id="row-'+user_item.id+'">'+user_item.username;

									// append priviledge level
									if (user_item.group_id == 100){
										tablerow = tablerow + '<em class="text-info"> (admin)</em></td>';
										
									} else {
										tablerow = tablerow +'</td>';
									}
									
    	                        	// add email
									tablerow = tablerow + '<td>'+user_item.email+'</td>';

									// add activation status
									
									if (user_item.activated == 1){
										tablerow = tablerow + '<td><span class="label label-success">Activated</span></td>';
									} else {
										tablerow = tablerow + '<td><span class="label label-warning">Unactivated</span></td>';
									}

									// add banned status
									if (user_item.banned == 1){
										tablerow = tablerow + '<td><span class="label label-warning">Banned</span></td>';
									} else {
										tablerow = tablerow + '<td></td>';
									}

									// add operations
									if (user_item.group_id == 100){
										tablerow = tablerow + '<td></td>';
										
									} else {
										tablerow = tablerow + '<td><a class="btn promote-user" user="'+user_item.id+'">Promote to Admin</a></td>';
									}
											
									// close tablerow
									tablerow = tablerow + '</tr>';

    	                            	
    	                            $('#user-admin-table > tbody').append( tablerow );
    	                        }); 
    	                        // successful loop, clear alerts
    	                        clearAlerts();
    	                    }catch(e) {     
    	                        alert('Exception while request..');
    	                    }       
    	                }else{
    	                	displayAlert("No user found."); 
    	                }                       
    	            },
    	            error: function(){                      
    	                alert('Error while request..');
    	            }
    	        });
    	}

    	
    });    

</script>
