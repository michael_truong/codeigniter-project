<html lang="en">
<head>
<title><?php if (ISSET ($_SESSION['survey_system_title'])) {echo $_SESSION['survey_system_title'];}else{echo "Codeigniter Survey";}?></title>
<meta name="resource-type" content="document" />
<meta name="robots" content="all, index, follow" />
<meta name="googlebot" content="all, index, follow" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<?php
/** -- Copy from here -- */
if(!empty($meta))
	foreach($meta as $name=>$content){
	echo "\n\t\t";
	?>
<meta name="<?php echo $name; ?>" content="<?php echo $content; ?>" />
<?php
}
echo "\n";

if(!empty($canonical))
{
	echo "\n\t\t";
	?>
<link rel="canonical" href="<?php echo $canonical?>" />
<?php

}
echo "\n\t";

foreach($css as $file){
	 	echo "\n\t\t";
	 	?>
<link rel="stylesheet" href="<?php echo $file; ?>" type="text/css" />
<?php
	} echo "\n\t";

	foreach($js as $file){
			echo "\n\t\t";
			?>
<script src="<?php echo $file; ?>"></script>
<?php
	} echo "\n\t";

	/** -- to here -- */
	?>

<!-- Le styles -->
<link
	href="<?php echo base_url(); ?>assets/themes/default/css/bootstrap.css"
	rel="stylesheet">
<link
	href="<?php echo base_url(); ?>assets/themes/default/css/bootstrap-responsive.css"
	rel="stylesheet">
<script type="text/javascript"
	src="http://twitter.github.io/bootstrap/assets/js/bootstrap-transition.js"></script>
<script type="text/javascript"
	src="http://twitter.github.io/bootstrap/assets/js/bootstrap-collapse.js"></script>
<link
	href="<?php echo base_url(); ?>assets/themes/default/css/general.css"
	rel="stylesheet">
<link
	href="<?php echo base_url(); ?>assets/themes/default/css/custom.css"
	rel="stylesheet">
<link rel="stylesheet"
	href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script
	src="<?php echo base_url(); ?>assets/themes/default/js/bootstrap.js"></script>
<script>
	$(document).ready(function(){
		$(function() {
		    $("input[type=submit]").addClass("btn btn-primary");
	  	});
	});
</script>


<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

<!-- Le fav and touch icons -->
<link rel="shortcut icon"
	href="<?php echo base_url(); ?>assets/themes/default/images/cs-favicon.png"
	type="image/x-icon" />
<meta property="og:image"
	content="<?php echo base_url(); ?>assets/themes/default/images/new-thumb.png" />
<link rel="image_src"
	href="<?php echo base_url(); ?>assets/themes/default/images/new-thumb.png" />
<style type="text/css">
::selection {
	background-color: #E13300;
	color: white;
}

::moz-selection {
	background-color: #E13300;
	color: white;
}

::webkit-selection {
	background-color: #E13300;
	color: white;
}

html,body {
	height: 100%;
	/* The html and body elements cannot have any padding or margin. */
}

.sidebar-nav {
	padding: 9px 0;
}

@media ( max-width : 980px) { /* Enable use of floated navbar text */
	.navbar-text.pull-right {
		float: none;
		padding-left: 5px;
		padding-right: 5px;
	}
	#footer {
		margin-left: -20px;
		margin-right: -20px;
		padding-left: 20px;
		padding-right: 20px;
	}
}

.navbar-inverse .brand,
.navbar-inverse .nav > li > a {
  color: #ccc;
  text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
}

/* Sticky footer styles
      -------------------------------------------------- */
#wrap {
	min-height: 100%;
	height: auto !important;
	height: 100%;
	/* Negative indent footer by it's height */
	margin: 0 auto -60px;
}

/* Set the fixed height of the footer here */
#push,#footer {
	height: 60px;
}

#footer {
	background-color: #f5f5f5;
}

#wrap .container-fluid {
	padding-top: 60px;
}

.container-fluid .credit {
	margin: 20px 0;
}
</style>
</head>

<body>
	<div id="wrap">

		<div class="navbar navbar-inverse navbar-fixed-top">
			<div class="navbar-inner">
				<div class="container">

					<!-- .btn-navbar is used as the toggle for collapsed navbar content -->
					<a class="btn btn-navbar" data-toggle="collapse"
						data-target=".nav-collapse"> <span class="icon-bar"></span> <span
						class="icon-bar"></span> <span class="icon-bar"></span>
					</a> 
					
					<img
						src="<?php echo base_url(); ?>assets/themes/default/images/new-thumb.png"
						style="float: left; margin-top: 5px; z-index: 5" alt="logo" /> <a
						class="brand" href="<?php echo site_url(); ?>">&nbsp;&nbsp;Codeigniter
						Survey</a>
					<div style="height: 0px;" class="nav-collapse collapse">
						<?php if ($this->tank_auth->is_logged_in()){?>
						<p class="navbar-text pull-right">
							Logged in as <b><?php echo $_SESSION['current_username']?> </b> <a
								href="<?php echo site_url('auth/logout'); ?>"> (Logout)</a>
						</p>
						<?php }?>
						<ul class="nav">
							<?php echo $this->load->get_section('nav'); ?>

						</ul>
					</div>

					<!--/.nav-collapse -->
				</div>
			</div>
		</div>

		<div class="container-fluid">
			<?php if($this->load->get_section('text_header') != '') { ?>
			<h1>
				<?php echo $this->load->get_section('text_header');?>
			</h1>
			<?php }?>
			<div class="row-fluid">
				<?php echo $output;?>
				<?php echo $this->load->get_section('sidebar'); ?>
			</div>
		</div>
		<!-- /container -->
		<div id="push"></div>
	</div>

	<footer id="footer">
		<div class="container-fluid">
			<p class="muted credit">Copyright 2013 &copy; Michael Truong</p>
		</div>
	</footer>

</body>
</html>
