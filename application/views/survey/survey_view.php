<!-- the input fields that will hold the variables we will use -->
<input type='hidden'
	id='current_page' />


<div id=content>
	<div id="main">

		<div class="codeigniter-hero-unit">
			<h2 class="text-center">
				<span class="subject"><?php echo $survey['surveyname'] ?></span>
			</h2>
			<div id="survey-description">
				<p class="text-info text-center">
					<?php echo $survey['survey_description']?>
				<p>
			</div>
		</div>
		
		<?php echo validation_errors('<p class="text-error">'); ?>
				
		<!--<?php echo $debug?>-->

		
		<?php if(ISSET ($parent_field_list)){?>
		<div class="pagination pagination-centered">
			<ul id="survey-pagination">
				<li><a href="javascript:previous();">&laquo;</a>
				</li>
				<li id="pagination-<?php $pageCount = 1; echo $pageCount?>"><a 
					href="javascript:go_to_page(<?php echo $pageCount?>);"><?php echo $pageCount?>
				</a> <?php foreach ($parent_field_list as $parent_field): ?> <?php if ($parent_field['field_type'] == 'PAGE'){?>
				</li>
				<li id="pagination-<?php $pageCount = $pageCount + 1; echo $pageCount?>"><a 
					href="javascript:go_to_page(<?php  echo $pageCount;?>);"><?php echo $pageCount?>
				</a> <?php }?> <?php endforeach ?>
				</li>

				<li><a href="javascript:next();">&raquo;</a>
				</li>
			</ul>
		</div>
		
		<!-- form start -->
		<?php echo form_open('survey/surveySubmit/'.$survey['surveyid']); ?>
				
		<div id="survey-content">
		
			<div id="page-<?php $pageCount = 1; echo $pageCount;?>"> <!-- default page -->
				<div class="survey-section"> <!-- default section -->
					<div class="container">
					<?php foreach ($parent_field_list as $parent_field): ?>
						<?php if ($parent_field['field_type'] == 'PAGE'){?>
						<!-- render end and start of new pagination tab -->
							</div></div></div> <!-- close current section/section container and page -->
							<div id="page-<?php $pageCount = $pageCount + 1; echo $pageCount;?>">
							<div class="survey-section"> <!-- default section -->
								<div class="container">
						<?php } else if ($parent_field['field_type'] == 'SECTION') {?>
							</div></div> <!-- close previous section/section container -->
							<div class="survey-section">
								<div class="container">
									<div id='survey-question-<?php echo $parent_field["parent_field_id"] ?>' class="survey-question" parenttype='<?php echo $parent_field["field_type"] ?>' parentid='<?php echo $parent_field["parent_field_id"] ?>'>
									</div>
						<?php } else {?>
							<div id='survey-question-<?php echo $parent_field["parent_field_id"] ?>' class="survey-question" parenttype='<?php echo $parent_field["field_type"] ?>' parentid='<?php echo $parent_field["parent_field_id"] ?>'>
							</div>
						<?php }?>

					<?php endforeach ?>
				
				</div></div> <!-- close default section/section container -->
			</div> <!-- close default page -->
			
		</div> <!-- end of survey content -->

		<?php } else {?>
		<p>This group has no parent fields</p>
		<?php }?>
		
		<!-- hidden submit buttons -->
		<?php echo form_submit('submit', 'Submit Survey', 'class = "hide" id="formSubmitSurvey"'); ?>
		<?php echo form_submit('submit', 'Save Progress', 'class = "hide" id="formSaveProgress"'); ?>
		<!-- form end -->
		<?php echo form_close(); ?>
		
		
	</div>
	<br>


</div>
<!-- Moved the scripts to the bottom for faster page loading -->
<script type="text/javascript">
    site_url = '<?=site_url()?>';
</script>
<script type="text/javascript"> 
var survey_responses = null;
<?php if (ISSET ($survey_responses)) {?>
	//initialise survey_responses array in javascript 
	survey_responses = <?php echo json_encode($survey_responses); ?>;
<?php } ?>

$(document).ready(function(){
    //set the value of our hidden input fields
    $('#current_page').val(1);
    
	// hide survey content except first one
	go_to_page(1); 

	loadParentFieldChildren();
	
	$('#survey-content').on("click", ".toggle-tip",function () {
		      $('#'+$(this).attr("linkedTip")).addClass('in').removeClass('hide'); 
		      return false; // slide it down
		      
		    });

	$('#survey-content').on("click", ".close",function () {
	      $(this).parent().removeClass('in').addClass('hide');
	      return false; // slide it down
	      
     });	

    $(document).on("click", "#saveSurveyProgress" , function(){
    	// removes the required attribute on options so that the form can be submitted
		// disabled due to some usability issues (hidden message on multipage surveys)
		// removeOptionRequiredAttribute();
        $('#formSaveProgress').click();
    });
    
    $(document).on("click", "#submitSurvey" , function(){
    	// adds the required attribute on options that are set as required 
    	// disabled due to some usability issues (hidden message on multipage surveys)
		// addOptionRequiredAttribute();
        $('#formSubmitSurvey').click();
    });

    $('#survey-content').on("keyup", ".textInput", function(){

            var value = $(this).val();

            if(value=="") {
                return false;
            }

            var questionType = 'text';
            if ($(this).hasClass('numericInput')){
            	questionType = 'numeric';
            }

            var id = $(this).attr('id');
            
            var json = {};
            json[id] = value;
            json['id'] = id;
            json['type'] = questionType;
            
            $.ajax({
                url: site_url+ '/surveyutility/ajax_validate',
                type: 'post',
                cache: false,               
                dataType: 'json',                
                data: json,
                success: function(data) {
                	$('#error-' +  data.id).html(data.error);
                }
            });

    });
});

function addOptionRequiredAttribute(){
	// for select, add required class by searching for .toggle-required .isrequired select
	$('#survey-content').find('.toggle-required.isRequired, select').addClass('required');
	// for radio, add required attribute by searching for .toggle-required .isrequired .radio
	$('#survey-content').find('.toggle-required.isRequired.radio').attr('required', "");
}

function removeOptionRequiredAttribute(){
	// for select, remove required class by searching for .toggle-required .isrequired select
	$('#survey-content').find('.toggle-required.isRequired, select').removeClass('required');
	// for radio, remove required attribute by searching for .toggle-required .isrequired .radio
	$('#survey-content').find('.toggle-required.isRequired.radio').removeAttr('required');
}

function loadParentFieldChildren(){	
	// grab all parent fields	
	var allSurveyParentFields = $.map($('.survey-question'), function(element) {
		  return $(element);
		});	
	// load all survey questions
	allSurveyParentFields.forEach(function(parentid) {
		loadChildren(parentid);
	});

}

function loadChildren(parentField){
    $.ajax({
        type: "post",
        dataType: 'json',
        url: site_url+ '/surveyutility/grabChild',
        cache: false,               
        data: { parentid : parentField.attr('parentid'), parenttype : parentField.attr('parenttype') },
        success: function(response){
            // response is expected to be the survey questions in json format
           	parentField.html("");
            var obj = response;
            if(obj.length>0){
                try{
                    $.each(obj, function(i,val){    

                    	var questionDisplay;
                        	
                    	if (val.input_type == "NONE"){
                    		questionDisplay = displaySection(val);
                    	} else {
                        	// display question
                    		questionDisplay = displayQuestion(val);
                    	}

                        parentField.append( questionDisplay );
                        // after the html is appended, load options if it has them
                        if ((val.input_type == "CHECKBOXES")||
                        	(val.input_type == "MULTIPLE_CHOICE")||
                        	(val.input_type == "DROPDOWN")){
                        	surveyField = $('#input-'+val.questionid);
                            loadOption(surveyField);
                        } else {
                            // after the input is appended, fill in responses
    						loadResponse(val.questionid, val.input_type);
                        }
                        
						
                    }); 
                }catch(e) {     
                    alert('Exception while request..');
                }       
            }else{
                parentField.html('No questions found');
            };          
        },
        error: function(){                      
            parentField.html("");
            parentField.html('Error while request..');
            // retry
            loadChildren(parentField);
        }
    });

}

// load response into input fields
function loadResponse(questionid, input_type){
	if ( (survey_responses === null) || (survey_responses.length == 0)){
		// no responses were saved
		return true;
	}
	if (input_type == "NONE"){
		// nothing to load
	} else if (input_type == "CHECKBOXES"){
		survey_responses.forEach(function(response) {
        	if (questionid == response.questionid){
            	// break down answerdescription into array that is comma delimited
            	var checkboxResponse = (response.answerdescription).split(',');
            	if(checkboxResponse === null || checkboxResponse.length == 0){
                	// presume no checkboxes were selected
            	} else {
					checkboxResponse.forEach(function(checkbox){
						$("#survey-content").find("#input-"+response.questionid).find('input[value="'+checkbox+'"]').attr("checked","checked");
					});
                }
            }
    	});
	} else if (input_type == "MULTIPLE_CHOICE"){
		survey_responses.forEach(function(response) {
        	if (questionid == response.questionid){
        		$("#survey-content").find("#input-"+response.questionid).find('input[value="'+response.answerdescription+'"]').attr("checked","checked");
            }
    	});
	} else if (input_type == "DROPDOWN"){
		survey_responses.forEach(function(response) {
        	if (questionid == response.questionid){
        		$("#survey-content").find("#select-"+response.questionid).val(response.answerdescription);
            }
    	});
    } else {
        // match response for text input
    	survey_responses.forEach(function(response) {
        	if (questionid == response.questionid){
        		 $("#survey-content").find("#input-"+response.questionid).val(response.answerdescription);
            }
    	});
    }
	return true;
    

}

function loadOption(surveyField){
    $.ajax({
        type: "post",
        dataType: 'json',
        url: site_url+ '/surveyutility/grabOptions',
        cache: false,               
        data: { questionid : surveyField.attr('questionid'), input_type : surveyField.attr('input_type') },
        success: function(response){
            // response is expected to be the survey questions in json format
           	surveyField.html("");

            var obj = response;
            if(obj.length>0){
                try{
                    var optionInput = "";
                    $.each(obj, function(i,val){    
                    	var optionDisplay = displayOptions(val, surveyField.attr('input_type'), surveyField.attr('isRequired'));
                    	optionInput = optionInput + optionDisplay;
                    }); 
                    // append select label if required
                    if (surveyField.attr('input_type') == "DROPDOWN"){
                        // wrap the input with select 
                    	optionInputWrapper = '<select class="toggle-required ';
                        if(surveyField.attr('isRequired') == 1){
                        	optionInputWrapper = optionInputWrapper + "isRequired";			
                        }
                        defaultOptionInput = '<option></option>';
                        optionInputWrapper = optionInputWrapper + '" name="input-'+surveyField.attr('questionid')+'" id="select-'+surveyField.attr('questionid')+'" >'
                        				+ defaultOptionInput
    									+ optionInput 
                    					+ '</select>';
                    	// set the wrapper and options as the optionInput 
    					optionInput = optionInputWrapper;
                    }
                	surveyField.append(optionInput);

                    // after the input is appended, and options loaded, fill in responses
					loadResponse(surveyField.attr('questionid'), surveyField.attr('input_type'));
                    
                }catch(e) {     
                    alert('Exception while request..');
                }       
            }else{
            	surveyField.html('No options found');
            };          
        },
        error: function(){                      
        	surveyField.html("");
        	surveyField.html('Error while request..');
        	// retry
        	loadOption(surveyField);
        }
    });

}

function displayOptions(option, input_type, isRequired){
	var optionDisplay;
	if (input_type == "CHECKBOXES"){
		optionDisplay = '<label class="checkbox">'
			  + '<input type="checkbox" name="input-'+option.questionid+'[]" value="'+option.itemdescription+'"/>'
			  + option.itemdescription
			  + '</label>';
	} else if (input_type == "MULTIPLE_CHOICE"){
		optionDisplay = '<label class="radio">'
			+ '<input type="radio" class="toggle-required radio ';
			if (isRequired == 1){
				optionDisplay = optionDisplay + 'isRequired';
			}
		optionDisplay = optionDisplay +'"name="input-'+option.questionid+'" value="'+option.itemdescription+'">'+option.itemdescription
			+ '</label>';
	}  else if (input_type == "DROPDOWN"){
		optionDisplay = '<option>'+option.itemdescription+'</option>';
	}
	return optionDisplay;
}

function displaySection(surveyQuestion){
    var sectionDisplay = '<blockquote>'
    	+  '<p>'+surveyQuestion.questiontitle+'</p>'
    	+ '<small class="text-info">'+surveyQuestion.help_text+'</small>'
    	+ '</blockquote>';
	return sectionDisplay;
}

function displayQuestion(question){
	// display question
	var requiredField = '';
	// add asterisk for required questions
	if (question.required == 1){
		requiredField = requiredField + '<span class="label label-important">* Required</span>';
	}
	if (question.input_type == "NUMERIC"){
		requiredField = requiredField + '<span class="label label-info">Numeric Input</span>';
	}
	var questionDisplay ='<p>  <a linkedTip="tip-'+question.questionid+'" href="#" class="toggle-tip"><i class="icon-question-sign"></i></a> <strong> ' + question.questiontitle
	+'</strong></p>'  + requiredField ;
	
	// add help tip
	questionDisplay = questionDisplay
	+ '<div id="tip-'+question.questionid+'" class="alert alert-info alert-block fade hide">'
	+ ' <button href="#" type="button" class="close">&times;</button>'
	+ '<b>Question Information</b>'
	+ '<p>'+question.help_text+'</p>'
	+ '</div>';

	var generatedInputForm = '';
	if (question.input_type == "TEXT"){
		generatedInputForm ='<div class="form-horizontal">'
    	+ '<input class="textInput" id="input-'+question.questionid+'" questionid="'+question.questionid+'" type="text" name="input-'+question.questionid+'" />'
		+ '</div>';		
	} else	if (question.input_type == "NUMERIC"){
		generatedInputForm ='<div class="form-horizontal">'
    	+ '<input class="numericInput textInput" id="input-'+question.questionid+'" questionid="'+question.questionid+'" type="text" name="input-'+question.questionid+'" />'
		+ '</div>';
	} else	if (question.input_type == "LONG_ANSWER"){
		generatedInputForm ='<div class="form-horizontal">'
			+ '<textarea class="textInput" id="input-'+question.questionid+'" questionid="'+question.questionid+'" name="input-'+question.questionid+'" cols="80" rows="3" ></textarea>'
			+ '</div>';
	} else if (question.input_type == "CHECKBOXES"){
		generatedInputForm = '<div class="question-options" id="input-'+question.questionid+'" input_type="'+question.input_type+'" isRequired="'+question.required+'" questionid="'+question.questionid+'"></div>'; // checkboxes cant be required (no selection is valid)
	} else if (question.input_type == "MULTIPLE_CHOICE"){
		generatedInputForm = '<div class="question-options"id="input-'+question.questionid+'" input_type="'+question.input_type+'" isRequired="'+question.required+'" questionid="'+question.questionid+'" ></div>';
	}  else if (question.input_type == "DROPDOWN"){
		generatedInputForm = '<div class="question-options"id="input-'+question.questionid+'" input_type="'+question.input_type+'" isRequired="'+question.required+'" questionid="'+question.questionid+'"></div>';
	}

	var errorDiv = '<a class="text-error" id="error-input-'+question.questionid+'"/>';
	
	return questionDisplay + generatedInputForm + errorDiv;
}

function previous(){
    
    new_page = parseInt($('#current_page').val()) - 1;
    //if there is an item before the current active link run the function
    if(new_page >= 1){
        go_to_page(new_page);
    }
    
}
 
function next(){
    new_page = parseInt($('#current_page').val()) + 1;
    //if there is an item after the current active link run the function
    if(new_page <= <?php echo $pageCount?>){
        go_to_page(new_page);
    }
    
}
function go_to_page(page_num){    
    //hide all children elements of content div, get specific items and show them
    $('#survey-content').children().css('display', 'none').removeClass('active_page');
    $('#survey-content').find('#page-'+page_num).css('display', 'block').addClass('active_page');
    $('#survey-pagination').children().removeClass('active');
    $('#survey-pagination').find('#pagination-'+page_num).addClass('active');
    
    //update the current page input field
    $('#current_page').val(page_num);
}


</script>
