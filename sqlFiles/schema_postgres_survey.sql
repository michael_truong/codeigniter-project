-- requires schema_postgres_tankauth_users.sql to be run beforehand
-- requires schema_postgres_survey_user_groups.sql to be run beforehand

-- drop tables if needed
DROP TABLE IF EXISTS users_completed_surveys;
DROP TABLE IF EXISTS registered_surveys;
DROP TABLE IF EXISTS survey_responses;
DROP TABLE IF EXISTS survey_question_options;
DROP TABLE IF EXISTS survey_questions;
DROP TABLE IF EXISTS survey_parent_fields;
DROP TABLE IF EXISTS parent_field;
DROP TABLE IF EXISTS survey_owner;
DROP TABLE IF EXISTS surveys;

CREATE Table surveys (
    surveyid serial NOT NULL,
    surveyname character varying(100),
    survey_description text,
    disabled smallint NOT NULL DEFAULT 0,
    anonymous smallint NOT NULL DEFAULT 0,
    default_group integer references groups (id),
    survey_state character varying (100) NOT NULL DEFAULT 'PENDING',
    CONSTRAINT survey_pkey PRIMARY KEY (surveyid )
);
ALTER TABLE surveys
  OWNER TO codeigniteruser;

Create Table survey_owner(
    userid integer references users (id),
    surveyid integer references surveys (surveyid),
    primary key (userid, surveyid)
);
ALTER TABLE survey_owner
  OWNER TO codeigniteruser;

Create Table parent_field (
    parent_field_id serial NOT NULL,
    field_type character varying (100),
    description character varying (100),
    survey_order integer,
    CONSTRAINT parent_field_pkey PRIMARY KEY (parent_field_id )
);
ALTER TABLE parent_field
  OWNER TO codeigniteruser;

Create Table survey_parent_fields(
    parent_field_id integer references parent_field (parent_field_id),
    surveyid integer references surveys (surveyid),
    primary key (parent_field_id, surveyid)
);
ALTER TABLE survey_parent_fields
  OWNER TO codeigniteruser;

CREATE Table survey_questions (
    questionid serial NOT NULL,
    input_type character varying(100) NOT NULL default 'TEXT',
    questiontitle text,
    help_text text default 'No help available for this question.',
    survey_order integer,
    required smallint NOT NULL default 0,
    parent_field_id integer NOT NULL references parent_field (parent_field_id),
    CONSTRAINT survey_question_pkey PRIMARY KEY (questionid )  
);
ALTER TABLE survey_questions
  OWNER TO codeigniteruser;

CREATE Table survey_question_options
(
    itemid serial NOT NULL,
    itemdescription text,
    option_order integer,
    questionid integer NOT NULL references survey_questions (questionid),
    CONSTRAINT survey_question_items_pkey PRIMARY KEY (itemid )
);
ALTER TABLE survey_question_options
  OWNER TO codeigniteruser;


Create Table registered_surveys (
    groupid integer references groups (id),
    surveyid integer references surveys (surveyid),
    primary key (groupid, surveyid)
);
ALTER TABLE registered_surveys
  OWNER TO codeigniteruser;

CREATE Table survey_responses(
    response_id serial,
    userid integer references users (id),
    questionid integer NOT NULL references survey_questions (questionid),
    answerdescription text,
    unlinked smallint NOT NULL default 0,
    submitted_response smallint NOT NULL default 0,
    survey_response_uuid text,
    CONSTRAINT survey_responses_pkey PRIMARY KEY (response_id )
);
ALTER TABLE survey_responses
  OWNER TO codeigniteruser;

create table users_completed_surveys (
    userid integer references users (id),
    surveyid integer references surveys (surveyid),
    when_completed timestamp without time zone NOT NULL DEFAULT now(),
    primary key (userid, surveyid)  
);
ALTER TABLE users_completed_surveys
  OWNER TO codeigniteruser;