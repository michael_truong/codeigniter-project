-- requires schema_postgres_tankauth_users.sql to be run beforehand

-- drop tables if needed
DROP TABLE IF EXISTS user_groups;
DROP TABLE IF EXISTS groups;

-- Table structure for table `groups`

CREATE TABLE groups
(
  id serial NOT NULL,
  groupname character varying(50) NOT NULL,
  text text NOT NULL,
  is_default_group smallint NOT NULL DEFAULT 0,
  CONSTRAINT user_groups_pkey PRIMARY KEY (id )
)
WITH (
  OIDS=FALSE
);
ALTER TABLE groups
  OWNER TO codeigniteruser;
  
-- Table relation between table "users" and "groups"

Create Table user_groups (
   groups_id integer references groups (id),
   user_id integer references users (id),
   primary key (groups_id, user_id)
);
ALTER TABLE user_groups
  OWNER TO codeigniteruser;