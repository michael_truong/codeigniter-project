-- modify encoding if required using superuser account
-- update pg_database set encoding = pg_char_to_encoding('UTF8') where datname = 'codeigniterdb'

-- Drop Tables if required
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS user_profiles;
DROP TABLE IF EXISTS user_autologin;
DROP TABLE IF EXISTS login_attempts;
DROP TABLE IF EXISTS ci_sessions;

-- SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE ci_sessions
(
  session_id character varying(40) NOT NULL DEFAULT '0',
  ip_address character varying(16) NOT NULL DEFAULT '0',
  user_agent character varying(150) NOT NULL,
  last_activity integer NOT NULL DEFAULT 0,
  user_data text NOT NULL,
  CONSTRAINT ci_sessions_pkey PRIMARY KEY (session_id )
)
WITH (
  OIDS=FALSE
);
ALTER TABLE ci_sessions
  OWNER TO codeigniteruser; 

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE login_attempts
(
  id serial NOT NULL,
  ip_address character varying(40) NOT NULL,
  login character varying(50) NOT NULL,
  "time" timestamp without time zone NOT NULL DEFAULT now(),
  CONSTRAINT login_attempts_pkey PRIMARY KEY (id )
)
WITH (
  OIDS=FALSE
);
ALTER TABLE login_attempts
  OWNER TO codeigniteruser;
CREATE OR REPLACE FUNCTION codeigniter.update_modified_column()
RETURNS TRIGGER AS $$
BEGIN
    NEW.time = now(); 
    RETURN NEW;
END;
$$ language 'plpgsql';

CREATE TRIGGER update_login_attempts_modtime BEFORE UPDATE ON login_attempts FOR EACH ROW EXECUTE PROCEDURE codeigniter.update_modified_column();

-- --------------------------------------------------------

--
-- Table structure for table `user_autologin`
--

CREATE TABLE user_autologin
(
  key_id character(32) NOT NULL,
  user_id integer NOT NULL DEFAULT 0,
  user_agent character varying(150) NOT NULL,
  last_ip character varying(40) NOT NULL,
  last_login timestamp without time zone NOT NULL DEFAULT now(),
  CONSTRAINT user_autologin_pkey PRIMARY KEY (key_id, user_id )
)
WITH (
  OIDS=FALSE
);
ALTER TABLE user_autologin
  OWNER TO codeigniteruser;

CREATE OR REPLACE FUNCTION codeigniter.update_modified_login()
RETURNS TRIGGER AS $$
BEGIN
    NEW.last_login = now(); 
    RETURN NEW;
END;
$$ language 'plpgsql';

CREATE TRIGGER update_autologin_modtime BEFORE UPDATE ON user_autologin FOR EACH ROW EXECUTE PROCEDURE codeigniter.update_modified_login();

-- --------------------------------------------------------

--
-- Table structure for table `user_profiles`
--

CREATE TABLE user_profiles
(
  id serial NOT NULL,
  user_id integer NOT NULL,
  country character varying(20) DEFAULT NULL,
  website character varying(255) DEFAULT NULL,
  CONSTRAINT user_profiles_pkey PRIMARY KEY (id )
)
WITH (
  OIDS=FALSE
);
ALTER TABLE user_profiles
  OWNER TO codeigniteruser;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--
-- Note: since all users are created as members, the first user will have to be 
-- set as an admin manually through the database :
--            update users set group_id = 100 where id = 1;

CREATE TABLE users
(
  id serial NOT NULL,
  username character varying(50) NOT NULL,
  password character varying(255) NOT NULL,
  email character varying(100) NOT NULL,
  group_id integer NOT NULL DEFAULT 300,
  activated smallint NOT NULL DEFAULT 1,
  banned smallint NOT NULL DEFAULT 0,
  ban_reason character varying(255) DEFAULT NULL,
  new_password_key character varying(50) DEFAULT NULL,
  new_password_requested timestamp without time zone DEFAULT NULL,
  new_email character varying(100) DEFAULT NULL,
  new_email_key character varying(50) DEFAULT NULL,
  last_ip character varying(40) NOT NULL,
  last_login timestamp without time zone NOT NULL DEFAULT now(),
  created timestamp without time zone NOT NULL DEFAULT now(),
  modified timestamp without time zone NOT NULL DEFAULT now(),
  CONSTRAINT user_pkey PRIMARY KEY (id )
)
WITH (
  OIDS=FALSE
);
ALTER TABLE users
  OWNER TO codeigniteruser;

CREATE TRIGGER update_users_login BEFORE UPDATE ON users FOR EACH ROW EXECUTE PROCEDURE codeigniter.update_modified_login();

CREATE OR REPLACE FUNCTION codeigniter.update_modified()
RETURNS TRIGGER AS $$
BEGIN
    NEW.modified = now(); 
    RETURN NEW;
END;
$$ language 'plpgsql';

CREATE TRIGGER update_users_modtime BEFORE UPDATE ON users FOR EACH ROW EXECUTE PROCEDURE codeigniter.update_modified();