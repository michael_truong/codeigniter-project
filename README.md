# This is a CodeIgniter Web Based Survey System

#Steps to take when first moving to a different server
    #set up your apache server if you haven't already
        #unzip files to your apache server

    #set up your postgres database if you havent already
	   #create relevant postgres users/tables in order (read sqlFiles/postgres_commands)
		  #application/sqlFiles/schema_postgres_tankauth_users.sql
		  #application/sqlFiles/schema_postgres_user_groups.sql
		  #application/sqlFiles/survey_postgres_schema.sql
		
    #Files to change when hosting on a different server
	   #application/config/config.php
	       #When hosting with Grieg using apache1341, leave hostname field as blank in conf/database.php
	   #application/config/database.php
	   #application/config/email.php